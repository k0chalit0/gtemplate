<div class="form-group">
	{{ Form::label($name, $label) }}
	{{ $control }}
	@if ($error)
		<span class="help-inline">{{ $error }}</span>
	@endif
</div>