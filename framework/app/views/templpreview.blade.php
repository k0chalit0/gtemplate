<!DOCTYPE html>
<html lang="en">
<head>
  @include('includesgen.head')
</head>
<body class="dark-theme">
@if (Auth::check())
  @include('includes.header')

  @include('includes.info-right')
@endif
<div class="page-container">
 @if (Auth::check())
  @include('includes.sidebar-left')
@endif

<div id="main-content">
  <div class="page-content"> 
    @yield('content')
  </div>
</div><!--/main-content end-->
</div><!--/page-container end-->
  @include('includes.footer')
</body>
</html>