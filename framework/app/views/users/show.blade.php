<!-- app/views/users/show.blade.php -->

@extends('master')

@section('content')
    <div class="page-content">
        <div class="container">
            <nav class="navbar navbar-inverse">
                <ul class="nav navbar-nav">
                    <li><a href="{{ URL::to('users') }}">Ver todos los Usuarios</a></li>
                    <li><a href="{{ URL::to('users/create') }}">Crear un Usuario</a></li>
                </ul>
            </nav>
        	<h1>Viendo Usuario: "{{ $user->first_name." ".$user->last_name }}"</h1>

            <div class="jumbotron text-center">
                <h2>{{ $user->username }}</h2>
                <p>
                    <strong>Email:</strong> {{ $user->email }}<br>
                </p>
            </div>
        </div>
    </div>
@endsection
