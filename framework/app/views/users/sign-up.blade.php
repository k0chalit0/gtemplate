@extends('master')

@section('content')
<div class="login-container">
	<div class="middle-login">
		<div class="block-web">
			<div class="head">
			<h3 class="text-center">GenTemplate - Registro</h3>
			</div>
			<div style="background:#fff;">
				{{ Form::open(['route'=> 'register', 'method'=>'POST', 'role'=>'form','class'=>'form-horizontal']) }}
				<div class="content">
				<div class="form-group">
	              <div class="col-sm-12">
	                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-user"></i></span>
	                  {{Form::input('text', 'first_name','', array('placeholder'=>'Nombre', 'class'=>'form-control')) }}
	                </div>
	              </div>
	            </div>
	            <div class="form-group">
	              <div class="col-sm-12">
	                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-user"></i></span>
	                  {{Form::input('text', 'last_name','', array('placeholder'=>'Apellido', 'class'=>'form-control')) }}
	                </div>
	              </div>
	            </div>
	            <div class="form-group">
	              <div class="col-sm-12">
	              <p>{{$errors->first('username');}}</p>
	                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-user"></i></span>
	                  {{Form::input('text', 'username','', array('placeholder'=>'Username', 'class'=>'form-control')) }}
	                </div>
	              </div>
	            </div>
	            <div class="form-group">
	              <div class="col-sm-12">
	              <p>{{$errors->first('email');}}</p>
	                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
	                  {{Form::input('text', 'email','', array('placeholder'=>'Correo electronico', 'class'=>'form-control')) }}
	                </div>
	              </div>
	            </div>
	        	<div class="form-group">
	              <div class="col-sm-12">
	                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-lock"></i></span>
	                  {{Form::input('password', 'password','', array('placeholder'=>'Contraseña','class'=>'form-control')) }}
	                </div>
	              </div>
	            </div>
				<div class="form-group">
	              <div class="col-sm-12">
	                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-lock"></i></span>
	                  {{Form::input('password', 'password_confirmation','', array('placeholder'=>'Confirmacion de ontraseña','class'=>'form-control')) }}
	                </div>
	              </div>
	            </div>
			</div>
			<div class="foot">
				<p>
					<input type="submit" value="Registrar" class="btn btn-primary">
				</p>
			</div>
				{{ Form::close() }}
				<div class="registrate">Si usted ya tiene una cuenta acceda <a href="/log-in"> aquí</a>
				</div>
			</div>
		</div>
		<div class="text-center out-links"><a href="#">&copy;  GenTemplate 2015 </a></div>
	</div>
</div>
@endsection