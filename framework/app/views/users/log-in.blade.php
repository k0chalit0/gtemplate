@extends('master')
@section('content')
<div class="login-container">
  <div class="middle-login">
    <div class="block-web">
      <div class="head">
        <h3 class="text-center">GenTemplate - Admin</h3>
      </div>
      	<div style="background:#fff;">
				{{ Form::open(['route'=> 'login', 'method'=>'POST', 'role'=>'form', 'class'=>'form-horizontal'] ) }}
				<div class="content">
				<h4 class="title">Acceso de usuario</h4>
				@if (Session::has('login_error'))
					<span class="label label-danger">Credenciales no validas</span>
				@endif
					 <div class="form-group">
		              <div class="col-sm-12">
		                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
		                  {{Form::input('text', 'email','', array('placeholder'=>'Correo Electronico', 'class'=>'form-control')) }}
		                </div>
		              </div>
		            </div>
		            <div class="form-group">
		              <div class="col-sm-12">
		                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-lock"></i></span>
		                  {{Form::input('password', 'password','', array('placeholder'=>'Contraseña','class'=>'form-control')) }}
		                </div>
		              </div>
		            </div>
		            </div>
				<div class="foot">
					<input type="submit" value="Ingresar" class="btn btn-primary">
				</div>
				{{ Form::close() }}
				<div class="registrate">Si usted no tiene una cuenta Regístrese <a href="/sign-up"> aquí</a>
				</div>
		</div>
    </div>
    <div class="text-center out-links"><a href="#">&copy;  GenTemplate 2015 </a></div>
  </div>
</div>
@endsection