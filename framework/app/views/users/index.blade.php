<!-- app/views/users/index.blade.php -->

@extends('master')

@section('content')
    <div class="page-content">
        <div class="container">

        <nav class="navbar navbar-inverse">
            <ul class="nav navbar-nav">
                <li><a href="{{ URL::to('users') }}">Ver todos los Usuarios</a></li>
                <li><a href="{{ URL::to('users/create') }}">Crear un Usuario</a>
            </ul>
        </nav>

        <h1>Lista de Usuarios</h1>

        <!-- will be used to show any messages -->
        @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif

        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <td>ID</td>
                    <td>Nombre Completo</td>
                    <td>UserName</td>
                    <td>E-mail</td>
                    <td>Rol</td>
                    <td>Acciones</td>
                </tr>
            </thead>
            <tbody>
            @foreach($users as $key => $value)
                <tr>
                    <td>{{ $value->id }}</td>
                    <td>{{ $value->first_name." ".$value->last_name }}</td>
                    <td>{{ $value->username }}</td>
                    <td>{{ $value->email }}</td>
                    <td>{{ $value->rol }}</td>

                    <!-- we will also add show, edit, and delete buttons -->
                    <td>

                        <a class="btn btn-small btn-success" href="{{ URL::to('users/' . $value->id) }}">Ver</a>

                        <a class="btn btn-small btn-info" href="{{ URL::to('users/' . $value->id . '/edit') }}">Editar</a>
                        {{ Form::open(array('method'=> 'DELETE', 'route' => array('users.destroy', $value->id))) }}        
                        {{ Form::submit('Eliminar', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        @if($value->rol=='user')
                        <a class="btn btn-labeled btn-default" href="{{ URL::to('users/asignAdmin/' . $value->id ) }}">
                            <span class="btn-label">
                                <i class="glyphicon glyphicon-chevron-up"></i>
                            </span>
                            Admin
                        </a>
                        @elseif($value->rol=='admin')
                        <a class="btn btn-labeled btn-default" href="{{ URL::to('users/desasignAdmin/' . $value->id ) }}">
                            <span class="btn-label">
                                <i class="glyphicon glyphicon-chevron-down"></i>
                            </span>
                            Quitar Admin
                        </a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </div>
    </div>
@endsection