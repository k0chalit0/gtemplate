@extends('master')

@section('content')
<div class="page-content">
    <div class="container">
    @if(isset($mensaje))
        <div>
        {{ $mensaje }}
        </div>
    @endif
	<h1>Mis Variaciones </h1>
	 <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>ID</td>
                <td>ID template 1</td>
                <td>ID Template 2</td>
                <td>Estado</td>
                <td>URL</td>
                <td>Acciones</td>
            </tr>
        </thead>
        <tbody>
        @foreach($result['vars'] as $key => $value)
            <tr>
                <td>{{ $value->id }}</td>
                <td>{{ $value->template1 }}</td>
                <td>{{ $value->template2 }}</td>
                <td>{{ $value->status }}</td>
                <td> <a href="/v/{{ $value->url }}">Link</a></td>

                <td>

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div>
    <h2>Generar nueva variacion</h2>
        {{ Form::open(array('url' => 'variations/guardar', 'enctype'=>'multipart/form-data')) }}
        @foreach($result['templates'] as $key => $value)
            <p><label>{{ Form::checkbox('template[]',$value['id'] ) }} {{$value['name'] }} </label></p>
        @endforeach
                <p>{{ Form::submit('Crear', array('class' => 'btn btn-btn-success', 'disabled'=>'disabled')) }}</p>

            {{ Form::close() }}
    </div>
    </div>
</div>

@endsection

@section('footer_scripts')
<script>
    $('input[type=checkbox]').on('change', function (e) {
        if ($('input[type=checkbox]:checked').length > 2) {
            $(this).prop('checked', false);
            alert("Solo 2 permitidos");
        }
        else
        {
            if($('input[type=checkbox]:checked').length == 2)
            {
                $('input[type="submit"]').attr('disabled', false);
            }
            else
            {
                $('input[type="submit"]').attr('disabled', true);
            }
        }
    });
</script>
@endsection