<!DOCTYPE html>
<!--if lt IE 7html.no-js.lt-ie9.lt-ie8.lt-ie7
-->
<!--if IE 7html.no-js.lt-ie9.lt-ie8
-->
<!--if IE 8html.no-js.lt-ie9
-->
<!--[if gt IE 8]><!-->
<html class="no-js">
  <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta name="language" content="es">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title>GenTemplate</title>
    <link rel="stylesheet" href="https://fontastic.s3.amazonaws.com/mBcpKha37mryLvEf949bD9/icons.css">
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  </head>
  <body>
  @if (Auth::check())
    <header class="header clearfix">
      <div class="box-left">
        <ul class="header_nav">
          <li>
            <div class="toggle-menu toggle-menu-left">
              <label for="check-nav"></label><a href="#" class="toggle-menu_btn"><span></span><span></span><span></span></a>
            </div>
          </li>
          <li>
            <h1 class="logo"><a href="/">GenTemplate</a></h1>
          </li>
          <li><a href="#" class="header_link"><i class="icon-arrow-double-l"></i>Ir a mi sitio web</a></li>
        </ul>
      </div>
      <div class="box-right">
        <ul class="header_nav">
        
          <li><a href="{{ route('logout') }}" class="header_link">
              Cerrar Session<i class="icon-arrow-double-r"></i></a></li>
          <li>
            <div class="toggle-menu toggle-menu-left">
              <label for="check-nav-2"></label><a href="#" class="toggle-menu_btn"><span></span><span></span><span></span></a>
            </div>
          </li>
        </ul>
      </div>
    </header>
    <main class="main">
      <input type="checkbox" checked id="check-nav" class="check-nav">
      <nav id="nav-main" class="navigation">
        <ul class="menu-user">
            <li class="menu-user_item"><a class="menu-user_link"><i class="menu-user_icon icon-user"></i><span class="menu-user_text">
              {{Auth::user()->first_name." ".Auth::user()->last_name}}
            </span></a></li>
            <li class="menu-user_item"><a class="menu-user_link" href="{{ route('mistemplates') }}"><i class="menu-user_icon icon-copy"></i><span class="menu-user_text">Mis páginas</span></a></li>

        </ul>
        <ul class="menu">
          <li class="menu_item"><a href="/" class="menu_link"><i class="menu_icon icon-file-add"></i><span class="menu_text">Crear página</span></a></li>
          <li class="menu-user_item"><a href="{{ route('logout') }}" class="menu-user_link"><i class="menu-user_icon icon-log-out"></i><span class="menu-user_text">Salir</span></a></li>
        </ul>
      </nav>
      @endif      
        @yield('content')

    </main>
    <script src="{{ asset('bootstrap/js/min/plugins-min.js') }}"></script>
    <script src="{{ asset('bootstrap/js/main.js ') }}"></script>
    <script src="{{ asset('bootstrap/js/main-template.js ') }}"></script>
    <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="{{ asset('bootstrap/js/app.js ') }}"></script>

  </body>
</html>