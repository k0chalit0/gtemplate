<!-- app/views/seo/edit.blade.php -->

@extends('master')

@section('content')
    <div class="page-content">

	    <div class="container">

	    <nav class="navbar navbar-inverse">
	        <ul class="nav navbar-nav">
	            <li><a href="{{ URL::to('mistemplates') }}">Ver todos mis templates</a></li>
	        </ul>
	    </nav>
		<h1>Definiendo SEO</h1>
		<!-- if there are creation errors, they will show here -->
		{{ HTML::ul($errors->all()) }}

		{{ Form::model($seotemplate, array('route' => array('seotemplates.update', $seotemplate->id), 'method' => 'PUT')) }}

			{{ Field::text('title',$seotemplate->title) }}
			{{ Form::label('description','Descripcion',array('id'=>'','class'=>'')) }}
			{{ Form::textarea('description',$seotemplate->description,array('class'=>'form-control','placeholder'=>'Description')) }}
			{{ Field::text('keywords',$seotemplate->keywords) }}
			{{-- Form::hidden('slug',$seotemplate->slug) --}}
			{{ Form::hidden('usertemplate_id',$seotemplate->usertemplate_id) }}
			<p>
		    {{ Form::submit('Guardar', array('class' => 'btn btn-btn-success')) }}
		    </p>

		{{ Form::close() }}
	    
	    </div>
	</div>
@endsection