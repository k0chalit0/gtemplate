<!-- app/views/seo/new.blade.php -->

@extends('master')

@section('content')
    <div class="page-content">

	    <div class="container">

	    <nav class="navbar navbar-inverse">
	        <ul class="nav navbar-nav">
	            <li><a href="{{ URL::to('mistemplates') }}">Ver todos mis templates</a></li>
	        </ul>
	    </nav>
		<h1>Definiendo SEO</h1>
		<!-- if there are creation errors, they will show here -->
		{{ HTML::ul($errors->all()) }}

		{{ Form::open(array('url' => 'seotemplates', 'enctype'=>'multipart/form-data')) }}

			{{ Field::text('title',$usertemplate->name) }}
			{{ Form::label('description','Descripcion',array('id'=>'','class'=>'')) }}
			{{ Form::textarea('description','',array('class'=>'form-control','placeholder'=>'Description')) }}
			{{ Field::text('keywords') }}
			<div class="form-group">
			{{ Form::label('url', 'URL')}}
			{{ Form::text('url', strtolower(str_replace(' ', '-', $usertemplate->name)), array('disabled'=>'disabled', 'class'=>'form-control')) }}
			</div>
			{{ Form::hidden('slug', strtolower(str_replace(' ', '-', trim($usertemplate->name) ))) }}
			{{ Form::hidden('usertemplate_id',$usertemplate->id) }}
			<p>
		    {{ Form::submit('Guardar', array('class' => 'btn btn-btn-success')) }}
		    </p>

		{{ Form::close() }}
	    
	    </div>
	</div>
@endsection    	