@extends('master')
@section('content')
 <div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Tab/Accordions</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->  
      <div class="row">
          <div class="col-sm-6 col-md-6 col-lg-6">
            <div class="panel-group accordion" id="accordion">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"> <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> <i class="fa fa-angle-right"></i> Basic accordion </a> </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse">
                  <div class="panel-body"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vel orci et felis tempus rutrum. </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"> <a class="" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"> <i class="fa fa-angle-right"></i> Collapsible Group Item #2 </a> </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse in" style="height: auto;">
                  <div class="panel-body">Sed pretium est sit amet ligula consequat, sed suscipit arcu rhoncus. Sed bibendum posuere tincidunt. </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"> <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree"> <i class="fa fa-angle-right"></i> Collapsible Group Item #3 </a> </h4>
                </div>
                <div id="collapseThree" class="panel-collapse in">
                  <div class="panel-body"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vel orci et felis tempus rutrum. Mauris euismod augue in ante tempor, in dapibus nunc lobortis. Etiam euismod diam justo, quis luctus leo commodo et. Sed pretium est sit amet ligula consequat, sed suscipit arcu rhoncus. Sed bibendum posuere tincidunt. In iaculis ante ac ullamcorper fermentum. In vel mi porta, bibendum nulla et, scelerisque lacus. Morbi pellentesque interdum blandit. In consequat tempus pharetra. Aenean non velit lacus.  </div>
                </div>
              </div>
            </div>
            <div class="panel-group accordion accordion-semi" id="accordion3">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"> <a class="" data-toggle="collapse" data-parent="#accordion3" href="#ac3-1"> <i class="fa fa-angle-right"></i> Semi-primary Accordion </a> </h4>
                </div>
                <div style="height: auto;" id="ac3-1" class="panel-collapse in">
                  <div class="panel-body"> Etiam euismod diam justo, quis luctus leo commodo et. Sed pretium est sit amet ligula consequat, sed suscipit arcu rhoncus. Sed bibendum posuere tincidunt. </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"> <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ac3-2"> <i class="fa fa-angle-right"></i> Collapsible Group Item #2 </a> </h4>
                </div>
                <div id="ac3-2" class="panel-collapse collapse" style="height: 0px;">
                  <div class="panel-body"> In vel mi porta, bibendum nulla et, scelerisque lacus. Morbi pellentesque interdum blandit. In consequat tempus pharetra. </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"> <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ac3-3"> <i class="fa fa-angle-right"></i> Collapsible Group Item #3 </a> </h4>
                </div>
                <div id="ac3-3" class="panel-collapse collapse" style="height: 0px;">
                  <div class="panel-body">Etiam euismod diam justo, quis luctus leo commodo et. Sed pretium est sit amet ligula consequat, sed suscipit arcu rhoncus. </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-6 col-lg-6">
            <div class="panel-group accordion accordion-color" id="accordion2">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"> <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse-1"> <i class="fa fa-angle-right"></i> Primary Color </a> </h4>
                </div>
                <div id="collapse-1" class="panel-collapse collapse">
                  <div class="panel-body"> We have a full documentation for every single thing in this template, let's check it out and if you need support with. </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"> <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse-2"> <i class="fa fa-angle-right"></i> Collapsible Group Item #2 </a> </h4>
                </div>
                <div id="collapse-2" class="panel-collapse collapse" style="height: 0px;">
                  <div class="panel-body"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vel orci et felis tempus rutrum. Mauris euismod augue in ante tempor, in dapibus nunc lobortis. Etiam euismod diam justo, quis luctus leo commodo et.  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"> <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse-3"> <i class="fa fa-angle-right"></i> Collapsible Group Item #3 </a> </h4>
                </div>
                <div id="collapse-3" class="panel-collapse collapse" style="height: 0px;">
                  <div class="panel-body"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non </div>
                </div>
              </div>
            </div>
            <div class="panel-group accordion accordion-semi" id="accordion4">
              <div class="panel panel-default">
                <div class="panel-heading success">
                  <h4 class="panel-title"> <a class="collapsed" data-toggle="collapse" data-parent="#accordion4" href="#ac4-1"> <i class="fa fa-angle-right"></i> Success Color </a> </h4>
                </div>
                <div style="height: 0px;" id="ac4-1" class="panel-collapse collapse">
                  <div class="panel-body">Sed bibendum posuere tincidunt. In iaculis ante ac ullamcorper fermentum. In vel mi porta, bibendum nulla et, scelerisque lacus. Morbi pellentesque interdum blandit. In consequat tempus pharetra. Aenean non velit lacus. </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading warning">
                  <h4 class="panel-title"> <a class="collapsed" data-toggle="collapse" data-parent="#accordion4" href="#ac4-2"> <i class="fa fa-angle-right"></i> Warning Color </a> </h4>
                </div>
                <div id="ac4-2" class="panel-collapse collapse">
                  <div class="panel-body"> In consequat tempus pharetra. Aenean non velit lacus. </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading danger">
                  <h4 class="panel-title"> <a class="collapsed" data-toggle="collapse" data-parent="#accordion4" href="#ac4-3"> <i class="fa fa-angle-right"></i> Danger Color </a> </h4>
                </div>
                <div id="ac4-3" class="panel-collapse collapse">
                  <div class="panel-body">Sed bibendum posuere tincidunt. In iaculis ante ac ullamcorper fermentum. In vel mi porta, bibendum nulla et, scelerisque lacus. Morbi pellentesque interdum blandit. In consequat tempus pharetra. Aenean non velit lacus. </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        
      <div class="row">
          <div class="col-sm-6 col-md-6">
          <h3>Tab</h3>
            <div class="tab-container">
              <ul class="nav nav-tabs">
                <li class=""><a href="#home" data-toggle="tab">Home</a></li>
                <li class="active"><a href="#profile" data-toggle="tab">Profile</a></li>
                <li class=""><a href="#messages" data-toggle="tab">Messages</a></li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane cont" id="home">
                  <h4>Basic Tabs</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vel orci et felis tempus rutrum. Mauris euismod augue in ante tempor, in dapibus nunc lobortis. Etiam euismod diam justo, quis luctus leo commodo et.</p>
                </div>
                <div class="tab-pane cont active" id="profile">
                  <h4>Typography</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vel orci et felis tempus rutrum.</p>
                </div>
                <div class="tab-pane" id="messages">Aenean non velit lacus.</div>
              </div>
            </div>
            <div class="tab-container tab-left">
              <ul class="nav nav-tabs flat-tabs">
                <li class=""><a href="#tab3-1" data-toggle="tab"><i class="fa fa-home"></i></a></li>
                <li class=""><a href="#tab3-2" data-toggle="tab"><i class="fa fa-text-height"></i></a></li>
                <li class="active"><a href="#tab3-3" data-toggle="tab"><i class="fa fa-camera"></i></a></li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane cont fade" id="tab3-1">
                  <h3>Left Tabs</h3>
                  <ul>
                    <li>Responsive design</li>
                    <li>HTML5</li>
                    <li>CSS3</li>
                    <li>jQuery</li>
                  </ul>
                </div>
                <div class="tab-pane cont fade" id="tab3-2">
                  <h4>Typography</h4>
                  <p>Fusce vel orci et felis tempus rutrum. Mauris euismod augue in ante tempor, in dapibus nunc lobortis. Etiam euismod diam justo, quis luctus leo commodo et. Sed pretium est sit amet ligula consequat, sed suscipit arcu rhoncus. Sed bibendum posuere tincidunt. In iaculis ante ac ullamcorper fermentum. In vel mi porta, </p>
                </div>
                <div class="tab-pane fade active in" id="tab3-3">
                  <h4>Typography</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vel orci et felis tempus rutrum. Mauris euismod augue in ante tempor, in dapibus nunc lobortis. Etiam euismod diam justo, quis luctus leo commodo et. Sed pretium est sit amet ligula consequat, sed suscipit arcu rhoncus. Sed bibendum posuere tincidunt. In iaculis ante ac ullamcorper fermentum. In vel mi porta, bibendum nulla et, scelerisque lacus. Morbi pellentesque interdum blandit. In consequat tempus pharetra. Aenean non velit lacus. </p>
                  <a href="#">Read more</a> </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-6 col-lg-6">
            <div class="tab-container tab-bottom">
              <div class="tab-content">
                <div class="tab-pane cont" id="tab2-1">
                  <h4>Basic Tabs</h4>
                  <p>In consequat tempus pharetra. Aenean non velit lacus.</p>
                </div>
                <div class="tab-pane cont" id="tab2-2">
                  <h4>Typography</h4>
                  <p>Sed bibendum posuere tincidunt. In iaculis ante ac ullamcorper fermentum. In vel mi porta, bibendum nulla et, scelerisque lacus. Morbi pellentesque interdum blandit. In consequat tempus pharetra. Aenean non velit lacus. </p>
                </div>
                <div class="tab-pane active" id="tab2-3">
                  <h4>Typography</h4>
                  <p>Sed bibendum posuere tincidunt. In iaculis ante ac ullamcorper fermentum. In vel mi porta, bibendum nulla et, scelerisque lacus. Morbi pellentesque interdum blandit. In consequat tempus pharetra. Aenean non velit lacus.</p>
                  <a href="#">Read more</a> </div>
              </div>
              <ul class="nav nav-tabs flat-tabs">
                <li class=""><a href="#tab2-1" data-toggle="tab">Home</a></li>
                <li class=""><a href="#tab2-2" data-toggle="tab">Profile</a></li>
                <li class="active"><a href="#tab2-3" data-toggle="tab">Messages</a></li>
              </ul>
            </div>
            <div class="tab-container tab-right">
              <ul class="nav nav-tabs flat-tabs">
                <li class="active"><a href="#tab4-1" data-toggle="tab"><i class="fa fa-home"></i></a></li>
                <li><a href="#tab4-2" data-toggle="tab"><i class="fa fa-text-height"></i></a></li>
                <li><a href="#tab4-3" data-toggle="tab"><i class="fa fa-camera"></i></a></li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active cont fade in" id="tab4-1">
                  <h3>Right Tabs</h3>
                  <ul>
                    <li>Responsive design</li>
                    <li>HTML5</li>
                    <li>CSS3</li>
                    <li>jQuery</li>
                  </ul>
                </div>
                <div class="tab-pane fade cont" id="tab4-2">
                  <h4>Typography</h4>
                  <p>In iaculis ante ac ullamcorper fermentum. In vel mi porta, bibendum nulla et, scelerisque lacus. Morbi pellentesque interdum blandit. In consequat tempus pharetra. Aenean non velit lacus.</p>
                </div>
                <div class="tab-pane fade" id="tab4-3">
                  <h4>Typography</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vel orci et felis tempus rutrum. Mauris euismod augue in ante tempor, in dapibus nunc lobortis. Etiam euismod diam justo, quis luctus leo commodo et. Sed pretium est sit amet ligula consequat, sed suscipit arcu rhoncus. Sed bibendum posuere tincidunt. In iaculis ante ac ullamcorper fermentum. In vel mi porta, bibendum nulla et, scelerisque lacus. Morbi pellentesque interdum blandit. In consequat tempus pharetra. Aenean non velit lacus.</p>
                  <a href="#">Read more</a> </div>
              </div>
            </div>
          </div>
        </div><!--/row-->  
    </div><!--/page-content end--> 
@stop