@extends('master')
@section('content')    
    <div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Dashboard</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Form Validation States</h3>
            </div>
            <div class="porlets-content">
              <form action="" class="form-horizontal row-border">
                <div class="form-group has-success lable-padd">
                  <label for="" class="col-md-3">Input with Success</label>
                  <div class="col-md-6">
                    <input type="text" class="form-control">
                  </div>
                  <div class="col-md-3">
                    <p class="help-block"><i class="fa fa-check"></i> Great success!</p>
                  </div>
                </div><!--/form-group-->
                <div class="form-group has-warning lable-padd">
                  <label for="" class="col-md-3">Input with Warning</label>
                  <div class="col-md-6">
                    <input type="text" class="form-control">
                  </div>
                  <div class="col-md-3">
                    <p class="help-block"><i class="fa fa-exclamation-triangle"></i> Be careful!</p>
                  </div>
                </div><!--/form-group-->
                <div class="form-group has-error lable-padd">
                  <label for="" class="col-md-3">Input with Error</label>
                  <div class="col-md-6">
                    <input type="text" class="form-control">
                  </div>
                  <div class="col-md-3">
                    <p class="help-block"><i class="fa fa-times-circle"></i> Oops!</p>
                  </div>
                </div><!--/form-group-->
              </form>
            </div><!--/porlets-content--> 
          </div><!--/block-web--> 
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Basic Form</h3>
            </div>
            <div class="porlets-content">
              <form action="#" parsley-validate novalidate>
                <div class="form-group">
                  <label>User Name</label>
                  <input type="text" name="nick" parsley-trigger="change" required placeholder="Enter user name" class="form-control">
                </div><!--/form-group-->
                <div class="form-group">
                  <label>Email address</label>
                  <input type="email" name="email" parsley-trigger="change" required placeholder="Enter email" class="form-control">
                </div><!--/form-group-->
                <div class="form-group">
                  <label>Password</label>
                  <input id="pass1" type="password" placeholder="Password" required class="form-control">
                </div><!--/form-group-->
                <div class="form-group">
                  <label>Repeat Password</label>
                  <input parsley-equalto="#pass1" type="password" required placeholder="Password" class="form-control">
                </div><!--/form-group-->
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="remember">
                    Remember me </label>
                </div><!--/checkbox-->
                <button class="btn btn-primary" type="submit">Submit</button>
                <button class="btn btn-default">Cancel</button>
              </form>
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-6-->
        
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Horizontal Form</h3>
            </div>
            <div class="porlets-content">
              <form class="form-horizontal" role="form"  parsley-validate novalidate>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Email</label>
                  <div class="col-sm-7">
                    <input type="email" required parsley-type="email" class="form-control" id="inputEmail3" placeholder="Email">
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-3 control-label">Password</label>
                  <div class="col-sm-7">
                    <input type="password" required class="form-control" id="inputPassword3" placeholder="Password">
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-3 control-label">Repeat Password</label>
                  <div class="col-sm-7">
                    <input type="password" required class="form-control" id="inputPassword3" placeholder="Password">
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-3 control-label">Web Site</label>
                  <div class="col-sm-7">
                    <input type="url" required parsley-type="url" class="form-control" placeholder="URL">
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <div class="col-sm-offset-3 col-sm-7">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" name="remember_the_fallen">
                        Remember me </label>
                    </div>
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Registrer</button>
                    <button class="btn btn-default">Cancel</button>
                  </div>
                </div><!--/form-group--> 
              </form>
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-6--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Validation Types</h3>
            </div>
            <div class="porlets-content">
              <form class="form-horizontal group-border-dashed" action="#" parsley-validate novalidate>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Required</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" required placeholder="Type something" />
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <label class="col-sm-3 control-label">Min Length</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" required parsley-minlength="6" placeholder="Min 6 chars." />
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <label class="col-sm-3 control-label">Max Length</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" required parsley-maxlength="6" placeholder="Max 6 chars." />
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <label class="col-sm-3 control-label">Range Length</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" required parsley-rangelength="[5,10]" placeholder="Text between 5 - 10 chars length" />
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <label class="col-sm-3 control-label">Min Value</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" required parsley-min="6" placeholder="Min value is 6" />
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <label class="col-sm-3 control-label">Max Value</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" required parsley-max="6" placeholder="Max value is 6" />
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <label class="col-sm-3 control-label">Range Value</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" required parsley-range="[6, 100]" placeholder="Number between 6 - 100" />
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <label class="col-sm-3 control-label">Regular Exp</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" required parsley-regexp="#[A-Fa-f0-9]{6}" placeholder="Hex. Color" />
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <label class="col-sm-3 control-label">Equal To</label>
                  <div class="col-sm-3">
                    <input type="password" id="pass2" class="form-control" required placeholder="Password" />
                  </div>
                  <div class="col-sm-3">
                    <input type="password" class="form-control" required parsley-equalto="#pass2" placeholder="Re-Type Password" />
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <label class="col-sm-3 control-label">Min check</label>
                  <div class="col-sm-6">
                    <div class="radio">
                      <label>
                        <input type="checkbox" value="bar" name="mincheck[]">
                        <span class="custom-checkbox"></span> Option 1</label>
                    </div><!--/form-group--> 
                    <div class="radio">
                      <label>
                        <input type="checkbox" value="bar" name="mincheck[]">
                        <span class="custom-checkbox"></span> Option 2</label>
                    </div>
                    <div class="radio">
                      <label>
                        <input parsley-mincheck="2"  type="checkbox" value="bar" name="mincheck[]">
                        <span class="custom-checkbox"></span> Option 3</label>
                    </div>
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <label class="col-sm-3 control-label">Max check</label>
                  <div class="col-sm-6">
                    <div class="radio">
                      <label>
                        <input type="checkbox" value="bar" name="maxcheck[]">
                        <span class="custom-checkbox"></span> Option 1</label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="checkbox" value="bar" name="maxcheck[]">
                        <span class="custom-checkbox"></span> Option 2</label>
                    </div>
                    <div class="radio">
                      <label>
                        <input parsley-maxcheck="2"  type="checkbox" value="bar" name="maxcheck[]">
                        <span class="custom-checkbox"></span> Option 3</label>
                    </div>
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <label class="col-sm-3 control-label">E-Mail</label>
                  <div class="col-sm-6">
                    <input type="email" class="form-control" required parsley-type="email" placeholder="Enter a valid e-mail" />
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <label class="col-sm-3 control-label">URL</label>
                  <div class="col-sm-6">
                    <input parsley-type="url" type="url" class="form-control" required placeholder="URL" />
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <label class="col-sm-3 control-label">Digits</label>
                  <div class="col-sm-6">
                    <input parsley-type="digits" type="text" class="form-control" required placeholder="Enter only digits" />
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <label class="col-sm-3 control-label">Number</label>
                  <div class="col-sm-6">
                    <input parsley-type="number" type="text" class="form-control" required placeholder="Enter only numbers" />
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <label class="col-sm-3 control-label">Alphanumeric</label>
                  <div class="col-sm-6">
                    <input parsley-type="alphanum" type="text" class="form-control" required placeholder="Enter alphanumeric value" />
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <label class="col-sm-3 control-label">Date ISO</label>
                  <div class="col-sm-6">
                    <input parsley-type="dateIso" type="text" class="form-control" required placeholder="YYYY-MM-DD" />
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <label class="col-sm-3 control-label">Phone</label>
                  <div class="col-sm-6">
                    <input parsley-type="phone" type="text" class="form-control" required placeholder="(XXX) XXXX XXX" />
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <label class="col-sm-3 control-label">Textarea</label>
                  <div class="col-sm-6">
                    <textarea class="form-control"></textarea>
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button class="btn btn-default">Cancel</button>
                  </div>
                </div><!--/form-group--> 
              </form>
            </div><!--/porlets-content--> 
          </div><!--/block-web--> 
        </div><!--/col-md-12--> 
      </div><!--/row--> 
      
    </div><!--/page-content end--> 
@stop