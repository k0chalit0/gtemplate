@extends('master')
@section('content')
	<div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Float Charts</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
           <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header"> Area Chart </h3>
            </div>
			
			<div class="porlets-content">
			
			<div id="visitors-chart">
                            <div id="visitors-container" style="width: 100%;height:300px; text-align: center; margin:0 auto;">
                            </div>
                        </div>
			</div>
            
          </div><!--/block-web--> 
        </div><!--/col-md-12--> 
      </div><!--/row-->
	  
	  
	  <div class="row">
        <div class="col-md-12">
          <div class="block-web">
           <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header"> Real Time Chart  </h3>
            </div>
			
			<div class="porlets-content">
			<div id="reatltime-chart">
                                <div id="reatltime-chartContainer" style="width:100%;height:300px; text-align: center; margin:0 auto;">
                                </div>
                            </div>
			
			</div>
            
          </div><!--/block-web--> 
        </div><!--/col-md-12--> 
      </div><!--/row-->
	  
	  
	  <div class="row">
        <div class="col-md-6">
          <div class="block-web">
           <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Pie Chart  </h3>
            </div>
			
			<div class="porlets-content">
			<div id="pie-chart" class="pie-chart">
                                <div id="pie-chartContainer" style="width: 100%;height:400px; text-align: left;">
                                </div>
                            </div>
			
			</div>
            
          </div><!--/block-web--> 
        </div><!--/col-md-6--> 
		<div class="col-md-6">
          <div class="block-web">
           <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header"> Donut Chart  </h3>
            </div>
			
			<div class="porlets-content">
			<div id="pie-chart-donut" class="pie-chart">
                                <div id="pie-donutContainer" style="width: 100%;height:400px; text-align: left;">
                                </div>
                            </div>
			
			</div>
            
          </div><!--/block-web--> 
        </div><!--/col-md-6--> 
      </div><!--/row-->
     
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
           <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header"> Combined Chart  </h3>
            </div>
			
			<div class="porlets-content">
			
			<div id="combine-chart">
                                <div id="legendcontainer26" class="legend-block">
                                </div>
                                <div id="combine-chartContainer" style="width: 100%;height:300px; text-align: center; margin:0 auto;">
                                </div>
                            </div>
			</div>
            
          </div><!--/block-web--> 
        </div><!--/col-md-12--> 
      </div><!--/row-->
	  
	  
	  <div class="row">
        <div class="col-md-12">
          <div class="block-web">
           <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header"> Toggle Chart  </h3>
            </div>
			
			<div class="porlets-content">
			 <div id="toggle-chart">
                                <div class="clearfix">
                                    <form class="form-horizontal pull-left chart-control">
                                        <div class="control-group">
                                            <label class="control-label">Chart Type :</label>
                                            <div class="series-list">
                                                <label>
                                                    <input id="chartType1" checked name="ct" type="radio" value="line"/>
													<span class="custom-radio"></span>
                                                    Line Chart</label>
													
                                                <label>
                                                    <input id="chartType2" name="ct" type="radio" value="bar"/>
													<span class="custom-radio"></span>
                                                    Bar Chart </label>
                                            </div>
                                        </div>
                                    </form>
                                    <form class="form-horizontal chart-control pull-right chart-control">
                                        <div class="control-group ">
                                            <label class="control-label"> Toggle series :</label>
                                            <div class="series-list">
                                                <label>
                                                    <input type="checkbox" id="cbdata1" checked>
													<span class="custom-checkbox"></span>
                                                    data1</label>
                                                <label>
                                                    <input type="checkbox" id="cbdata2" checked>
													<span class="custom-checkbox"></span>
                                                    data2 </label>
                                                <label>
                                                    <input type="checkbox" id="cbdata3" checked>
													<span class="custom-checkbox"></span>
                                                    data3 </label>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div id="legendPlaceholder20">
                                </div>
                                <div id="toggle-chartContainer" style="width: 100%;height:300px; text-align: left;">
                                </div>
                        </div>
			
			</div>
            
          </div><!--/block-web--> 
        </div><!--/col-md-12--> 
      </div><!--/row-->
	  
	  
    </div><!--/page-content end--> 
@stop