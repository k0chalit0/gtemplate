@extends('master')
@section('content')
<div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Form Elements</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Default Form Elements</h3>
            </div>
            <div class="porlets-content">
              <form class="form-horizontal row-border" action="">
                <div class="form-group">
                  <label class="col-sm-3 control-label">Simplest Input</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control">
                  </div>
                </div><!--/form-group--> 

                <div class="form-group">
                  <label class="col-sm-3 control-label">Password Field</label>
                  <div class="col-sm-9">
                    <input type="password" class="form-control">
                  </div>
                </div><!--/form-group--> 
                
                <div class="form-group">
                  <label class="col-sm-3 control-label">Input with Placeholder</label>
                  <div class="col-sm-9">
                    <input type="text" placeholder="Placeholder" class="form-control">
                  </div>
                </div><!--/form-group--> 
                
                <div class="form-group">
                  <label class="col-sm-3 control-label">Disabled Input</label>
                  <div class="col-sm-9">
                    <input type="text" disabled="" placeholder="Disabled Input" class="form-control">
                  </div>
                </div><!--/form-group--> 
                
                <div class="form-group">
                  <label class="col-sm-3 control-label">Read only field</label>
                  <div class="col-sm-9">
                    <input type="text" value="Read only text goes here" readonly class="form-control">
                  </div>
                </div><!--/form-group--> 
                
                <div class="form-group">
                  <label class="col-sm-3 control-label">With pre-defined value</label>
                  <div class="col-sm-9">
                    <input type="text" value="http://" class="form-control">
                  </div>
                </div><!--/form-group--> 
                
                <div class="form-group">
                  <label class="col-sm-3 control-label">With max length</label>
                  <div class="col-sm-9">
                    <input type="text" placeholder="max 20 characters here" maxlength="20" class="form-control">
                  </div>
                </div><!--/form-group--> 
                
                <div class="form-group">
                  <label class="col-sm-3 control-label">Popover Input</label>
                  <div class="col-sm-9">
                    <input type="text" data-original-title="A Title" data-content="And here's some amazing content. It's very engaging. right?" data-toggle="popover" data-trigger="hover" placeholder="Popover Input" class="form-control popovers">
                  </div>
                </div><!--/form-group--> 
                
                <div class="form-group">
                  <label class="col-sm-3 control-label">Tooltip Input</label>
                  <div class="col-sm-9">
                    <input type="text" data-original-title="Tooltip text goes here. Tooltip text goes here." data-trigger="hover" class="form-control tooltips">
                  </div>
                </div><!--/form-group--> 
                
                <div class="form-group">
                  <label class="col-sm-3 control-label">Select Box</label>
                  <div class="col-sm-9">
                    <select id="source" class="form-control">
                      <optgroup label="Alaskan/Hawaiian Time Zone">
                      <option value="AK"> Argentina </option>
                      <option value="HI"> Aruba </option>
                      </optgroup>
                      <optgroup label="Pacific Time Zone">
                      <option value="CA"> Belarus </option>
                      <option value="NV"> Chile </option>
                      <option value="OR"> Ecuador </option>
                      <option value="WA"> Cyprus </option>
                      </optgroup>
                      <optgroup label="Mountain Time Zone">
                      <option value="AZ"> Finland </option>
                      <option value="CO"> Georgia </option>
                      <option value="ID"> Germany </option>
                      <option value="MT"> Honduras </option>
                      <option value="NE"> Georgia </option>
                      <option value="NM"> Ireland </option>
                      <option value="ND"> Japan </option>
                      <option value="UT"> Utah </option>
                      <option value="WY"> Wyoming </option>
                      </optgroup>
                      <optgroup label="Central Time Zone">
                      <option value="AL"> Kazakhstan </option>
                      <option value="AR"> Kuwait </option>
                      <option value="IL"> Illinois </option>
                      <option value="IA"> Libya </option>
                      <option value="KS"> Kansas </option>
                      <option value="KY"> Kentucky </option>
                      <option value="LA"> Macau </option>
                      <option value="MN"> Minnesota </option>
                      <option value="MS"> Mississippi </option>
                      <option value="MO"> Liberia </option>
                      <option value="OK"> Oklahoma </option>
                      <option value="SD"> South Dakota </option>
                      <option value="TX"> Texas </option>
                      <option value="TN"> Tennessee </option>
                      <option value="WI"> Wisconsin </option>
                      </optgroup>
                      <optgroup label="Eastern Time Zone">
                      <option value="CT"> Moldova </option>
                      <option value="DE"> Nauru </option>
                      <option value="FL"> New Zealand </option>
                      <option value="GA"> Georgia </option>
                      <option value="IN"> Indiana </option>
                      <option value="ME"> Nigeria </option>
                      <option value="MD"> Maryland </option>
                      <option value="MA"> Massachusetts </option>
                      <option value="MI"> Michigan </option>
                      <option value="NH"> New Hampshire </option>
                      <option value="NJ"> New Jersey </option>
                      <option value="NY"> New York </option>
                      <option value="NC"> North Carolina </option>
                      <option value="OH"> Ohio </option>
                      <option value="PA"> Pennsylvania </option>
                      <option value="RI"> Rhode Island </option>
                      <option value="SC"> South Carolina </option>
                      <option value="VT"> Vermont </option>
                      <option value="VA"> Virginia </option>
                      <option value="WV"> West Virginia </option>
                      </optgroup>
                    </select>
                  </div><!--/col-sm-9--> 
                </div><!--/form-group--> 
                
                <div class="form-group">
                  <label class="col-sm-3 control-label">Disabled Dropdown</label>
                  <div class="col-sm-9">
                    <select placeholder="Disabled Dropdown" disabled="" class="form-control">
                      <option> Lorem </option>
                      <option> Lorem ipsum dolor. </option>
                      <option> Amet, impedit aperiam? </option>
                      <option> Nemo, alias, quasi? </option>
                      <option> impedit, expedita. </option>
                    </select>
                  </div><!--/col-sm-9--> 
                </div><!--/form-group--> 
                
                <div class="form-group">
                  <label class="col-sm-3 control-label">Textarea</label>
                  <div class="col-sm-9">
                    <textarea class="form-control"></textarea>
                  </div>
                </div>
                <div class="bottom">
                  <button class="btn btn-primary" type="submit">Submit</button>
                  <button class="btn btn-default" type="button">Cancel</button>
                </div><!--/form-group-->
              </form>
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-6-->
        
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Basic Form Elements</h3>
            </div>
            <div class="porlets-content">
              <form class="form-horizontal row-border" action="">
                <div class="form-group">
                  <label class="col-sm-3 control-label">Before Text Field</label>
                  <div class="col-sm-9">
                    <div class="input-group"> <span class="input-group-addon">@</span>
                      <input type="text" placeholder="Username" class="form-control">
                    </div>
                  </div>
                </div><!--/form-group-->
                 
                <div class="form-group">
                  <label class="col-sm-3 control-label">After Text Field</label>
                  <div class="col-sm-9">
                    <div class="input-group">
                      <input type="text" class="form-control">
                      <span class="input-group-addon">.00</span> </div>
                  </div>
                </div><!--/form-group--> 
            
                <div class="form-group">
                  <label class="col-sm-3 control-label">Both</label>
                  <div class="col-sm-9">
                    <div class="input-group"> <span class="input-group-addon">$</span>
                      <input type="text" class="form-control">
                      <span class="input-group-addon">.00</span> </div>
                  </div>
                </div><!--form-group end--> 
                
                <div class="form-group">
                  <label class="col-sm-3 control-label">Checkbox</label>
                  <div class="col-sm-9">
                    <div class="input-group"> <span class="input-group-addon">
                      <input type="checkbox">
                      <span class="custom-checkbox"></span> </span>
                      <input type="text" class="form-control">
                    </div>
                  </div>
                </div><!--/form-group--> 
                
                <div class="form-group">
                  <label class="col-sm-3 control-label">Radio</label>
                  <div class="col-sm-9">
                    <div class="input-group"> <span class="input-group-addon">
                      <input type="radio">
                      <span class="custom-radio"></span> </span>
                      <input type="text" class="form-control">
                    </div>
                  </div>
                </div><!--/form-group--> 
                
                <div class="form-group">
                  <label class="col-sm-3 control-label">Buttons instead of text</label>
                  <div class="col-sm-9">
                    <div class="input-group">
                      <input type="text" class="form-control">
                      <div class="input-group-btn">
                        <button class="btn btn-danger" type="button">Go!</button>
                      </div>
                    </div>
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <label class="col-sm-3 control-label">Buttons with Dropdowns</label>
                  <div class="col-sm-9">
                    <div class="input-group">
                      <input type="text" class="form-control">
                      <div class="input-group-btn">
                        <button data-toggle="dropdown" class="btn btn-success dropdown-toggle" type="button">Action <i class="icon-caret-down"></i></button>
                        <ul class="dropdown-menu pull-right">
                          <li><a href="#">Action</a></li>
                          <li><a href="#">Another action</a></li>
                          <li><a href="#">Something else here</a></li>
                          <li class="divider"></li>
                          <li><a href="#">Separated link</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div><!--form-group--> 
                <div class="form-group">
                  <label class="col-sm-3 control-label">Segmentded Dropdown</label>
                  <div class="col-sm-9">
                    <div class="input-group">
                      <input type="text" class="form-control">
                      <div class="input-group-btn">
                        <button data-toggle="dropdown" class="btn btn-warning dropdown-toggle" type="button">Action <i class="icon-caret-down"></i></button>
                        <ul class="dropdown-menu pull-right">
                          <li><a href="#">Action</a></li>
                          <li><a href="#">Another action</a></li>
                          <li><a href="#">Something else here</a></li>
                          <li class="divider"></li>
                          <li><a href="#">Separated link</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div><!--/form-group--> 
                
                <div class="form-group">
                  <label class="col-sm-3 control-label">Inline Radio</label>
                  <div class="col-sm-9">
                    <label class="radio-inline">
                      <input type="radio" value="option1" id="inlineradio1">
                      <span class="custom-radio"></span> Option 1 </label>
                    <label class="radio-inline">
                      <input type="radio" value="option2" id="inlineradio2">
                      <span class="custom-radio"></span> Option 2 </label>
                    <label class="radio-inline">
                      <input type="radio" value="option3" id="inlineradio3">
                      <span class="custom-radio"></span> Option 3 </label>
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <label class="col-sm-3 control-label">Radio</label>
                  <div class="col-sm-9">
                    <div class="radio">
                      <label>
                        <input type="radio" checked="" value="option1" id="optionsRadios1" name="optionsRadios">
                        <span class="custom-radio"></span> Option one is this and that </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" value="option2" id="optionsRadios2" name="optionsRadios">
                        <span class="custom-radio"></span> Option two can be something else </label>
                    </div>
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <label class="col-sm-3 control-label">Inline Checkbox</label>
                  <div class="col-sm-9">
                    <label class="checkbox-inline">
                      <input type="checkbox" value="option1" id="inlinecheckbox1">
                      <span class="custom-checkbox"></span> Option 1 </label>
                    <label class="checkbox-inline">
                      <input type="checkbox" value="option2" id="inlinecheckbox2">
                      <span class="custom-checkbox"></span> Option 2 </label>
                    <label class="checkbox-inline">
                      <input type="checkbox" value="option3" id="inlinecheckbox3">
                      <span class="custom-checkbox"></span> Option 3 </label>
                  </div>
                </div><!--/form-group--> 
             
                <div class="form-group">
                  <label class="col-sm-3 control-label">Checkbox</label>
                  <div class="col-sm-9">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" value="">
                        <span class="custom-checkbox"></span> Option one is this and that </label>
                    </div>
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" value="">
                        <span class="custom-checkbox"></span> Option two can be something else </label>
                    </div>
                  </div>
                </div><!--/form-group-->
                <div class="bottom">
                  <button class="btn btn-primary" type="submit">Submit</button>
                  <button class="btn btn-default" type="button">Cancel</button>
                </div>
              </form>
            </div><!--/porlets-content--> 
          </div><!--/block-web--> 
        </div><!--/col-md-6--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Toggle Switches</h3>
            </div>
            <div class="porlets-content">
              <form class="form-horizontal">
                <div class="form-group">
                  <label class="col-sm-5 control-label">Default Switch</label>
                  <div class="col-sm-7 control-label">
                    <div class="toggle toggle-default"></div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Primary Switch</label>
                  <div class="col-sm-7 control-label">
                    <div class="toggle toggle-primary"></div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Success Switch</label>
                  <div class="col-sm-7 control-label">
                    <div class="toggle toggle-success"></div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Warning Switch</label>
                  <div class="col-sm-7 control-label">
                    <div class="toggle toggle-warning"></div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Danger Switch</label>
                  <div class="col-sm-7 control-label">
                    <div class="toggle toggle-danger"></div>
                  </div>
                </div>
              </form>
            </div><!--/porlets-content--> 
          </div><!--/block-web--> 
        </div><!--/col-md-6-->
        
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Checkboxes and Radios</h3>
            </div>
            <div class="porlets-content">
              <div class="row">
                <div class="col-md-6">
                  <div class="ckbox ckbox-default">
                    <input type="checkbox" value="1" id="checkboxDefault" checked="checked" />
                    <label for="checkboxDefault">Checkbox Default</label>
                  </div>
                  <div class="ckbox ckbox-primary">
                    <input type="checkbox" value="1" id="checkboxPrimary" checked="checked" />
                    <label for="checkboxPrimary">Checkbox Primary</label>
                  </div>
                  <div class="ckbox ckbox-warning">
                    <input type="checkbox" id="checkboxWarning" checked="checked" />
                    <label for="checkboxWarning">Checkbox Warning</label>
                  </div>
                  <div class="ckbox ckbox-success">
                    <input type="checkbox" id="checkboxSuccess" checked="checked" />
                    <label for="checkboxSuccess">Checkbox Success</label>
                  </div>
                  <div class="ckbox ckbox-danger">
                    <input type="checkbox" id="checkboxDanger" checked="checked" />
                    <label for="checkboxDanger">Checkbox Danger</label>
                  </div>
                  <div class="ckbox ckbox-default">
                    <input type="checkbox" id="checkboxDisabled" disabled="disabled" />
                    <label for="checkboxDisabled">Checkbox Disabled</label>
                  </div>
                </div><!-- /col-md-6 -->
                
                <div class="col-md-6">
                  <div class="rdio rdio-default">
                    <input type="radio" name="radio" id="radioDefault" value="1" checked="checked" />
                    <label for="radioDefault">Radio Default</label>
                  </div>
                  <div class="rdio rdio-primary">
                    <input type="radio" name="radio" value="2" id="radioPrimary" />
                    <label for="radioPrimary">Radio Primary</label>
                  </div>
                  <div class="rdio rdio-warning">
                    <input type="radio" name="radio" value="3" id="radioWarning" />
                    <label for="radioWarning">Radio Warning</label>
                  </div>
                  <div class="rdio rdio-success">
                    <input type="radio" name="radio" value="4" id="radioSuccess" />
                    <label for="radioSuccess">Radio Success</label>
                  </div>
                  <div class="rdio rdio-danger">
                    <input type="radio" name="radio" value="5" id="radioDanger" />
                    <label for="radioDanger">Radio Danger</label>
                  </div>
                  <div class="rdio rdio-default">
                    <input type="radio" name="radio" value="6" disabled="disabled" id="radioDisabled" />
                    <label for="radioDisabled">Radio Disabled</label>
                  </div>
                </div><!-- /col-md-6 -->
              </div><!--/row-->
            </div><!--/porlets-content--> 
          </div><!--/block-web--> 
        </div><!--/col-md-6--> 
      </div><!--/row-->
      
      
       <div class="row">
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Input Mask</h3>
            </div>
            <div class="porlets-content">
              <form class="form-horizontal row-border" action="">
                <div class="form-group lable-padd">
                  <label class="col-sm-3">Date</label>
                  <div class="col-sm-6">
                    <input type="text" data-inputmask="'alias': 'date'" class="form-control mask">
                  </div>
                  <div class="col-sm-3 left-align">
                    <p class="help-block">alias:date</p>
                  </div>
                </div>
                <div class="form-group lable-padd">
                  <label class="col-sm-3">Phone</label>
                  <div class="col-sm-6">
                    <input type="text" data-inputmask="'mask':'(999) 999-9999'" class="form-control mask">
                  </div>
                  <div class="col-sm-3 left-align">
                    <p class="help-block">(999) 999-9999</p>
                  </div>
                </div>
                <div class="form-group lable-padd">
                  <label class="col-sm-3">Phone + Ext</label>
                  <div class="col-sm-6">
                    <input type="text" data-inputmask="'mask':'(999) 999-9999 x999
                            99'" class="form-control mask">
                  </div>
                  <div class="col-sm-3 left-align">
                    <p class="help-block">(999) 999-9999 x99999</p>
                  </div>
                </div>
                <div class="form-group lable-padd">
                  <label class="col-sm-3">Int' Phone</label>
                  <div class="col-sm-6">
                    <input type="text" data-inputmask="'mask':'+33 999 999 999'" class="form-control mask">
                  </div>
                  <div class="col-sm-3 left-align">
                    <p class="help-block">+33 999 999 999</p>
                  </div>
                </div>
                <div class="form-group lable-padd">
                  <label class="col-sm-3">TaxID</label>
                  <div class="col-sm-6">
                    <input type="text" data-inputmask="'mask':'99-9999999'" class="form-control mask">
                  </div>
                  <div class="col-sm-3 left-align">
                    <p class="help-block">99-9999999</p>
                  </div>
                </div>
                <div class="form-group lable-padd">
                  <label class="col-sm-3">SSN</label>
                  <div class="col-sm-6">
                    <input type="text" data-inputmask="'mask':'999-99-9999'" class="form-control mask">
                  </div>
                  <div class="col-sm-3 left-align">
                    <p class="help-block">999-99-9999</p>
                  </div>
                </div>
                <div class="form-group lable-padd">
                  <label class="col-sm-3">Product Key</label>
                  <div class="col-sm-6">
                    <input type="text" data-inputmask="'mask':'a*-999-a999'" class="form-control mask">
                  </div>
                  <div class="col-sm-3 left-align">
                    <p class="help-block">a*-999-a999</p>
                  </div>
                </div>
                <div class="form-group lable-padd">
                  <label class="col-sm-3">Purchase Order</label>
                  <div class="col-sm-6">
                    <input type="text" data-inputmask="'mask':'PO: aaa-999-***'" class="form-control mask">
                  </div>
                  <div class="col-sm-3 left-align">
                    <p class="help-block">PO: aaa-999-***</p>
                  </div>
                </div>
                <div class="form-group lable-padd">
                  <label class="col-sm-3">Percent</label>
                  <div class="col-sm-6">
                    <input type="text" data-inputmask="'mask':'99%'" class="form-control mask">
                  </div>
                  <div class="col-sm-3 left-align">
                    <p class="help-block">99%</p>
                  </div>
                </div>
                <div class="form-group lable-padd">
                  <label class="col-sm-3">Currency</label>
                  <div class="col-sm-6">
                    <input type="text" data-inputmask="'mask':'999,999,999.99 $', 'numericInput' : true" class="form-control mask" style="text-align: right;">
                  </div>
                  <div class="col-sm-3 left-align">
                    <p class="help-block">right alignment</p>
                  </div>
                </div>
                <div class="form-group lable-padd">
                  <label class="col-sm-3">Currency 2</label>
                  <div class="col-sm-6">
                    <input type="text" data-inputmask="'mask':'$ 999,999,999.99', 'greedy' : false, 'rightAlignNumerics' : false" class="form-control mask">
                  </div>
                  <div class="col-sm-3 left-align">
                    <p class="help-block">left alignment</p>
                  </div>
                </div>
              </form>
            </div><!--/porlets-content--> 
          </div><!--/block-web--> 
        </div><!--/col-md-6-->
        
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Date Pickers</h3>
            </div>
            <div class="porlets-content">
              <form action="#" class="form-horizontal">
                <div class="form-group">
                  <label class="control-label col-md-4">Default Datepicker</label>
                  <div class="col-md-6 col-xs-11">
                    <input class="form-control form-control-inline input-medium default-date-picker"  size="16" type="text" value="" />
                    <span class="help-block">Select date</span> </div>
                </div><!--/form-group--> 

                <div class="form-group">
                  <label class="control-label col-md-3">Start with years viewMode</label>
                  <div class="col-md-6 col-xs-11">
                    <div data-date-viewmode="years" data-date-format="dd-mm-yyyy" data-date="12-02-2012"  class="input-append date dpYears">
                      <input type="text" readonly value="12-02-2012" size="30" class="form-control">
                      <span class="input-group-btn add-on">
                      <button class="btn btn-danger" type="button"><i class="fa fa-calendar"></i></button>
                      </span> </div>
                    <span class="help-block">Select date</span> </div>
                </div><!--/form-group--> 

                <div class="form-group">
                  <label class="control-label col-md-3">Months Only</label>
                  <div class="col-md-6 col-xs-11">
                    <div data-date-minviewmode="months" data-date-viewmode="years" data-date-format="mm/yyyy" data-date="102/2012"  class="input-append date dpMonths">
                      <input type="text" readonly value="02/2012" size="30" class="form-control">
                      <span class="input-group-btn add-on">
                      <button class="btn btn-danger" type="button"><i class="fa fa-calendar"></i></button>
                      </span> </div>
                    <span class="help-block">Select month only</span> </div>
                </div><!--/form-group--> 

                <div class="form-group">
                  <label class="control-label col-md-4">Date Range</label>
                  <div class="col-md-6">
                    <div class="input-group input-large" data-date="13/07/2013" data-date-format="mm/dd/yyyy">
                      <input type="text" class="form-control dpd1" name="from">
                      <span class="input-group-addon">To</span>
                      <input type="text" class="form-control dpd2" name="to">
                    </div>
                    <span class="help-block">Select date range</span> </div>
                </div><!--/form-group-->
              </form>

              <h4><strong>Color Pickers</strong> </h4>
              <hr>
              <form class="form-horizontal  tasi-form" action="#">
                <!--/form-group-->
                <div class="form-group">
                  <label class="control-label col-md-4">Default</label>
                  <div class="col-md-6">
                    <input type="text" class="colorpicker-default form-control" value="#8fff00" />
                  </div>
                </div><!--/form-group --> 

                <div class="form-group">
                  <label class="control-label col-md-4">RGBA</label>
                  <div class="col-md-6">
                    <input type="text" class="colorpicker-rgba form-control" value="rgb(0,194,255,0.78)" data-color-format="rgba" />
                  </div>
                </div><!--/form-group--> 

                <div class="form-group">
                  <label class="control-label col-md-4">As Component</label>
                  <div class="col-md-6 col-xs-11">
                    <div data-color-format="rgb" data-color="rgb(255, 146, 180)" class="input-append colorpicker-default color">
                      <input type="text" readonly value="" class="form-control">
                      <span class=" input-group-btn add-on">
                      <button class="btn btn-white" type="button" style="padding:10px"> <i style="background-color: rgb(234, 44, 70);"></i> </button>
                      </span> </div>
                  </div>
                </div><!--/form-group-->
              </form><!--/form-horizontal-->
              
            </div><!--/porlets-content--> 
          </div><!--/block-web--> 
        </div><!--/col-md-6-->
       </div><!--/row--> 
      
      
      <div class="row">
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Dropzone Multi-File Upload</h3>
            </div>
            <div class="porlets-content">
              <p>This is just a demo. Uploaded files are <strong>not</strong> stored. This does not handle your file uploads on the server. You have to implement the code to receive and store the file yourself.</p>
              <br />
              <form action="http://riaxe.com/file/post" class="dropzone">
                <div class="fallback">
                  <input name="file" type="file" multiple />
                </div>
              </form>
            </div><!--/porlets-content--> 
          </div><!--/block-web--> 
        </div><!--/col-md-6-->
        
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Form Wizard</h3>
            </div>
            <div class="porlets-content">
              <div id="progressWizard" class="basic-wizard">
                <ul class="nav nav-pills nav-justified">
                  <li><a href="#ptab1" data-toggle="tab"><span>Step 1:</span> Basic Info</a></li>
                  <li><a href="#ptab2" data-toggle="tab"><span>Step 2:</span> Product Info</a></li>
                  <li><a href="#ptab3" data-toggle="tab"><span>Step 3:</span> Payment</a></li>
                </ul>
                <div class="tab-content">
                  <div class="progress progress-striped active">
                    <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <div class="tab-pane" id="ptab1">
                    <form class="form">
                      <div class="form-group">
                        <label class="col-sm-4">Firstname</label>
                        <div class="col-sm-8">
                          <input type="text" name="firstname" class="form-control" />
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4">Lastname</label>
                        <div class="col-sm-8">
                          <input type="text" name="lastname" class="form-control" />
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4">Gender</label>
                        <div class="col-sm-8">
                          <div class="rdio rdio-primary">
                            <input type="radio" checked="checked" id="male2" value="m" name="radio">
                            <label for="male2">Male</label>
                          </div>
                          <div class="rdio rdio-primary">
                            <input type="radio" value="f" id="female2" name="radio">
                            <label for="female2">Female</label>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="tab-pane" id="ptab2">
                    <form class="form">
                      <div class="form-group">
                        <label class="col-sm-4">Product ID</label>
                        <div class="col-sm-5">
                          <input type="text" name="product_id" class="form-control" />
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4">Product Name</label>
                        <div class="col-sm-8">
                          <input type="text" name="product_name" class="form-control" />
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4">Category</label>
                        <div class="col-sm-4">
                          <select class="form-control">
                            <option value="">Choose One</option>
                            <option value="">3D Animation</option>
                            <option value="">Web Design</option>
                            <option value="">Software Engineering</option>
                          </select>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="tab-pane" id="ptab3">
                    <form class="form">
                      <div class="form-group">
                        <label class="col-sm-4">Card No</label>
                        <div class="col-sm-8">
                          <input type="text" name="cardno" class="form-control" />
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4">Expiration</label>
                        <div class="col-sm-4">
                          <select class="form-control">
                            <option value="">Month</option>
                            <option value="">January</option>
                            <option value="">February</option>
                            <option value="">March</option>
                            <option value="">...</option>
                          </select>
                        </div>
                        <div class="col-sm-4">
                          <select class="form-control">
                            <option value="">Year</option>
                            <option value="">2013</option>
                            <option value="">2014</option>
                            <option value="">2015</option>
                            <option value="">...</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4">CSV</label>
                        <div class="col-sm-4">
                          <input type="text" name="csv" class="form-control" />
                        </div>
                      </div>
                    </form>
                  </div>
                </div><!-- /tab-content -->
                
                <ul class="pager wizard">
                  <li class="previous"><a href="javascript:void(0)">Previous</a></li>
                  <li class="next"><a href="javascript:void(0)">Next</a></li>
                </ul>
              </div><!--/progressWizard-->
            </div><!--/porlets-content--> 
          </div><!--/block-web--> 
        </div><!--/col-md-6--> 
      </div><!--/row--> 
      
 
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">CKEditor</h3>
            </div>
            <div class="porlets-content">
              <div class="form">
                <form action="#" class="form-horizontal">
                  <div class="form-group">
                    <label class="col-sm-2 col-sm-2">CKEditor</label>
                    <div class="col-sm-10">
                      <textarea class="form-control ckeditor" name="editor1" rows="6"></textarea>
                    </div>
                  </div>
                </form>
              </div>
            </div><!--/porlets-content--> 
          </div><!--/block-web--> 
        </div><!--/col-md-12-->
       </div><!--/row--> 
       
       

    </div><!--/page-content end--> 

@stop