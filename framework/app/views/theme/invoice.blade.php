@extends('master')
@section('content')

  <div class="page-content">
    <div class="row">
            <div class="col-md-12">
              <h2><i class="fa fa-dollar"></i> Invoice</h2>
            </div><!--/col-md-12--> 
          </div><!--/row--> 
              
<div class="block-web ">
    <div class="row">
      <div class="col-sm-6">
        <h5>From</h5>
        <address>
      <strong>Twitter, Inc.</strong><br>
      795 Folsom Ave, Suite 600<br>
      San Francisco, CA 94107<br>
      <abbr title="Phone">P:</abbr> (123) 456-7890
    </address>
        </div><!-- /col-sm-6 -->
      
      <div class="col-sm-6 text-right">
        <h5 class="subtitle mb10">Invoice No.</h5>
        <h4 class="text-primary">INV-000567F7-00</h4>
        <h5 class="subtitle mb10">To</h5>
        <address>
      <strong>Twitter, Inc.</strong><br>
      795 Folsom Ave, Suite 600<br>
      San Francisco, CA 94107<br>
      <abbr title="Phone">P:</abbr> (123) 456-7890
    </address>
        <p><strong>Invoice Date:</strong> Marh 24, 2014</p>
        <p><strong>Due Date:</strong> March 26, 2014</p>
        </div><!--/ col-sm-6 -->
    </div><!--/ row -->
        
        <div class="table-responsive">
          <table class="table table-invoice">
            <thead>
              <tr>
                <th>Item</th>
                <th>Quantity</th>
                <th>Unit Price</th>
                <th>Total Price</th>
                </tr>
              </thead>
            <tbody>
              <tr>
                <td><div class="text-primary"><strong>Mobile App Developement</strong></div>
                  <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</small></td>
                <td>1</td>
                <td>$89.00</td>
                <td>$89.00</td>
                </tr>
              <tr>
                <td><div class="text-primary"><strong>Wodpress Customization</strong></div>
                  <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</small></td>
                <td>5</td>
                <td>$142.00</td>
                <td>$750.00</td>
                </tr>
              <tr>
                <td><div class="text-primary"><strong>Application Marketing</strong></div>
                  <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</small></td>
                <td>4</td>
                <td>$644.00</td>
                <td>$460.00</td>
                </tr>
              
              </tbody>
            </table>
        </div><!-- /table-responsive -->
        
        <table class="table table-total">
          <tbody>
            <tr>
              <td><strong>Sub Total :</strong></td>
              <td>$1350.00</td>
              </tr>
            <tr>
              <td><strong>VAT :</strong></td>
              <td>$70.00</td>
              </tr>
            <tr>
              <td><strong>TOTAL :</strong></td>
              <td>$1420.00</td>
              </tr>
            </tbody>
        </table>
        <div class="text-right">
          <button class="btn btn-primary"><i class="fa fa-dollar"></i> Make A Payment</button>
          <button class="btn btn-white"><i class="fa fa-print"></i> Print Invoice</button>
        </div>
        <div class="margin-top-20"></div>
        <div class="well"> Thank you for your business. Please make sure all cheques payable to <strong>Company, Inc.</strong> Account No. 65148374 </div>
</div>




  </div>  <!--/page-content end--> 
@stop