    @extends('master')
    @section('content')
    <div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Graph</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
          <div id="graph"></div>
          </div><!--/block-web--> 
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
          <div id="graph2"></div>
          </div><!--/block-web--> 
        </div><!--/col-md-12--> 
      </div><!--/row-->
     
      
    </div><!--/page-content end-->
    @stop
