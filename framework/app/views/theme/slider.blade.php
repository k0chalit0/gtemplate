@extends('master')
@section('content')

    <div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Sliders</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-4">
          <div class="block-web">
            <h3>Slider Colors</span></h3>
            <p>Basic slider with different color options</p>
            <div class="row-fluid">
              <div class="slider primary">
                <input type="text" class="slider-element" value="" data-slider-value="60" data-slider-step="1" data-slider-max="70" data-slider-orientation="horizontal" data-slider-selection="after" data-slider-tooltip="hide" />
              </div>
            </div>
            <div class="row-fluid">
              <div class="slider sucess">
                <input type="text" class="slider-element" value="" data-slider-max="70" data-slider-step="1" data-slider-value="50" data-slider-orientation="horizontal" data-slider-selection="after" data-slider-tooltip="hide" />
              </div>
            </div>
            <div class="row-fluid">
              <div class="slider info">
                <input type="text" class="slider-element" value="" data-slider-max="70" data-slider-step="1" data-slider-value="44" data-slider-orientation="horizontal" data-slider-selection="after" data-slider-tooltip="hide" />
              </div>
            </div>
            <div class="row-fluid">
              <div class="slider warning">
                <input type="text" class="slider-element" value="" data-slider-max="70" data-slider-step="1" data-slider-value="30" data-slider-orientation="horizontal" data-slider-selection="after" data-slider-tooltip="hide" />
              </div>
            </div>
            <div class="row-fluid">
              <div class="slider danger">
                <input type="text" class="slider-element" value="" data-slider-max="70" data-slider-step="1" data-slider-value="20" data-slider-orientation="horizontal" data-slider-selection="after" data-slider-tooltip="hide" />
              </div>
            </div>
          </div><!--/block-web--> 
        </div><!--/col-md-4-->
        
        <div class="col-md-4">
          <div class="block-web">
            <h3>Slider Orientation</span></h3>
            <p>vertical and horizontal orientaion <code>data-slider-orientation</code></p>
            <div class="slider sucess">
              <input type="text" data-slider-tooltip="hide" data-slider-selection="after" data-slider-orientation="vertical" data-slider-value="10" data-slider-step="1" data-slider-max="70" data-slider-min="-20" value="" class="slider-element" />
            </div>
            <div class="slider sucess">
              <input type="text" class="slider-element" value="" data-slider-min="1" data-slider-max="70" data-slider-step="1" data-slider-value="60" data-slider-orientation="vertical" data-slider-selection="after" data-slider-tooltip="hide" />
            </div>
            <div class="slider sucess">
              <input type="text" class="slider-element" value="" data-slider-min="1" data-slider-max="70" data-slider-step="1" data-slider-value="60" data-slider-orientation="vertical" data-slider-selection="after" data-slider-tooltip="hide" />
            </div>
            <div class="slider sucess">
              <input type="text" class="slider-element" value="" data-slider-min="1" data-slider-max="70" data-slider-step="1" data-slider-value="40" data-slider-orientation="vertical" data-slider-selection="after" data-slider-tooltip="hide" />
            </div>
            <div class="slider sucess">
              <input type="text" class="slider-element" value="" data-slider-min="1" data-slider-max="70" data-slider-step="1" data-slider-value="50" data-slider-orientation="vertical" data-slider-selection="after" data-slider-tooltip="hide" />
            </div>
          </div><!--/block-web--> 
        </div><!--/col-md-4-->
        
        <div class="col-md-4">
          <div class="block-web">
            <h3>Slider Flip fill</h3>
            <p>This option allows to change the side of the fill color which can be either before or after the knob</p>
            <div class="row-fluid">
              <div class="slider primary">
                <input type="text" class="slider-element" value="" data-slider-min="1" data-slider-max="70" data-slider-step="1" data-slider-value="60" data-slider-orientation="horizontal" data-slider-selection="after" data-slider-tooltip="hide" />
              </div>
            </div>
            <div class="row-fluid">
              <div class="slider primary">
                <input type="text" class="slider-element" value="" data-slider-min="1" data-slider-max="70" data-slider-step="1" data-slider-value="20" data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="hide" />
              </div>
            </div>
          </div><!--/block-web--> 
        </div><!--/col-md-4--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-4">
          <div class="block-web">
            <h3>Slider Values</span></h3>
            <p>This slider have the option to change <code>min</code>, <code>max</code> and <code>step</code> values to anything you want. By altering these values you could archive a level of smooth sliding</p>
            <div class="row-fluid">
              <div class="slider primary">
                <input type="text" class="slider-element" value="" tooltip='show' data-slider-min="1" data-slider-max="70" data-slider-step="1" data-slider-value="60" data-slider-orientation="horizontal" data-slider-selection="after" data-slider-tooltip="hide" />
              </div>
            </div>
            <div class="row-fluid">
              <div class="slider sucess">
                <input type="text" class="slider-element" value="" data-slider-min="1" data-slider-max="70" data-slider-step="1" data-slider-value="50" data-slider-orientation="horizontal" data-slider-selection="after" data-slider-tooltip="hide" />
              </div>
            </div>
            <div class="row-fluid">
              <div class="slider info">
                <input type="text" class="slider-element" value="" data-slider-min="1" data-slider-max="70" data-slider-step="1" data-slider-value="40" data-slider-orientation="horizontal" data-slider-selection="after" data-slider-tooltip="hide" />
              </div>
            </div>
          </div><!--/block-web--> 
        </div><!--/col-md-4-->
        
        <div class="col-md-4">
          <div class="block-web">
            <h3>Slider More Options</span></h3>
            <p>Sliders having the option for both vertical and horizontal orientaion by simply changing the <code>data-slider-selection</code></p>
            <div class="slider sucess">
              <input type="text" data-slider-tooltip="hide" data-slider-selection="before" data-slider-orientation="vertical" data-slider-value="30" data-slider-step="1" data-slider-max="70" data-slider-min="1" value="" class="slider-element" />
            </div>
            <div class="slider sucess">
              <input type="text" class="slider-element" value="" data-slider-min="1" data-slider-max="70" data-slider-step="1" data-slider-value="40" data-slider-orientation="vertical" data-slider-selection="before" data-slider-tooltip="hide" />
            </div>
            <div class="slider sucess">
              <input type="text" class="slider-element" value="" data-slider-min="1" data-slider-max="70" data-slider-step="1" data-slider-value="60" data-slider-orientation="vertical" data-slider-selection="before" data-slider-tooltip="hide" />
            </div>
            <div class="slider sucess">
              <input type="text" class="slider-element" value="" data-slider-min="1" data-slider-max="70" data-slider-step="1" data-slider-value="40" data-slider-orientation="vertical" data-slider-selection="before" data-slider-tooltip="hide" />
            </div>
            <div class="slider sucess">
              <input type="text" class="slider-element" value="" data-slider-min="1" data-slider-max="70" data-slider-step="1" data-slider-value="50" data-slider-orientation="vertical" data-slider-selection="before" data-slider-tooltip="hide" />
            </div>
          </div><!--/block-web--> 
        </div><!--/col-md-4-->
        
        <div class="col-md-4">
          <div class="block-web">
            <h3>Slider Range</span></h3>
            <div class="slider primary">
              <p>A cool demo that allows you to have a rang selector with two slider knobs and a value foramtor that can be currency or whatever you want.
                Attributes to change : <code>data-slider-value</code> </p>
              <br />
              <input type="text" data-slider-value="[250,450]" data-slider-step="5" data-slider-max="1000" data-slider-min="10" value="" class="slider-element" data-slider-selection="after" />
            </div>
          </div><!--/block-web--> 
        </div><!--/col-md-4--> 
        
      </div><!--/row--> 
      
    </div><!--/page-content end--> 
@stop