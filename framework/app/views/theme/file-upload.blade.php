@extends('master')
@section('content')
<div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>File Upload</h2>
        </div>
        <!--/col-md-12--> 
      </div>
      <!--/row-->
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">File Upload</h3>
            </div>
            <div class="porlets-content">
              <h4>Supports cross-domain, chunked and resumable file uploads and client-side image resizing.
                Works with any server-side platform (PHP, Python, Ruby on Rails, Java, Node.js, Go etc.) that supports standard HTML form file uploads.</h4>
              <br>
              <form id="fileupload" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
                <!-- Redirect browsers with JavaScript disabled to the origin page -->
                <noscript>
                <input type="hidden" name="redirect" value="http://blueimp.github.io/jQuery-File-Upload/">
                </noscript>
                <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                <div class="row fileupload-buttonbar">
                  <div class="col-lg-7"> 
                    <!-- The fileinput-button span is used to style the file input field as button --> 
                    <span class="btn btn-success fileinput-button"> <i class="glyphicon glyphicon-plus"></i> <span>Add files...</span>
                    <input type="file" name="files[]" multiple>
                    </span>
                    <button type="submit" class="btn btn-primary start"> <i class="glyphicon glyphicon-upload"></i> <span>Start upload</span> </button>
                    <button type="reset" class="btn btn-warning cancel"> <i class="glyphicon glyphicon-ban-circle"></i> <span>Cancel upload</span> </button>
                    <button type="button" class="btn btn-danger delete"> <i class="glyphicon glyphicon-trash"></i> <span>Delete</span> </button>
                    <input type="checkbox" class="toggle">
                    <span class="custom-checkbox"></span> 
                    <!-- The global file processing state --> 
                    <span class="fileupload-process"></span> </div>
                  <!-- The global progress state -->
                  <div class="col-lg-5 fileupload-progress fade"> 
                    <!-- The global progress bar -->
                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                      <div class="progress-bar progress-bar-success" style="width:0%;"> </div>
                    </div>
                    <!-- The extended global progress state -->
                    <div class="progress-extended"> &nbsp; </div>
                  </div>
                </div>
                <!-- The table listing the files available for upload/download -->
                <table role="presentation" class="table table-striped">
                  <tbody class="files">
                  </tbody>
                </table>
              </form>
              <br>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title">Demo Notes</h3>
                </div>
                <div class="panel-body">
            <ul>
                <li>The maximum file size for uploads in this demo is <strong>5 MB</strong> (default file size is unlimited).</li>
                <li>Only image files (<strong>JPG, GIF, PNG</strong>) are allowed in this demo (by default there is no file type restriction).</li>
                <li>Uploaded files will be deleted automatically after <strong>5 minutes</strong> (demo setting).</li>
                <li>You can <strong>drag &amp; drop</strong> files from your desktop on this webpage (see <a href="https://github.com/blueimp/jQuery-File-Upload/wiki/Browser-support">Browser support</a>).</li>
                <li>Please refer to the <a href="https://github.com/blueimp/jQuery-File-Upload">project website</a> and <a href="https://github.com/blueimp/jQuery-File-Upload/wiki">documentation</a> for more information.</li>
                <li>Built with Twitter's <a href="http://twitter.github.com/bootstrap/">Bootstrap</a> CSS framework and Icons from <a href="http://glyphicons.com/">Glyphicons</a>.</li>
            </ul>
        </div>
              </div>
            </div>
            
          </div>
          <!--/block-web--> 
        </div>
        <!--/col-md-12--> 
      </div>
      <!--/row--> 
      
      
    </div>
    <!--/page-content end--> 
@stop