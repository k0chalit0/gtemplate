@extends('master')
@section('content')
<div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Buttons</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Buttons</h3>
            </div>
            <div class="porlets-content">
              <div class="table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Button</th>
                      <th>btn-lg Button</th>
                      <th>Small Button</th>
                      <th>Small Mini</th>
                      <th>Disabled Button</th>
                      <th>Button with Icon</th>
                      <th>Split Button</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><a href="#" class="btn btn-default">Default</a></td>
                      <td><a href="#" class="btn btn-default btn-lg">Default</a></td>
                      <td><a href="#" class="btn btn-default btn-sm">Default</a></td>
                      <td><a href="#" class="btn btn-default btn-xs">Default</a></td>
                      <td><a href="#" class="btn btn-default disabled">Default</a></td>
                      <td><a href="#" class="btn btn-default"><i class="fa fa-cog"></i> Default</a></td>
                      <td><div class="btn-group"> <a href="#" class="btn btn-default">Default</a> <a href="#" data-toggle="dropdown" class="btn btn-default dropdown-toggle"><span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li> <a href="#">Action</a> </li>
                            <li> <a href="#">Another action</a> </li>
                            <li> <a href="#">Something else here</a> </li>
                            <li class="divider"></li>
                            <li> <a href="#">Separated link</a> </li>
                          </ul>
                        </div>
                        
                        <!-- /btn-group --></td>
                    </tr>
                    <tr>
                      <td><a href="#" class="btn btn-primary">Primary</a></td>
                      <td><a href="#" class="btn btn-primary btn-lg">Primary</a></td>
                      <td><a href="#" class="btn btn-primary btn-sm">Primary</a></td>
                      <td><a href="#" class="btn btn-primary btn-xs">Primary</a></td>
                      <td><a href="#" class="btn btn-primary disabled">Primary</a></td>
                      <td><a href="#" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Primary</a></td>
                      <td><div class="btn-group"> <a href="#" class="btn btn-primary">Primary</a> <a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li> <a href="#">Action</a> </li>
                            <li> <a href="#">Another action</a> </li>
                            <li> <a href="#">Something else here</a> </li>
                            <li class="divider"></li>
                            <li> <a href="#">Separated link</a> </li>
                          </ul>
                        </div>
                        
                        <!-- /btn-group --></td>
                    </tr>
                    <tr>
                      <td><a href="#" class="btn btn-info">Info</a></td>
                      <td><a href="#" class="btn btn-info btn-lg">Info</a></td>
                      <td><a href="#" class="btn btn-info btn-sm">Info</a></td>
                      <td><a href="#" class="btn btn-info btn-xs">Info</a></td>
                      <td><a href="#" class="btn btn-info disabled">Info</a></td>
                      <td><a href="#" class="btn btn-info"><i class="fa fa-sign-in"></i> Info</a></td>
                      <td><div class="btn-group"> <a href="#" class="btn btn-info">Info</a> <a href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li> <a href="#">Action</a> </li>
                            <li> <a href="#">Another action</a> </li>
                            <li> <a href="#">Something else here</a> </li>
                            <li class="divider"></li>
                            <li> <a href="#">Separated link</a> </li>
                          </ul>
                        </div>
                        
                        <!-- /btn-group --></td>
                    </tr>
                    <tr>
                      <td><a href="#" class="btn btn-success">Success</a></td>
                      <td><a href="#" class="btn btn-success btn-lg">Success</a></td>
                      <td><a href="#" class="btn btn-success btn-sm">Success</a></td>
                      <td><a href="#" class="btn btn-success btn-xs">Success</a></td>
                      <td><a href="#" class="btn btn-success disabled">Success</a></td>
                      <td><a href="#" class="btn btn-success"><i class="fa fa-check"></i> Success</a></td>
                      <td><div class="btn-group"> <a href="#" class="btn btn-success">Success</a> <a href="#" data-toggle="dropdown" class="btn btn-success dropdown-toggle"><span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li> <a href="#">Action</a> </li>
                            <li> <a href="#">Another action</a> </li>
                            <li> <a href="#">Something else here</a> </li>
                            <li class="divider"></li>
                            <li> <a href="#">Separated link</a> </li>
                          </ul>
                        </div>
                        
                        <!-- /btn-group --></td>
                    </tr>
                    <tr>
                      <td><a href="#" class="btn btn-warning">Warning</a></td>
                      <td><a href="#" class="btn btn-warning btn-lg">Warning</a></td>
                      <td><a href="#" class="btn btn-warning btn-sm">Warning</a></td>
                      <td><a href="#" class="btn btn-warning btn-xs">Warning</a></td>
                      <td><a href="#" class="btn btn-warning disabled">Warning</a></td>
                      <td><a href="#" class="btn btn-warning"><i class="fa fa-sign-out"></i> Warning</a></td>
                      <td><div class="btn-group"> <a href="#" class="btn btn-warning">Warning</a> <a href="#" data-toggle="dropdown" class="btn btn-warning dropdown-toggle"><span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li> <a href="#">Action</a> </li>
                            <li> <a href="#">Another action</a> </li>
                            <li> <a href="#">Something else here</a> </li>
                            <li class="divider"></li>
                            <li> <a href="#">Separated link</a> </li>
                          </ul>
                        </div>
                        
                        <!-- /btn-group --></td>
                    </tr>
                    <tr>
                      <td><a href="#" class="btn btn-danger">Danger</a></td>
                      <td><a href="#" class="btn btn-danger btn-lg">Danger</a></td>
                      <td><a href="#" class="btn btn-danger btn-sm">Danger</a></td>
                      <td><a href="#" class="btn btn-danger btn-xs">Danger</a></td>
                      <td><a href="#" class="btn btn-danger disabled">Danger</a></td>
                      <td><a href="#" class="btn btn-danger"><i class="fa fa-trash-o"></i> Danger</a></td>
                      <td><div class="btn-group"> <a href="#" class="btn btn-danger">Danger</a> <a href="#" data-toggle="dropdown" class="btn btn-danger dropdown-toggle"><span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li> <a href="#">Action</a> </li>
                            <li> <a href="#">Another action</a> </li>
                            <li> <a href="#">Something else here</a> </li>
                            <li class="divider"></li>
                            <li> <a href="#">Separated link</a> </li>
                          </ul>
                        </div>
                        
                        <!-- /btn-group --></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div><!--/block-web--> 
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Basic Buttons</h3>
            </div>
            <div class="porlets-content">
              <p class="alert alert-info"> Use any of the available button classes to quickly create a styled button. </p>
              <a class="btn btn-default" href="#">Default</a> <a class="btn btn-primary" href="#">Primary</a> <a class="btn btn-success" href="#">Success</a> <a class="btn btn-info" href="#">Info</a> <a class="btn btn-warning" href="#">Warning</a> <a class="btn btn-danger" href="#">Danger</a> <a class="btn btn-default disabled" href="#">Disabled</a> <a class="btn btn-link" href="#">Link</a> 
              </div><!--/porlets-content--> 
          </div><!--/block-web--> 
        </div><!--/col-md-6-->
        
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Label Buttons</h3>
            </div>
            <div class="porlets-content">
              <ul class="demo-btns">
                <li> <a class="btn btn-labeled btn-success" href="#"> <span class="btn-label"><i class="glyphicon glyphicon-ok"></i></span>Success </a> </li>
                <li> <a class="btn btn-labeled btn-danger" href="#"> <span class="btn-label"><i class="glyphicon glyphicon-remove"></i></span>Cancel </a> </li>
                <li> <a class="btn btn-labeled btn-warning" href="#"> <span class="btn-label"><i class="glyphicon glyphicon-bookmark"></i></span>Bookmark </a> </li>
                <li> <a class="btn btn-labeled btn-primary" href="#"> <span class="btn-label"><i class="glyphicon glyphicon-camera"></i></span>Camera </a> </li>
                <li> <a class="btn btn-labeled btn-default" href="#"> <span class="btn-label"><i class="glyphicon glyphicon-chevron-left"></i></span>Left </a> </li>
                <li> <a class="btn btn-labeled btn-default" href="#"> <span class="btn-label"><i class="glyphicon glyphicon-chevron-right"></i></span> Right </a> </li>
                <li> <a class="btn btn-labeled btn-success" href="#"> <span class="btn-label"><i class="glyphicon glyphicon-thumbs-up"></i></span>Thumbs
                  Up </a> </li>
                <li> <a class="btn btn-labeled btn-danger" href="#"> <span class="btn-label"><i class="glyphicon glyphicon-thumbs-down"></i></span>Thumbs
                  Down </a> </li>
                <li> <a class="btn btn-labeled btn-info" href="#"> <span class="btn-label"><i class="glyphicon glyphicon-refresh"></i></span>Refresh </a> </li>
                <li> <a class="btn btn-labeled btn-danger" href="#"> <span class="btn-label"><i class="glyphicon glyphicon-trash"></i></span>Trash </a> </li>
                <li> <a href="#" class="btn btn-success btn-labeled"> <span class="btn-label"><i class="glyphicon glyphicon-info-sign"></i></span>Info Web </a> </li>
              </ul>
            </div><!--/porlets-content--> 
          </div><!--/block-web--> 
        </div><!--/col-md-6--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Button Sizes</h3>
            </div>
            <div class="porlets-content">
              <p> Large buttons to attract call to action <code> .btn .btn-lg </code> </p>
              <a class="btn btn-primary btn-lg" href="#">Large button</a>&nbsp;<a class="btn btn-default btn-lg" href="#">Large button</a>
              <hr>
              <p> The Default button <code> .btn .btn-default </code> </p>
              <a class="btn btn-primary" href="#">Default button</a>&nbsp;<a class="btn btn-default" href="#">Default button</a>
              <hr>
              <p> Small button for elegance <code> .btn .btn-sm </code> </p>
              <a class="btn btn-primary btn-sm" href="#">Small button</a>&nbsp;<a class="btn btn-default btn-sm" href="#">Small button</a>
              <hr>
              <p> Extra small button for added info <code> .btn .btn-xs </code> </p>
              <a class="btn btn-primary btn-xs" href="#">Mini button</a>&nbsp;<a class="btn btn-default btn-xs" href="#">Mini button</a> 
              </div><!--/porlets-content--> 
          </div><!--/block-web--> 
        </div><!--/col-md-6-->
        
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Drop Down buttons </h3>
            </div>
            <div class="porlets-content">
              <p> Button group with dropup/dropdown toggle <code> .btn-group </code> </p>
              <div class="btn-group">
                <button class="btn btn-primary"> Drop Down </button>
                <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle"> <span class="caret"></span> </button>
                <ul class="dropdown-menu">
                  <li> <a href="#">Action</a> </li>
                  <li> <a href="#">Another action</a> </li>
                  <li> <a href="#">Something else here</a> </li>
                  <li class="divider"></li>
                  <li> <a href="#">Separated link</a> </li>
                </ul>
              </div>
              <div class="btn-group dropup">
                <button class="btn btn-default"> Drop Up </button>
                <button data-toggle="dropdown" class="btn btn-default dropdown-toggle"> <span class="caret"></span> </button>
                <ul class="dropdown-menu">
                  <li> <a href="#">Action</a> </li>
                  <li> <a href="#">Another action</a> </li>
                  <li> <a href="#">Something else here</a> </li>
                  <li class="divider"></li>
                  <li> <a href="#">Separated link</a> </li>
                </ul>
              </div>
              <hr class="simple">
              <p> Default button dropdowns <code> .dropdown-toggle </code> </p>
              <div class="btn-group">
                <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle"> Action <span class="caret"></span> </button>
                <ul class="dropdown-menu">
                  <li> <a href="#">Action</a> </li>
                  <li> <a href="#">Another action</a> </li>
                  <li> <a href="#">Something else here</a> </li>
                  <li class="divider"></li>
                  <li> <a href="#">Separated link</a> </li>
                </ul>
              </div>
              <div class="btn-group">
                <button data-toggle="dropdown" class="btn btn-default dropdown-toggle"> Action <span class="caret"></span> </button>
                <ul class="dropdown-menu">
                  <li> <a href="#">Action</a> </li>
                  <li> <a href="#">Another action</a> </li>
                  <li> <a href="#">Something else here</a> </li>
                  <li class="divider"></li>
                  <li> <a href="#">Separated link</a> </li>
                </ul>
              </div>
              <hr class="simple">
              <p> Smaller button dropdowns <code> .btn-sm .dropdown-toggle </code> </p>
              <div class="btn-group">
                <button data-toggle="dropdown" class="btn btn-primary btn-sm dropdown-toggle"> Action <span class="caret"></span> </button>
                <ul class="dropdown-menu">
                  <li> <a href="#">Action</a> </li>
                  <li> <a href="#">Another action</a> </li>
                  <li> <a href="#">Something else here</a> </li>
                  <li class="divider"></li>
                  <li> <a href="#">Separated link</a> </li>
                </ul>
              </div>
              <div class="btn-group">
                <button data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle"> Action <span class="caret"></span> </button>
                <ul class="dropdown-menu">
                  <li> <a href="#">Action</a> </li>
                  <li> <a href="#">Another action</a> </li>
                  <li> <a href="#">Something else here</a> </li>
                  <li class="divider"></li>
                  <li> <a href="#">Separated link</a> </li>
                </ul>
              </div>
              <hr class="simple">
              <p> Extra small button dropdowns <code> .btn-xs .dropdown-toggle </code> </p>
              <div class="btn-group">
                <button data-toggle="dropdown" class="btn btn-primary btn-xs dropdown-toggle"> Action <span class="caret"></span> </button>
                <ul class="dropdown-menu">
                  <li> <a href="#">Action</a> </li>
                  <li> <a href="#">Another action</a> </li>
                  <li> <a href="#">Something else here</a> </li>
                  <li class="divider"></li>
                  <li> <a href="#">Separated link</a> </li>
                </ul>
              </div>
              <div class="btn-group">
                <button data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle"> Action <span class="caret"></span> </button>
                <ul class="dropdown-menu">
                  <li> <a href="#">Action</a> </li>
                  <li> <a href="#">Another action</a> </li>
                  <li> <a href="#">Something else here</a> </li>
                  <li class="divider"></li>
                  <li> <a href="#">Separated link</a> </li>
                </ul>
              </div>
            </div><!--/porlets-content--> 
          </div><!--/block-web--> 
        </div><!--/col-md-6--> 
      </div><!--/row--> 
      
      
      <div class="row">
      <div class="col-md-7">
        <div class="block-web">
          <div class="header">
            <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
            <h3 class="content-header">Button Groups</h3>
          </div>
          <div class="porlets-content">
			<p> Group a series of buttons together on a single line with the button group. Wrap a series of buttons with <code> .btn </code> in <code> .btn-group </code> . </p>
            <div class="row">
              <div class="col-md-12">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width:30%">Horizontal Group</th>
                      <th style="width:25%">With Icons</th>
                      <th style="width:45%">Multiple Button Groups</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><div class="btn-group">
                          <button class="btn btn-default" type="button"> Left </button>
                          <button class="btn btn-default" type="button"> Middle </button>
                          <button class="btn btn-default" type="button"> Right </button>
                        </div></td>
                      <td><div class="btn-group">
                          <button class="btn btn-default" type="button"> <i class="fa fa-align-left"></i> </button>
                          <button class="btn btn-default" type="button"> <i class="fa fa-align-center"></i> </button>
                          <button class="btn btn-default" type="button"> <i class="fa fa-align-right"></i> </button>
                          <button class="btn btn-default" type="button"> <i class="fa fa-align-justify"></i> </button>
                        </div></td>
                      <td><div class="btn-toolbar">
                          <div class="btn-group">
                            <button class="btn btn-default" type="button"> 1 </button>
                            <button class="btn btn-default" type="button"> 2 </button>
                            <button class="btn btn-default" type="button"> 3 </button>
                            <button class="btn btn-default" type="button"> 4 </button>
                          </div>
                          <div class="btn-group">
                            <button class="btn btn-default" type="button"> 5 </button>
                            <button class="btn btn-default" type="button"> 6 </button>
                            <button class="btn btn-default" type="button"> 7 </button>
                          </div>
                          <div class="btn-group">
                            <button class="btn btn-default" type="button"> 8 </button>
                          </div>
                        </div></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <hr class="simple">
            <p> Make a set of buttons appear vertically stacked rather than horizontally by putting it in <code> .btn-group-vertical </code> . </p>
            <div class="row">
              <div class="col-md-12">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width:25%">Vertical Group</th>
                      <th style="width:25%">With Dropdown</th>
                      <th style="width:25%">With Icons</th>
                      <th style="width:25%">Multiple Button Groups</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><div class="btn-group-vertical">
                          <button class="btn btn-default" type="button"> Top </button>
                          <button class="btn btn-default" type="button"> Middle </button>
                          <button class="btn btn-default" type="button"> Bottom </button>
                        </div></td>
                      <td><div class="btn-group-vertical">
                          <button class="btn btn-primary" type="button"> Button 1 </button>
                          <button class="btn btn-primary" type="button"> Button 2 </button>
                          <button class="btn btn-primary" type="button"> Button 3 </button>
                          <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button"> Dropdown <i class="fa fa-caret-down"></i> </button>
                          <ul class="dropdown-menu">
                            <li> <a href="#">Dropdown link</a> </li>
                            <li> <a href="#">Dropdown link</a> </li>
                          </ul>
                        </div></td>
                      <td><div class="btn btn-group-vertical"> <a href="#" class="btn btn-default"><i class="fa fa-align-left"></i></a> <a href="#" class="btn btn-default"><i class="fa fa-align-center"></i></a> <a href="#" class="btn btn-default"><i class="fa fa-align-right"></i></a> <a href="#" class="btn btn-default"><i class="fa fa-align-justify"></i></a> </div></td>
                      <td><div class="btn-toolbar">
                          <div class="btn-group-vertical">
                            <button class="btn btn-primary" type="button"> Page 1 </button>
                            <button class="btn btn-primary" type="button"> Page 2 </button>
                            <button class="btn btn-primary" type="button"> Page 3 </button>
                            <button class="btn btn-primary" type="button"> Page 4 </button>
                          </div>
                        </div></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div><!--/porlets-content--> 
        </div><!--/block-web--> 
      </div><!--/col-md-7-->
      
      <div class="col-md-5">
        <div class="block-web">
          <div class="header">
            <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
            <h3 class="content-header">Block Buttons </h3>
          </div>
          <div class="porlets-content">
            <p> Block buttons <code> .btn .btn-block </code> </p>
            <div class="well">
              <button class="btn btn-primary btn-lg btn-block" type="button"> Block level button </button>
              <button class="btn btn-default btn-sm btn-block" type="button"> Block level small button </button>
              <button class="btn btn-default btn-xs btn-block" type="button"> Block level extra small button </button>
            </div>
            <hr class="simple">
            <p> Block group buttons <code> .btn-group .btn-group-justified </code> </p>
            <div class="well">
              <div class="btn-group btn-group-justified"> <a class="btn btn-default" href="#">Left</a> <a class="btn btn-default" href="#">Middle</a> <a class="btn btn-default" href="#">Right</a> </div>

            </div>
          </div><!--/porlets-content--> 
        </div><!--/block-web--> 
      </div><!--/col-md-5--> 
    </div><!--/row--> 

  </div><!--/page-content end--> 
@stop