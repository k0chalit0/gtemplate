@extends('master')
@section('content')
<div class="page-content">
<div class="row">
<form action="index.html" class="lockscreen">
  <div class="logo text-center">
    <h2 class="semi-bold ">WebPro Admin</h2>
  </div>
  <div> <img src="images/avatar1.jpg">
    <div>
      <h3><i class="fa fa-user fa-2x text-muted pull-right hidden-xs"></i>John Doe <small><i class="fa fa-lock text-muted"></i> &nbsp;Locked</small></h3>
      <p class="text-muted"> <a href="mailto:info@riaxe.com">info@riaxe.com</a> </p>
      <div class="input-group">
        <input type="password" placeholder="Password" class="form-control">
        <div class="input-group-btn">
          <button type="submit" class="btn btn-primary"> <i class="fa fa-key"></i> </button>
        </div>
      </div>
      <p class="no-margin margin-top-5"> Logged as someone else? <a href="login.html"> Click here</a> </p>
    </div>
  </div>
  <p class="font-xs margin-top-5 text-center"> Copyright WebProAdmin 2014. </p>
</form>
</div>
</div>
@stop