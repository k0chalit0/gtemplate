@extends('master')
@section('content')

 <div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Nestable</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
          <p>Drag &amp; drop hierarchical list with mouse and touch compatibility (jQuery plugin)</p>
            <menu id="nestable-menu">
                <button data-action="expand-all" type="button" class="btn btn-success">Expand All</button>
                <button data-action="collapse-all" type="button" class="btn btn-danger">Collapse All</button>
              </menu>
            <div class="row">
        <div class="col-md-4">
     
            <div class="cf nestable-lists">
                <h4>Nestable List 1 </h4>
                <div class="dd" id="nestable">
                  <ol class="dd-list">
                    <li class="dd-item" data-id="1">
                      <div class="dd-handle">Item 1</div>
                    </li>
                    <li class="dd-item" data-id="2">
                      <div class="dd-handle">Item 2</div>
                      <ol class="dd-list">
                        <li class="dd-item" data-id="3">
                          <div class="dd-handle">Item 3</div>
                        </li>
                        <li class="dd-item" data-id="4">
                          <div class="dd-handle">Item 4</div>
                        </li>
                        <li class="dd-item" data-id="5">
                          <div class="dd-handle">Item 5</div>
                          <ol class="dd-list">
                            <li class="dd-item" data-id="6">
                              <div class="dd-handle">Item 6</div>
                            </li>
                            <li class="dd-item" data-id="7">
                              <div class="dd-handle">Item 7</div>
                            </li>
                            <li class="dd-item" data-id="8">
                              <div class="dd-handle">Item 8</div>
                            </li>
                          </ol>
                        </li>
                        <li class="dd-item" data-id="9">
                          <div class="dd-handle">Item 9</div>
                        </li>
                        <li class="dd-item" data-id="10">
                          <div class="dd-handle">Item 10</div>
                        </li>
                      </ol>
                    </li>
                    <li class="dd-item" data-id="11">
                      <div class="dd-handle">Item 11</div>
                    </li>
                    <li class="dd-item" data-id="12">
                      <div class="dd-handle">Item 12</div>
                    </li>
                  </ol>
                </div>
              </div>
            
        
        </div><!--/col-md-4--> 
        
        <div class="col-md-4">
       
          <div class="cf nestable-lists">
                <h4>Nestable List 2 </h4>
                <div class="dd" id="nestable2">
                  <ol class="dd-list">
                    <li class="dd-item" data-id="13">
                      <div class="dd-handle">Item 13</div>
                    </li>
                    <li class="dd-item" data-id="14">
                      <div class="dd-handle">Item 14</div>
                    </li>
                    <li class="dd-item" data-id="15">
                      <div class="dd-handle">Item 15</div>
                      <ol class="dd-list">
                        <li class="dd-item" data-id="16">
                          <div class="dd-handle">Item 16</div>
                        </li>
                        <li class="dd-item" data-id="17">
                          <div class="dd-handle">Item 17</div>
                        </li>
                        <li class="dd-item" data-id="18">
                          <div class="dd-handle">Item 18</div>
                        </li>
                      </ol>
                    </li>
                  </ol>
                </div>
              </div>
        </div><!--/col-md-4--> 
        
         <div class="col-md-4">
          
          <div class="cf nestable-lists">
                <h4>Draggable Handles</h4>
                <p>If you're clever with your CSS and markup this can be achieved without any JavaScript changes.</p>
                <div class="dd" id="nestable3">
                  <ol class="dd-list">
                    <li class="dd-item dd3-item" data-id="13">
                      <div class="dd-handle dd3-handle"></div>
                      <div class="dd3-content">Item 13</div>
                    </li>
                    <li class="dd-item dd3-item" data-id="14">
                      <div class="dd-handle dd3-handle"></div>
                      <div class="dd3-content">Item 14</div>
                    </li>
                    <li class="dd-item dd3-item" data-id="15">
                      <div class="dd-handle dd3-handle"></div>
                      <div class="dd3-content">Item 15</div>
                      <ol class="dd-list">
                        <li class="dd-item dd3-item" data-id="16">
                          <div class="dd-handle dd3-handle"></div>
                          <div class="dd3-content">Item 16</div>
                        </li>
                        <li class="dd-item dd3-item" data-id="17">
                          <div class="dd-handle dd3-handle"></div>
                          <div class="dd3-content">Item 17</div>
                        </li>
                        <li class="dd-item dd3-item" data-id="18">
                          <div class="dd-handle dd3-handle"></div>
                          <div class="dd3-content">Item 18</div>
                        </li>
                      </ol>
                    </li>
                  </ol>
                </div>
              </div>
        </div><!--/col-md-4--> 
      </div><!--/row-->
      
          </div><!--/block-web--> 
        </div><!--/col-md-12--> 
      </div><!--/row-->
     
      
      
      
    </div><!--/page-content end--> 
@stop