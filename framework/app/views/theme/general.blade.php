@extends('master')
@section('content')

<div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>General</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Pagination</h3>
            </div>
            <div class="porlets-content"> <strong>Default Pagination</strong>
              <p>Simple pagination inspired by Rdio, great for apps and search results.</p>
              <ul class="pagination margin-top-5">
                <li class="disabled"><a href="#"><i class="fa fa-angle-left"></i></a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
              </ul>
              <div class="margin-top-5"></div>
              <strong>Disabled and active states</strong>
              <p>Links are split to each other by adding a class of <code>.pagination-split</code></p>
              <ul class="pagination pagination-split margin-top-5">
                <li class="disabled"><a href="#"><i class="fa fa-angle-left"></i></a></li>
                <li><a href="#">1</a></li>
                <li class="active"><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
              </ul>
              <div class="margin-top-5"></div>
              <strong>Sizing</strong>
              <p>Add <code>.pagination-lg</code> or <code>.pagination-sm</code> for additional sizes.</p>
              <ul class="pagination pagination-lg margin-top-5">
                <li class="disabled"><a href="#"><i class="fa fa-angle-left"></i></a></li>
                <li><a href="#">1</a></li>
                <li class="active"><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
              </ul>
            </div><!--/porlets-content--> 
          </div><!--/block-web--> 
        </div><!--/col-md-6-->
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Progress Bars</h3>
            </div>
            <div class="porlets-content"> <strong>Basic Progress</strong>
              <p>Default progress bar</p>
              <div class="progress">
                <div style="width: 40%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar progress-bar-success"> <span class="sr-only">40% Complete (success)</span> </div>
              </div>
              <div class="progress">
                <div style="width: 20%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info"> <span class="sr-only">20% Complete</span> </div>
              </div>
              <div class="progress">
                <div style="width: 60%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="progress-bar progress-bar-warning"> <span class="sr-only">60% Complete (warning)</span> </div>
              </div>
              <div class="progress">
                <div style="width: 80%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="80" role="progressbar" class="progress-bar progress-bar-danger"> <span class="sr-only">80% Complete</span> </div>
              </div>
              <br>
              <strong>Striped Progress</strong>
              <p>Uses a gradient to create a striped effect. Not available in IE8</p>
              <div class="progress progress-striped">
                <div style="width: 40%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar progress-bar-success"> <span class="sr-only">40% Complete (success)</span> </div>
              </div>
              <br>
              <strong>Animated Progress</strong>
              <p>Add <code>.active</code> to <code>.progress-striped</code> to animate the stripes right to left. Not available in all versions of IE.</p>
              <div class="progress progress-striped active">
                <div style="width: 40%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar progress-bar-primary"> <span class="sr-only">40% Complete (success)</span> </div>
              </div>
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-6--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Pagers</h3>
            </div>
            <div class="porlets-content"> <strong>Default</strong>
              <p>By default, the pager centers links.</p>
              <ul class="pager">
                <li><a href="#"><i class="fa fa-angle-left"></i> Previous</a></li>
                <li><a href="#">Next <i class="fa fa-angle-right"></i></a></li>
              </ul>
              <div class="clearfix margin-top-20"></div>
              <strong>Aligned Links</strong>
              <p>Alternatively, you can align each link to the sides:</p>
              <ul class="pager">
                <li class="previous"><a href="#">← Older</a></li>
                <li class="next"><a href="#">Newer →</a></li>
              </ul>
              <div class="clearfix margin-top-20"></div>
              <strong>Optional Disabled State</strong>
              <p>Pager links also use the general <code>.disabled</code> utility class from the pagination.</p>
              <ul class="pager">
                <li class="previous disabled"><a href="#">← Older</a></li>
                <li class="next"><a href="#">Newer →</a></li>
              </ul>
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-6-->
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Labels & Badges</h3>
            </div>
            <div class="porlets-content"> <strong>Labels</strong>
              <p>Add any of the below mentioned modifier classes to change the appearance of a label.</p>
              <span class="label label-default">Default</span> <span class="label label-primary">Primary</span> <span class="label label-success">Success</span> <span class="label label-info">Info</span> <span class="label label-warning">Warning</span> <span class="label label-danger">Danger</span> <br>
              <br>
              <strong>Badges</strong>
              <p>Easily highlight new or unread items by adding below to links, navs, and more.</p>
              <span class="badge">42</span> <span class="badge badge-primary">10</span> <span class="badge badge-success">28</span> <span class="badge badge-warning">90</span> <span class="badge badge-danger">33</span> <span class="badge badge-info">21</span> <br>
              <br>
              <strong>Badge Example</strong>
              <p>Adapts to active nav states.</p>
              <ul class="nav nav-pills nav-stacked">
                <li class="active"> <a href="#"> <span class="badge pull-right">42</span> Home </a> </li>
                <li><a href="#">Profile</a></li>
                <li> <a href="#"> <span class="badge pull-right">3</span> Messages </a> </li>
              </ul>
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-6--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Alerts</h3>
            </div>
            <div class="porlets-content">
              <div class="alert alert-success"> <strong>Well done!</strong> You successfully read this important alert message. </div>
              <div class="alert alert-info"> <strong>Heads up!</strong> This alert needs your attention, but it's not super important. </div>
              <div class="alert alert-warning"> <strong>Warning!</strong> Better check yourself, you're not looking too good. </div>
              <div class="alert alert-danger"> <strong>Oh snap!</strong> Change a few things up and try submitting again. </div>
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-6-->
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Heading</h3>
            </div>
            <div class="porlets-content">
            <p>All HTML headings, <code>&lt;h1&gt;</code> through <code>&lt;h6&gt;</code>, are available. <code>.h1</code> through <code>.h6</code> classes are also available, for when you want to match the font styling of a heading but still want your text to be displayed inline.</p>
              <h1>Example heading <span class="label label-default">New</span></h1>
              <h2>Example heading <span class="label label-default">New</span></h2>
              <h3>Example heading <span class="label label-default">New</span></h3>
              <h4>Example heading <span class="label label-default">New</span></h4>
              <h5>Example heading <span class="label label-default">New</span></h5>
              <h6>Example heading <span class="label label-default">New</span></h6>
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-6--> 
      </div><!--/row--> 
      
      
      <div class="row">
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Tooltips</h3>
            </div>
            <div class="porlets-content">
            <p>When using tooltips on elements within a <code>.btn-group</code> or an <code>.input-group</code>, you'll have to specify the option <code>container: 'body'</code> (documented below) to avoid unwanted side effects (such as the element growing wider and/or losing its rounded corners when the tooltip is triggered).</p>
             <div class="tooltip-example">
             	<button data-original-title="Tooltip on left" type="button" class="btn btn-default tooltips" data-toggle="tooltip" data-placement="left" title="">Tooltip Left</button>
                <button type="button" class="btn btn-default tooltips" data-toggle="tooltip" data-placement="right" title="" data-original-title="Tooltip on right">Tooltip Right</button>
                <button type="button" class="btn btn-default tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Tooltip on top">Tooltip Top</button>
                <button type="button" class="btn btn-default tooltips" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Tooltip on bottom">Tooltip Bottom</button>
              </div>
             
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-6-->
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Popovers</h3>
            </div>
            <div class="porlets-content">
            <p>Sometimes you want to add a popover to a hyperlink that wraps multiple lines. The default behavior of the popover plugin is to center it horizontally and vertically. Add <code>white-space: nowrap;</code> to your anchors to avoid this.</p>
             <div class="popover-example">
                <button title="" data-original-title="" type="button" class="btn btn-default popovers" data-container="body" data-toggle="popover" data-placement="left" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                  Popover Left
                </button>
                <button title="" data-original-title="" type="button" class="btn btn-default popovers" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                  Popover Top
                </button>
                <button title="" data-original-title="" type="button" class="btn btn-default popovers" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                  Popover Bottom
                </button>
                <button title="" data-original-title="" type="button" class="btn btn-default popovers" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                  Popover Right
                </button>
              </div> 
              
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-6--> 
      </div><!--/row--> 
      
       <div class="row">
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Modal</h3>
            </div>
            <div class="porlets-content">
            <p>Toggle a modal via JavaScript by clicking the button below. It will slide down and fade in from the top of the page.</p>
           <!-- Button trigger modal -->
            <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
              Launch demo modal
            </button>
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                  </div>
                  <div class="modal-body">
                    ...
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                  </div>
                </div>
              </div>
            </div>
             
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-6-->
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Popovers</h3>
            </div>
            <div class="porlets-content">
             <p>Modals have two optional sizes, available via modifier classes to be placed on a <code>.modal-dialog</code>.</p>
             <!-- Large modal -->
             
            <button class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Large modal</button>
            
            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    <h4 id="myModalLabel" class="modal-title">Large modal</h4>
                  </div>
                  <div class="modal-body">
                    ...
                  </div>
                  <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                    <button class="btn btn-primary" type="button">Save changes</button>
                  </div>
                </div>
              </div>
            </div>
            
            <!-- Small modal -->
            <button class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">Small modal</button>
            
            <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">
                  <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    <h4 id="myModalLabel" class="modal-title">Small modal</h4>
                  </div>
                  <div class="modal-body">
                    ...
                  </div>
                  <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                    <button class="btn btn-primary" type="button">Save changes</button>
                  </div>
                </div>
              </div>
            </div>

              
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-6--> 
      </div><!--/row--> 
      
      
    </div><!--/page-content end--> 
@stop