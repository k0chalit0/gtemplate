@extends('master')
@section('content')

<div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Media Gallery</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="table-responsive">
              <table class="table table-striped table-bordered media-table">
                <thead>
                  <tr>
                    <th style="width: 150px">Image</th>
                    <th>Description</th>
                    <th>Date</th>
                    <th>File info</th>
                    <th class="text-center">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><div class="thumbnail">
                        <div class="thumbnail-view"> <a href="{{ asset('images/gallery/img1-lg.jpg') }}" class="thumbnail-view-hover image-zoom"> </a> <img width="125" alt="Gallery Image" src="{{ asset('images/gallery/img1-sm.jpg') }}"> </div>
                      </div>
                      <!-- /.thumbnail --></td>
                    <td><a title="" href="javascript:;">Donec quam felis</a>
                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p></td>
                    <td>Mar 24, 2014. 12:30</td>
                    <td class="file-info"><span><strong>Size:</strong> 215 Kb</span> <br>
                      <span><strong>Format:</strong> .jpg</span> <br>
                      <span><strong>Dimensions:</strong> 120 x 120</span></td>
                    <td class="text-center"><button class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></button>
                      &nbsp;
                      <button class="btn btn-xs btn-danger"><i class="fa fa-times"></i></button></td>
                  </tr>
                  <tr>
                    <td><div class="thumbnail">
                        <div class="thumbnail-view"> <a href="{{ asset('images/gallery/img2-lg.jpg') }}" class="thumbnail-view-hover image-zoom"> </a> <img width="125" alt="Gallery Image" src="{{ asset('images/gallery/img2-sm.jpg') }}"> </div>
                      </div>
                      <!-- /.thumbnail --></td>
                    <td><a title="" href="javascript:;">Donec quam felis</a>
                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p></td>
                    <td>Mar 24, 2014. 12:35</td>
                    <td class="file-info"><span><strong>Size:</strong> 215 Kb</span> <br>
                      <span><strong>Format:</strong> .jpg</span> <br>
                      <span><strong>Dimensions:</strong> 120 x 120</span></td>
                    <td class="text-center"><button class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></button>
                      &nbsp;
                      <button class="btn btn-xs btn-danger"><i class="fa fa-times"></i></button></td>
                  </tr>
                  <tr>
                    <td><div class="thumbnail">
                        <div class="thumbnail-view"> <a href="{{ asset('images/gallery/img3-lg.jpg') }}" class="thumbnail-view-hover image-zoom"> </a> <img width="125" alt="Gallery Image" src="{{ asset('images/gallery/img3-sm.jpg') }}"> </div>
                      </div>
                      <!-- /.thumbnail --></td>
                    <td><a title="" href="javascript:;">Donec quam felis</a>
                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p></td>
                    <td>Mar 24, 2014. 12:40</td>
                    <td class="file-info"><span><strong>Size:</strong> 215 Kb</span> <br>
                      <span><strong>Format:</strong> .jpg</span> <br>
                      <span><strong>Dimensions:</strong> 120 x 120</span></td>
                    <td class="text-center"><button class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></button>
                      &nbsp;
                      <button class="btn btn-xs btn-danger"><i class="fa fa-times"></i></button></td>
                  </tr>
                  <tr>
                    <td><div class="thumbnail">
                        <div class="thumbnail-view"> <a href="{{ asset('images/gallery/img4-lg.jpg') }}" class="ui-lightbox thumbnail-view-hover image-zoom"> </a> <img width="125" alt="Gallery Image" src="{{ asset('images/gallery/img4-sm.jpg') }}"> </div>
                      </div>
                      <!-- /.thumbnail --></td>
                    <td><a title="" href="javascript:;">Donec quam felis</a>
                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p></td>
                    <td>Mar 24, 2014. 12:45</td>
                    <td class="file-info"><span><strong>Size:</strong> 215 Kb</span> <br>
                      <span><strong>Format:</strong> .jpg</span> <br>
                      <span><strong>Dimensions:</strong> 120 x 120</span></td>
                    <td class="text-center"><button class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></button>
                      &nbsp;
                      <button class="btn btn-xs btn-danger"><i class="fa fa-times"></i></button></td>
                  </tr>
                </tbody>
              </table>
            </div><!--/table-responsive--> 
          </div><!--/block-web--> 
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-3 col-sm-6">
          <div class="thumbnail">
            <div class="thumbnail-view"> <a href="{{ asset('images/gallery/img1-lg.jpg') }}" class="thumbnail-view-hover ui-lightbox  image-zoom"></a> <img src="{{ asset('images/gallery/img1-sm.jpg') }}" style="width: 100%" alt="Gallery Image" /> </div>
            <div class="caption">
              <h3>Thumbnail label</h3>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a href="javascript:;" class="btn btn-primary btn-sm btn-sm">Button</a> <a href="javascript:;" class="btn btn-danger btn-sm btn-sm">Button</a></p>
            </div>
            <div class="thumbnail-footer">
              <div class="pull-left"> <a href="javascript:;"><i class="fa fa-thumbs-up"></i>505</a> <a href="javascript:;"><i class="fa fa-thumbs-down"></i> 34</a> </div>
              <div class="pull-right"> <a href="javascript:;"><i class="fa fa-clock-o"></i> 2 days ago</a> </div>
            </div>
          </div>
        </div><!-- /col-md-3-->
        
        <div class="col-md-3 col-sm-6">
          <div class="thumbnail">
            <div class="thumbnail-view"> <a href="{{ asset('images/gallery/img2-lg.jpg') }}" class="thumbnail-view-hover ui-lightbox  image-zoom"></a> <img src="{{ asset('images/gallery/img2-sm.jpg') }}" style="width: 100%" alt="Gallery Image" /> </div>
            <div class="caption">
              <h3>Thumbnail label</h3>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a href="javascript:;" class="btn btn-primary btn-sm">Button</a> <a href="javascript:;" class="btn btn-danger btn-sm">Button</a></p>
            </div>
            <div class="thumbnail-footer">
              <div class="pull-left"> <a href="javascript:;"><i class="fa fa-thumbs-up"></i> 425</a> <a href="javascript:;"><i class="fa fa-thumbs-down"></i> 36</a> </div>
              <div class="pull-right"> <a href="javascript:;"><i class="fa fa-clock-o"></i> 2 days ago</a> </div>
            </div>
          </div>
        </div><!-- /col-md-3-->
        
        <div class="col-md-3 col-sm-6">
          <div class="thumbnail">
            <div class="thumbnail-view"> <a href="{{ asset('images/gallery/img3-lg.jpg') }}" class="thumbnail-view-hover ui-lightbox image-zoom"></a> <img src="{{ asset('images/gallery/img3-sm.jpg') }}" style="width: 100%" alt="Gallery Image" /> </div>
            <div class="caption">
              <h3>Thumbnail label</h3>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a href="javascript:;" class="btn btn-primary btn-sm">Button</a> <a href="javascript:;" class="btn btn-danger btn-sm">Button</a></p>
            </div>
            <div class="thumbnail-footer">
              <div class="pull-left"> <a href="javascript:;"><i class="fa fa-thumbs-up"></i> 387</a> <a href="javascript:;"><i class="fa fa-thumbs-down"></i> 24</a> </div>
              <div class="pull-right"> <a href="javascript:;"><i class="fa fa-clock-o"></i> 2 days ago</a> </div>
            </div>
          </div>
        </div><!-- /col-md-3 -->
        
        <div class="col-md-3 col-sm-6">
          <div class="thumbnail">
            <div class="thumbnail-view"> <a href="{{ asset('images/gallery/img4-lg.jpg') }}" class="thumbnail-view-hover ui-lightbox  image-zoom"></a> <img src="{{ asset('images/gallery/img4-sm.jpg') }}" style="width: 100%" alt="Gallery Image" /> </div>
            <div class="caption">
              <h3>Thumbnail label</h3>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a href="javascript:;" class="btn btn-primary btn-sm">Button</a> <a href="javascript:;" class="btn btn-danger btn-sm">Button</a></p>
            </div>
            <div class="thumbnail-footer">
              <div class="pull-left"> <a href="javascript:;"><i class="fa fa-thumbs-up"></i>346</a> <a href="javascript:;"><i class="fa fa-thumbs-down"></i> 46</a> </div>
              <div class="pull-right"> <a href="javascript:;"><i class="fa fa-clock-o"></i> 2 days ago</a> </div>
            </div>
          </div>
        </div><!-- /col-md-3 --> 
      </div><!-- /row --> 
      
      <br />
      <h3 class="heading">Image Only</h3>
      <div class="row">
        <div class="col-md-3 col-sm-6">
          <div class="thumbnail">
            <div class="thumbnail-view"> <a href="{{ asset('images/gallery/img1-lg.jpg') }}" class="thumbnail-view-hover ui-lightbox  image-zoom"></a> <img src="{{ asset('images/gallery/img1-sm.jpg') }}" style="width: 100%" alt="Gallery Image" /> </div>
          </div>
        </div><!-- /col-md-3 -->
        
        <div class="col-md-3 col-sm-6">
          <div class="thumbnail">
            <div class="thumbnail-view"> <a href="{{ asset('images/gallery/img2-lg.jpg') }}" class="thumbnail-view-hover ui-lightbox  image-zoom"></a> <img src="{{ asset('images/gallery/img2-sm.jpg') }}" style="width: 100%" alt="Gallery Image" /> </div>
          </div>
        </div><!-- /col-md-3 -->
        
        <div class="col-md-3 col-sm-6">
          <div class="thumbnail">
            <div class="thumbnail-view"> <a href="{{ asset('images/gallery/img3-lg.jpg') }}" class="thumbnail-view-hover ui-lightbox  image-zoom"></a> <img src="{{ asset('images/gallery/img3-sm.jpg') }}" style="width: 100%" alt="Gallery Image" /> </div>
          </div>
        </div><!-- /col-md-3 -->
        
        <div class="col-md-3 col-sm-6">
          <div class="thumbnail">
            <div class="thumbnail-view"> <a href="{{ asset('images/gallery/img4-lg.jpg') }}" class="thumbnail-view-hover ui-lightbox  image-zoom"></a> <img src="{{ asset('images/gallery/img4-sm.jpg') }}" style="width: 100%" alt="Gallery Image" /> </div>
          </div>
        </div><!-- /col-md-3 --> 
        
      </div><!-- /row --> 
    </div><!--/page-content end--> 
@stop