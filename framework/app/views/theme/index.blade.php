@extends('master')
@section('content')
  <div class="page-content">
    <div class="row">
      <div class="col-md-12">
        <h2><i class="fa fa-dashboard"></i> Dashboard</h2>
      </div><!--/col-md-12--> 
    </div><!--/row--> 
   
     <div class="row">
      <div class="col-md-6">
        <div class="block-web">
          <h3 class="content-header"> Your Current Setup
            <div data-toggle="buttons" class="button-group pull-right"> <a class="btn active small border-gray right-margin" href="javascript:;"> <span class="button-content">
              <input type="radio" name="radio-toggle-1">
              Today </span> </a> <a class="btn small border-gray" href="javascript:;"> <span class="button-content">
              <input type="radio" name="radio-toggle-1">
            Weekly </span> </a> </div>
          </h3>
          <table class="table margin-top-20" width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="fa-border"><button class="btn btn-danger padd-adj" type="button">12</button>
              Total Posts</td>
              <td class="fa-border"><button class="btn btn-primary padd-adj" type="button">28</button>
              Social Reactions</td>
              <td class="fa-border"><button class="btn btn-info padd-adj" type="button">15</button>
              Social Reactions</td>
            </tr>
            <tr>
              <td class="fa-border"><button class="btn btn-default padd-adj" type="button">20</button>
              Comments</td>
              <td class="fa-border"><button class="btn btn-success padd-adj" type="button">40</button>
              New User</td>
              <td class="fa-border"><button class="btn btn-warning padd-adj" type="button">30</button>
              New User</td>
            </tr>
          </table>
          <div class="bottom">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><strong>Hourly Visit</strong></td>
                <td><div id="work-progress1"></div></td>
                <td width="100">&nbsp;</td>
                <td><strong>Bounce Rate</strong></td>
                <td><div id="work-progress2"></div></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="block-web">
          <h3 class="content-header text-center">Message Board</h3>
          <div class="panel-icon-add"><i class="fa fa-thumb-tack"></i></div>
          <p class="text-center">Please be informed that server 3 will be offline from <strong>11:00 AM</strong> to <strong>12:00 AM</strong></p>
        </div><!--/block-web-->
      </div><!--/col-md-3-->
      <div class="col-md-3">
        <div class="panel">
          <div class="weather-bg">
            <div class="panel-body">
              <div class="row">
                <div class="col-xs-6"> <i class="fa fa-cloud"></i> California </div>
                <div class="col-xs-6">
                  <div class="degree"> 34° </div>
                </div>
              </div>
            </div>
          </div>
          <div class="weather-category">
            <ul>
              <li class="active">
                <h5>humidity</h5>
              57% </li>
              <li>
                <h5>precip</h5>
              2.50 in </li>
              <li>
                <h5>winds</h5>
              10 mph </li>
            </ul>
          </div><!--/weather-category-->
        </div><!--/panel-->
      </div><!--/col-md-3-->
    </div><!--/row-->
   
    <div class="row">
      <div class="col-md-6">
        <div class="block-web">
          <h3 class="content-header"> Quick Stats
            <div class="button-group pull-right" data-toggle="buttons"> <a href="javascript:;" class="btn active small border-gray right-margin"> <span class="button-content">
              <input type="radio" name="radio-toggle-1">
              Top this week </span> </a> <a href="javascript:;" class="btn small border-gray right-margin"> <span class="button-content">
              <input type="radio" name="radio-toggle-1">
              Refering </span> </a> <a href="javascript:;" class="btn small border-gray"> <span class="button-content">
              <input type="radio" name="radio-toggle-1">
              Others </span> </a> 
            </div><!--/button-group-->
          </h3>
          <div class="custom-bar-chart">
            <ul class="y-axis">
              <li><span>100</span></li>
              <li><span>80</span></li>
              <li><span>60</span></li>
              <li><span>40</span></li>
              <li><span>20</span></li>
              <li><span>0</span></li>
            </ul>
            <div class="bar">
              <div class="value tooltips" data-original-title="30%" data-toggle="tooltip" data-placement="top">30%</div>
              <div class="title">SUN</div>
            </div><!--/bar-->
            <div class="bar">
              <div class="value tooltips bar-bg-color" data-original-title="50%" data-toggle="tooltip" data-placement="top">50%</div>
              <div class="title">MON</div>
            </div><!--/bar-->
            <div class="bar ">
              <div class="value tooltips" data-original-title="40%" data-toggle="tooltip" data-placement="top">40%</div>
              <div class="title">TUE</div>
            </div><!--/bar-->
            <div class="bar ">
              <div class="value tooltips" data-original-title="80%" data-toggle="tooltip" data-placement="top">80%</div>
              <div class="title">WED</div>
            </div><!--/bar-->
            <div class="bar">
              <div class="value tooltips bar-bg-color" data-original-title="70%" data-toggle="tooltip" data-placement="top">70%</div>
              <div class="title">THU</div>
            </div><!--/bar-->
            <div class="bar ">
              <div class="value tooltips" data-original-title="50%" data-toggle="tooltip" data-placement="top">50%</div>
              <div class="title">FRI</div>
            </div><!--/bar-->
            <div class="bar">
              <div class="value tooltips" data-original-title="40%" data-toggle="tooltip" data-placement="top">40%</div>
              <div class="title">SUT</div>
            </div><!--/bar-->
            <div class="bar">
              <div class="value tooltips" data-original-title="35%" data-toggle="tooltip" data-placement="top">35%</div>
              <div class="title">SUN</div>
            </div><!--/bar-->
          </div><!--/custom-bar-chart-->
        </div><!--/block-web-->
      </div><!--/col-md-6-->
      
      <div class="col-md-6">
        <div class="block-web">
          <div class="header">
            <h3 class="content-header">Progress to wards goals</h3>
          </div>
          <div class="porlets-content margin-top-20">
            <div class="knob-content-box">
              <h4><strong>Have 100,000 Users</strong></h4>
              <br>
            <small>Estimated time 3 months</small></div>
            <div class="knob-box">
              <input type="text" class="dial" value="42%" data-angleoffset="-125" data-anglearc="250" data-width="140" data-fgcolor="#e74949" data-thickness=".15" />
            </div>
            <div class="knob-content-box">
              <h4><strong>Lower bounce rate to 10%</strong></h4>
              <br>
            <small>Estimated time 3 months</small> </div>
            <div class="knob-box">
              <input type="text" class="dial" data-cursor="true" value="12%" data-width="140" data-fgcolor="#ffa200" data-thickness=".15" />
            </div>
          </div>
          <div class="bottom">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><strong class="bottom-content-box">Today</strong>
                  <div class="progress progress-hieght">
                    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"> 60% </div>
                  </div>
                  <strong class="bottom-content-box">Yesterday</strong>
                  <div class="progress progress-hieght">
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;"> 40% </div>
                </div></td>
                <td width="20%"></td>
                <td><strong class="bottom-content-box">Today</strong>
                  <div class="progress progress-hieght">
                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;"> 80% </div>
                  </div>
                  <strong class="bottom-content-box">Yesterday</strong>
                  <div class="progress progress-hieght">
                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;"> 30% </div>
                </div></td>
              </tr>
            </table>
          </div><!--/bottom-->
        </div><!--/block-web-->
      </div><!--/col-md-6-->
    </div><!--/row end-->
    
    
    <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="header">
                <h3 class="content-header">Graph</h3>
              </div>
            <div class="porlets-content">
			<div id="graph"></div>
            </div><!--/porlets-content--> 
         </div><!--/block-web--> 
       </div><!--/col-md-12--> 
    </div><!--/row-->
   
    <div class="row">
      <div class="col-md-4">
        <div class="block-web green-bg-color">
          <h3 class="content-header">Most Important Task</h3>
            <div class="porlets-content">
             <ul class="list-group task-list no-margin collapse in">
              <li class="list-group-item green-light-bg-color">
                <label class="label-checkbox inline">
                  <input type="checkbox" checked="" class="task-finish">
                <span class="custom-checkbox"></span> </label>
              New frontend layout <span class="pull-right"> <a class="task-del" href="#"><i class="fa fa-times"></i></a> </span> </li>
              <li class="list-group-item">
                <label class="label-checkbox inline">
                  <input type="checkbox" class="task-finish">
                <span class="custom-checkbox"></span> </label>
              Windows Phone App <span class="pull-right"> <a class="task-del" href="#"><i class="fa fa-times"></i></a> </span> </li>
              <li class="list-group-item">
                <label class="label-checkbox inline">
                  <input type="checkbox" class="task-finish">
                <span class="custom-checkbox"></span> </label>
              Mobile Development <span class="pull-right"> <a class="task-del" href="#"><i class="fa fa-times"></i></a> </span> </li>
              <li class="list-group-item">
                <label class="label-checkbox inline">
                  <input type="checkbox" class="task-finish">
                <span class="custom-checkbox"></span> </label>
              SEO Optimisation <span class="label label-warning m-left-xs">1:30PM</span> <span class="pull-right"> <a class="task-del" href="#"><i class="fa fa-times"></i></a> </span> </li>
              <li class="list-group-item">
                <label class="label-checkbox inline">
                  <input type="checkbox" class="task-finish">
                <span class="custom-checkbox"></span> </label>
              Windows Phone App <span class="pull-right"> <a class="task-del" href="#"><i class="fa fa-times"></i></a> </span> </li>
              <li class="list-group-item">
                <label class="label-checkbox inline">
                  <input type="checkbox" class="task-finish">
                <span class="custom-checkbox"></span> </label>
              Bug Fixes <span class="label label-danger m-left-xs">4:40PM</span> <span class="pull-right"> <a class="task-del" href="#"><i class="fa fa-times"></i></a> </span> </li>
              <form class="form-inline margin-top-10" role="form">
                <input type="text" class="form-control" placeholder="Enter tasks here...">
                <button class="btn btn-default btn-warning pull-right" type="submit"><i class="fa fa-plus"></i> Add Task</button>
              </form>
            </ul><!-- /list-group --> 
          </div><!--/porlets-content-->
        </div><!--/block-web-->
      </div><!--/col-md-4-->
     

      <div class="col-md-4">
        <div class="block-web">
          <h3 class="content-header">Note</h3>
          <div class="block widget-notes">
            <div contenteditable="true" class="paper"> Send e-mail to supplier<br>
              <s>Conference at 4 pm.</s><br>
              <s>Order a pizza</s><br>
              <s>Buy flowers</s><br>
              Buy some coffee.<br>
              Dinner at Plaza.<br>
              Take Alex for walk.<br>
              Buy some coffee.<br>
            </div>
          </div><!--/widget-notes-->
        </div><!--/block-web-->
      </div><!--/col-md-4 -->
      
      <div class="col-md-4">
        <div class="kalendar"></div>
        <div class="list-group"> <a class="list-group-item" href="#"> <span class="badge bg-danger">7:50</span> Consectetuer </a> <a class="list-group-item" href="#"> <span class="badge bg-success">10:30</span> Lorem ipsum dolor sit amet </a> <a class="list-group-item" href="#"> <span class="badge bg-light">11:40</span> Consectetuer adipiscing </a> 
        </div><!--/calendar end-->
      </div><!--/col-md-4 end-->
    </div><!--/row end-->
  </div>  <!--/page-content end--> 
  @stop