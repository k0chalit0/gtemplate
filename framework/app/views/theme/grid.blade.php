@extends('master')
@section('content')
<div class="page-content">
    <div class="row">
      <div class="col-lg-12">
            
        <p>Bootstrap includes a responsive, mobile first fluid grid system that appropriately scales up to 12 columns as the device or viewport size increases. It includes predefined classes for easy layout options, as well as powerful mixins for generating more semantic layouts.</p>
        </div>
      <div class="col-lg-12">
        <div class="panel">
          <div class="panel-body">col-lg-12</div>
        </div>
        </div>
      </div>
  
    <div class="row">
      <div class="col-lg-6">
        <div class="panel">
          <div class="panel-body">col-lg-6</div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="panel">
          <div class="panel-body">col-lg-6</div>
        </div>
      </div>
      </div>
    <div class="row">
      <div class="col-lg-4">
        <div class="panel">
          <div class="panel-body">col-lg-4</div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="panel">
          <div class="panel-body">col-lg-4</div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="panel">
          <div class="panel-body">col-lg-4</div>
        </div>
      </div>
      </div>
    <div class="row">
      <div class="col-lg-3">
        <div class="panel">
          <div class="panel-body">col-lg-3</div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="panel">
          <div class="panel-body">col-lg-3</div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="panel">
          <div class="panel-body">col-lg-3</div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="panel">
          <div class="panel-body">col-lg-3</div>
        </div>
      </div>
      </div>
    <div class="row">
      <div class="col-lg-2">
        <div class="panel">
          <div class="panel-body">col-lg-2</div>
        </div>
      </div>
      <div class="col-lg-2">
        <div class="panel">
          <div class="panel-body">col-lg-2</div>
        </div>
      </div>
      <div class="col-lg-2">
        <div class="panel">
          <div class="panel-body">col-lg-2</div>
        </div>
      </div>
      <div class="col-lg-2">
        <div class="panel">
          <div class="panel-body">col-lg-2</div>
        </div>
      </div>
      <div class="col-lg-2">
        <div class="panel">
          <div class="panel-body">col-lg-2</div>
        </div>
      </div>
      <div class="col-lg-2">
        <div class="panel">
          <div class="panel-body">col-lg-2</div>
        </div>
      </div>
      </div>
    <div class="row">
      <div class="col-lg-12">
        <p class="hd-title">Mobile, tablet, and desktop</p>
      </div>
      </div>
    <div class="row">
      <div class="col-lg-4">
        <div class="row">
          <div class="col-xs-6">
            <div class="panel">
              <div class="panel-body">col-xs-6</div>
            </div>
          </div>
          <div class="col-xs-6">
            <div class="panel">
              <div class="panel-body">col-xs-6</div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-8">
        <div class="panel">
          <div class="panel-body">col-lg-8</div>
        </div>
      </div>
      </div>
    <div class="row">
      <div class="col-lg-6">
        <div class="row">
          <div class="col-sm-6">
            <div class="panel">
              <div class="panel-body">col-sm-6</div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="panel">
              <div class="panel-body">col-sm-6</div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="panel">
          <div class="panel-body">col-lg-6</div>
        </div>
      </div>
      </div>
    <div class="row">
      <div class="col-lg-8">
        <div class="row">
          <div class="col-md-6">
            <div class="panel">
              <div class="panel-body">col-md-6</div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="panel">
              <div class="panel-body">col-md-6</div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="panel">
          <div class="panel-body">col-lg-4</div>
        </div>
      </div>
      </div>
    <div class="row">
      <div class="col-sm-6">
        <div class="panel">
          <div class="panel-body">col-sm-6</div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="panel">
          <div class="panel-body">col-sm-6</div>
        </div>
      </div>
      </div>
        
        
  </div><!--/page-content end--> 
  @stop