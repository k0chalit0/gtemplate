@extends('master')
@section('content')

  <div class="page-content top-margin"> 
    
    <!--Colored Blocks start-->
    <div class="row">
      <div class="col-md-12 col-sm-12">
          <h2>Colored Blocks</h2>
      </div>
      <div class="col-sm-4 col-md-4">
        <div class="block-web">
          <div class="header">
            <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
            <h3>Default Block</h3>
          </div>
          <div class="porlets-content">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non lectus molestie, condimentum quam et, iaculis justo. Nunc vel ultricies nunc. Aliquam tempus sodales eros vel tincidunt. Proin ultricies bibendum urna et aliquam. Nunc quis nisl sit amet erat bibendum aliquet.</p>
          </div>
        </div>
        <div class="block-web dark-box">
          <div class="header">
            <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
            <h3>Dark Box</h3>
          </div>
          <div class="porlets-content">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non lectus molestie, condimentum quam et, iaculis justo. Nunc vel ultricies nunc. Aliquam tempus sodales eros vel tincidunt. Proin ultricies bibendum urna et aliquam. Nunc quis nisl sit amet erat bibendum aliquet.</p>
          </div>
        </div>
      </div>
      <div class="col-sm-4 col-md-4">
        <div class="block-web primary-box">
          <div class="header">
            <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
            <h3>Primary Box</h3>
          </div>
          <div class="porlets-content">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non lectus molestie, condimentum quam et, iaculis justo. Nunc vel ultricies nunc. Aliquam tempus sodales eros vel tincidunt. Proin ultricies bibendum urna et aliquam. Nunc quis nisl sit amet erat bibendum aliquet.</p>
          </div>
        </div>
        <div class="block-web success-box">
          <div class="header">
            <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
            <h3>Success Box</h3>
          </div>
          <div class="porlets-content">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non lectus molestie, condimentum quam et, iaculis justo. Nunc vel ultricies nunc. Aliquam tempus sodales eros vel tincidunt. Proin ultricies bibendum urna et aliquam. Nunc quis nisl sit amet erat bibendum aliquet.</p>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-sm-4">
        <div class="block-web info-box">
          <div class="header">
            <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
            <h3>Info Box</h3>
          </div>
          <div class="porlets-content">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non lectus molestie, condimentum quam et, iaculis justo. Nunc vel ultricies nunc. Aliquam tempus sodales eros vel tincidunt. Proin ultricies bibendum urna et aliquam. Nunc quis nisl sit amet erat bibendum aliquet.</p>
          </div>
        </div>
        <div class="block-web danger-box">
          <div class="header">
            <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
            <h3>Danger Box</h3>
          </div>
          <div class="porlets-content">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non lectus molestie, condimentum quam et, iaculis justo. Nunc vel ultricies nunc. Aliquam tempus sodales eros vel tincidunt. Proin ultricies bibendum urna et aliquam. Nunc quis nisl sit amet erat bibendum aliquet.</p>
          </div>
        </div>
      </div>
    </div>
    <!--/Colored Blocks end--> 
    <!--Colored Header Blocks start-->
    <div class="row">
      <div class="col-md-12 col-sm-12">
          <h2>Colored Header Blocks</h2>
      </div>
      <div class="col-md-4 col-sm-4">
        <div class="block block-color">
          <div class="header">
            <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
            <h3>Default Block</h3>
          </div>
          <div class="porlets-content">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non lectus molestie, condimentum quam et, iaculis justo. Nunc vel ultricies nunc. Aliquam tempus sodales eros vel tincidunt. Proin ultricies bibendum urna et aliquam. Nunc quis nisl sit amet erat bibendum aliquet.</p>
          </div>
        </div>
        <div class="block block-color primary">
          <div class="header">
            <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
            <h3>Primary Block</h3>
          </div>
          <div class="porlets-content">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non lectus molestie, condimentum quam et, iaculis justo. Nunc vel ultricies nunc. Aliquam tempus sodales eros vel tincidunt. Proin ultricies bibendum urna et aliquam. Nunc quis nisl sit amet erat bibendum aliquet.</p>
          </div>
        </div>
      </div>
      <div class=" col-md-4 col-sm-4">
        <div class="block block-color success">
          <div class="header">
            <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
            <h3>Success Block</h3>
          </div>
          <div class="porlets-content">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non lectus molestie, condimentum quam et, iaculis justo. Nunc vel ultricies nunc. Aliquam tempus sodales eros vel tincidunt. Proin ultricies bibendum urna et aliquam. Nunc quis nisl sit amet erat bibendum aliquet.</p>
          </div>
        </div>
        <div class="block block-color info">
          <div class="header">
            <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
            <h3>Info Block</h3>
          </div>
          <div class="porlets-content">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non lectus molestie, condimentum quam et, iaculis justo. Nunc vel ultricies nunc. Aliquam tempus sodales eros vel tincidunt. Proin ultricies bibendum urna et aliquam. Nunc quis nisl sit amet erat bibendum aliquet.</p>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-sm-4">
        <div class="block block-color warning">
          <div class="header">
            <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
            <h3>Warning Block</h3>
          </div>
          <div class="porlets-content">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non lectus molestie, condimentum quam et, iaculis justo. Nunc vel ultricies nunc. Aliquam tempus sodales eros vel tincidunt. Proin ultricies bibendum urna et aliquam. Nunc quis nisl sit amet erat bibendum aliquet.</p>
          </div>
        </div>
        <div class="block block-color danger">
          <div class="header">
            <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
            <h3>Danger Block</h3>
          </div>
          <div class="porlets-content">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non lectus molestie, condimentum quam et, iaculis justo. Nunc vel ultricies nunc. Aliquam tempus sodales eros vel tincidunt. Proin ultricies bibendum urna et aliquam. Nunc quis nisl sit amet erat bibendum aliquet.</p>
          </div>
        </div>
      </div>
    </div><!--/row end--> 
   </div>
@stop