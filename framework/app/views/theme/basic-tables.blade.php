@extends('master')
@section('content')
	<div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2><i class="fa fa-table fa-fw "></i> Basic Tables</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Normal Tables</h3>
            </div>
            <div class="porlets-content">
              <p>Adds borders to any table row within <code>&lt;table&gt;</code> by adding the <code>.table-bordered</code> with the base class</p>
              <div class="table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Column name</th>
                      <th>Column name</th>
                      <th>Column name</th>
                      <th>Column name</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Row 1</td>
                      <td>Row 2</td>
                      <td>Row 3</td>
                      <td>Row 4</td>
                    </tr>
                    <tr>
                      <td>Row 1</td>
                      <td>Row 2</td>
                      <td>Row 3</td>
                      <td>Row 4</td>
                    </tr>
                    <tr>
                      <td>Row 1</td>
                      <td>Row 2</td>
                      <td>Row 3</td>
                      <td>Row 4</td>
                    </tr>
                    <tr>
                      <td>Row 1</td>
                      <td>Row 2</td>
                      <td>Row 3</td>
                      <td>Row 4</td>
                    </tr>
                    <tr>
                      <td>Row 1</td>
                      <td>Row 2</td>
                      <td>Row 3</td>
                      <td>Row 4</td>
                    </tr>
                    <tr>
                      <td>Row 1</td>
                      <td>Row 2</td>
                      <td>Row 3</td>
                      <td>Row 4</td>
                    </tr>
                  </tbody>
                </table>
              </div><!--/table-responsive-->
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">No Padding</h3>
            </div>
            <div class="porlets-content">
              <div class="alert alert-info">
                <button data-dismiss="alert" class="close"> × </button>
                <i class="fa-fw fa fa-info"></i> Adds zebra-striping to table row within <code>&lt;table&gt;</code> by adding the <code>.table-striped</code> with the base class </div>
              <div class="table-responsive">
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Column name</th>
                      <th>Column name</th>
                      <th>Column name</th>
                      <th>Column name</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Row 1</td>
                      <td>Row 2</td>
                      <td>Row 3</td>
                      <td>Row 4</td>
                    </tr>
                    <tr>
                      <td>Row 1</td>
                      <td>Row 2</td>
                      <td>Row 3</td>
                      <td>Row 4</td>
                    </tr>
                    <tr>
                      <td>Row 1</td>
                      <td>Row 2</td>
                      <td>Row 3</td>
                      <td>Row 4</td>
                    </tr>
                    <tr>
                      <td>Row 1</td>
                      <td>Row 2</td>
                      <td>Row 3</td>
                      <td>Row 4</td>
                    </tr>
                    <tr>
                      <td>Row 1</td>
                      <td>Row 2</td>
                      <td>Row 3</td>
                      <td>Row 4</td>
                    </tr>
                    <tr>
                      <td>Row 1</td>
                      <td>Row 2</td>
                      <td>Row 3</td>
                      <td>Row 4</td>
                    </tr>
                  </tbody>
                </table>
              </div><!--/table-responsive-->
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Table hover States</h3>
            </div>
            <div class="porlets-content">
              <div class="alert alert-info">
                <button data-dismiss="alert" class="close"> × </button>
                <i class="fa-fw fa fa-info"></i> Enables hover effect <code>&lt;table&gt;</code> by adding the <code>.table-hover</code> with the base class </div>
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Username</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>Mark</td>
                      <td>Otto</td>
                      <td>@mdo</td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Jacob</td>
                      <td>Thornton</td>
                      <td>@fat</td>
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>Larry</td>
                      <td>the Bird</td>
                      <td>@twitter</td>
                    </tr>
                    <tr>
                      <td>4</td>
                      <td>Wise</td>
                      <td>Man</td>
                      <td>@myorange</td>
                    </tr>
                  </tbody>
                </table>
              </div><!--/table-responsive-->
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-6-->
        
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Table TR with Color</h3>
            </div>
            <div class="porlets-content">
              <div class="alert alert-info">
                <button data-dismiss="alert" class="close"> × </button>
                <i class="fa-fw fa fa-info"></i> Add custom colors to your TR and TD <code>&lt;tr&gt;</code> by adding <code>.success</code>, <code>.danger</code>, <code>.warning</code> and <code>.info</code> respectively </div>
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th> <i class="fa fa-building"></i> Product</th>
                      <th> <i class="fa fa-calendar"></i> Payment Taken</th>
                      <th> <i class="glyphicon glyphicon-send"></i> Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="success">
                      <td>1</td>
                      <td>TB - Monthly</td>
                      <td>01/03/2014</td>
                      <td>Approved</td>
                    </tr>
                    <tr class="danger">
                      <td>2</td>
                      <td>TB - Monthly</td>
                      <td>02/03/2014</td>
                      <td>Declined</td>
                    </tr>
                    <tr class="warning">
                      <td>3</td>
                      <td>TB - Monthly</td>
                      <td>03/03/2014</td>
                      <td>Pending</td>
                    </tr>
                    <tr class="info">
                      <td>4</td>
                      <td>TB - Monthly</td>
                      <td>04/03/2014</td>
                      <td>Call in to confirm</td>
                    </tr>
                  </tbody>
                </table>
              </div><!--/table-responsive-->
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-6--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Condenced table + combined prev. classes</h3>
            </div>
            <div class="porlets-content">
              <div class="alert alert-warning">
                <button data-dismiss="alert" class="close"> × </button>
                <i class="fa-fw fa fa-info"></i> A combined table effect with all classes mentioned above added to <code>&lt;table&gt;</code>. <code> .table-bordered .table-striped .table-condensed .table-hover .smart-form .has-tickbox </code> </div>
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-condensed table-hover">
                  <thead>
                    <tr>
                      <th>
                          <input type="checkbox" name="checkbox-inline">
                          <span class="custom-checkbox"></span>
                      </th>
                      <th>Column name <a class="btn btn-xs btn-default pull-right" href="javascript:void(0);"><i class="fa fa-filter"></i></a> </th>
                      <th>Column name <a class="btn btn-xs btn-default pull-right" href="javascript:void(0);"><i class="fa fa-filter"></i></a></th>
                      <th>Column name <a class="btn btn-xs btn-default pull-right" href="javascript:void(0);"><i class="fa fa-filter"></i></a></th>
                      <th>Column name <a class="btn btn-xs btn-default pull-right" href="javascript:void(0);"><i class="fa fa-filter"></i></a></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                          <input type="checkbox" name="checkbox-inline">
                          <span class="custom-checkbox"></span></td>
                      <td>Row 1</td>
                      <td>Row 2</td>
                      <td>Row 3</td>
                      <td>Row 4</td>
                    </tr>
                    <tr>
                      <td>
                          <input type="checkbox" name="checkbox-inline">
                          <span class="custom-checkbox"></span></td>
                      <td>Row 1</td>
                      <td>Row 2</td>
                      <td>Row 3</td>
                      <td>Row 4</td>
                    </tr>
                    <tr>
                      <td>
                          <input type="checkbox" name="checkbox-inline">
                          <span class="custom-checkbox"></span></td>
                      <td>Row 1</td>
                      <td>Row 2</td>
                      <td>Row 3</td>
                      <td>Row 4</td>
                    </tr>
                    <tr>
                      <td>
                          <input type="checkbox" name="checkbox-inline">
                          <span class="custom-checkbox"></span></td>
                      <td>Row 1</td>
                      <td>Row 2</td>
                      <td>Row 3</td>
                      <td>Row 4</td>
                    </tr>
                    <tr>
                      <td>
                          <input type="checkbox" name="checkbox-inline">
                          <span class="custom-checkbox"></span></td>
                      <td>Row 1</td>
                      <td>Row 2</td>
                      <td>Row 3</td>
                      <td>Row 4</td>
                    </tr>
                    <tr>
                      <td>
                          <input type="checkbox" name="checkbox-inline">
                          <span class="custom-checkbox"></span></td>
                      <td>Row 1</td>
                      <td>Row 2</td>
                      <td>Row 3</td>
                      <td>Row 4</td>
                    </tr>
                  </tbody>
                </table>
              </div><!--/table-responsive-->
            </div><!--/porlets-content--> 
          </div><!--/block-web--> 
        </div><!--/col-md-12--> 
      </div><!--/row--> 
      
    </div><!--/page-content end--> 
@stop