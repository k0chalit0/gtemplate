@extends('master')
@section('content')
    <div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>FullCalendar</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-lg-3">
          <div class="block-web">
            <div id='external-events'>
              <h4>Draggable Events</h4>
              <div class='external-event btn btn-primary'>My Event 1</div>
              <div class='external-event btn btn-success'>My Event 2</div>
              <div class='external-event btn btn-info'>My Event 3</div>
              <div class='external-event btn btn-warning'>My Event 4</div>
              <div class='external-event btn btn-danger'>My Event 5</div>
              <div class='external-event btn btn-info'>My Event 6</div>
              <div class='external-event btn btn-warning'>My Event 7</div>
              <div class='external-event btn btn-danger'>My Event 8</div>
              <p>
                <input type='checkbox' id='drop-remove' />
                <span class="custom-checkbox"></span>
                <label for='drop-remove'>remove after drop</label>
              </p>
            </div><!--/external-events--> 
          </div><!--/block-web--> 
        </div><!--/col-md-3--> 
        <div class="col-lg-9">
          <div class="block-web">
            <div id='calendar'></div>
          </div><!--/block-web--> 
        </div><!--/col-md-9--> 
      </div><!--/row--> 
      
    </div><!--/page-content end--> 
  </div><!--/main-content end--> 
</div><!--/page-container end--> 
@stop