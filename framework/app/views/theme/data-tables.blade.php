  @extends('master')
  @section('content')  
    <div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Data Table</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
           <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Data Table</h3>
            </div>
         <div class="porlets-content">
            <div class="table-responsive">
                <table  class="display table table-bordered table-striped" id="dynamic-table">
                  <thead>
                    <tr>
                      <th>Rendering engine</th>
                      <th>Browser</th>
                      <th>Platform(s)</th>
                      <th class="hidden-phone">Engine version</th>
                      <th class="hidden-phone">CSS grade</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="gradeX">
                      <td>Trident</td>
                      <td>Internet
                        Explorer 4.0</td>
                      <td>Win 95+</td>
                      <td class="center hidden-phone">4</td>
                      <td class="center hidden-phone">X</td>
                    </tr>
                    <tr class="gradeC">
                      <td>Trident</td>
                      <td>Internet
                        Explorer 5.0</td>
                      <td>Win 95+</td>
                      <td class="center hidden-phone">5</td>
                      <td class="center hidden-phone">C</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Trident</td>
                      <td>Internet
                        Explorer 5.5</td>
                      <td>Win 95+</td>
                      <td class="center hidden-phone">5.5</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Trident</td>
                      <td>Internet
                        Explorer 6</td>
                      <td>Win 98+</td>
                      <td class="center hidden-phone">6</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Trident</td>
                      <td>Internet Explorer 7</td>
                      <td>Win XP SP2+</td>
                      <td class="center hidden-phone">7</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Trident</td>
                      <td>AOL browser (AOL desktop)</td>
                      <td>Win XP</td>
                      <td class="center hidden-phone">6</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Firefox 1.0</td>
                      <td>Win 98+ / OSX.2+</td>
                      <td class="center hidden-phone">1.7</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Firefox 1.5</td>
                      <td>Win 98+ / OSX.2+</td>
                      <td class="center hidden-phone">1.8</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Firefox 2.0</td>
                      <td>Win 98+ / OSX.2+</td>
                      <td class="center hidden-phone">1.8</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Firefox 3.0</td>
                      <td>Win 2k+ / OSX.3+</td>
                      <td class="center hidden-phone">1.9</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Camino 1.0</td>
                      <td>OSX.2+</td>
                      <td class="center hidden-phone">1.8</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Camino 1.5</td>
                      <td>OSX.3+</td>
                      <td class="center hidden-phone">1.8</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Netscape 7.2</td>
                      <td>Win 95+ / Mac OS 8.6-9.2</td>
                      <td class="center hidden-phone">1.7</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Netscape Browser 8</td>
                      <td>Win 98SE+</td>
                      <td class="center hidden-phone">1.7</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Netscape Navigator 9</td>
                      <td>Win 98+ / OSX.2+</td>
                      <td class="center hidden-phone">1.8</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Mozilla 1.0</td>
                      <td>Win 95+ / OSX.1+</td>
                      <td class="center hidden-phone">1</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Mozilla 1.1</td>
                      <td>Win 95+ / OSX.1+</td>
                      <td class="center hidden-phone">1.1</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Mozilla 1.2</td>
                      <td>Win 95+ / OSX.1+</td>
                      <td class="center hidden-phone">1.2</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Mozilla 1.3</td>
                      <td>Win 95+ / OSX.1+</td>
                      <td class="center hidden-phone">1.3</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Mozilla 1.4</td>
                      <td>Win 95+ / OSX.1+</td>
                      <td class="center hidden-phone">1.4</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Mozilla 1.5</td>
                      <td>Win 95+ / OSX.1+</td>
                      <td class="center hidden-phone">1.5</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Mozilla 1.6</td>
                      <td>Win 95+ / OSX.1+</td>
                      <td class="center hidden-phone">1.6</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Mozilla 1.7</td>
                      <td>Win 98+ / OSX.1+</td>
                      <td class="center hidden-phone">1.7</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Mozilla 1.8</td>
                      <td>Win 98+ / OSX.1+</td>
                      <td class="center hidden-phone">1.8</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Seamonkey 1.1</td>
                      <td>Win 98+ / OSX.2+</td>
                      <td class="center hidden-phone">1.8</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Epiphany 2.20</td>
                      <td>Gnome</td>
                      <td class="center hidden-phone">1.8</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Webkit</td>
                      <td>Safari 1.2</td>
                      <td>OSX.3</td>
                      <td class="center hidden-phone">125.5</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Webkit</td>
                      <td>Safari 1.3</td>
                      <td>OSX.3</td>
                      <td class="center hidden-phone">312.8</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Webkit</td>
                      <td>Safari 2.0</td>
                      <td>OSX.4+</td>
                      <td class="center hidden-phone">419.3</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Webkit</td>
                      <td>Safari 3.0</td>
                      <td>OSX.4+</td>
                      <td class="center hidden-phone">522.1</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Webkit</td>
                      <td>OmniWeb 5.5</td>
                      <td>OSX.4+</td>
                      <td class="center hidden-phone">420</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Webkit</td>
                      <td>iPod Touch / iPhone</td>
                      <td>iPod</td>
                      <td class="center hidden-phone">420.1</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Webkit</td>
                      <td>S60</td>
                      <td>S60</td>
                      <td class="center hidden-phone">413</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Presto</td>
                      <td>Opera 7.0</td>
                      <td>Win 95+ / OSX.1+</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Presto</td>
                      <td>Opera 7.5</td>
                      <td>Win 95+ / OSX.2+</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Presto</td>
                      <td>Opera 8.0</td>
                      <td>Win 95+ / OSX.2+</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Presto</td>
                      <td>Opera 8.5</td>
                      <td>Win 95+ / OSX.2+</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Presto</td>
                      <td>Opera 9.0</td>
                      <td>Win 95+ / OSX.3+</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Presto</td>
                      <td>Opera 9.2</td>
                      <td>Win 88+ / OSX.3+</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Presto</td>
                      <td>Opera 9.5</td>
                      <td>Win 88+ / OSX.3+</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Presto</td>
                      <td>Opera for Wii</td>
                      <td>Wii</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Presto</td>
                      <td>Nokia N800</td>
                      <td>N800</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Presto</td>
                      <td>Nintendo DS browser</td>
                      <td>Nintendo DS</td>
                      <td class="center hidden-phone">8.5</td>
                      <td class="center hidden-phone">C/A<sup>1</sup></td>
                    </tr>
                    <tr class="gradeC">
                      <td>KHTML</td>
                      <td>Konqureror 3.1</td>
                      <td>KDE 3.1</td>
                      <td class="center hidden-phone">3.1</td>
                      <td class="center hidden-phone">C</td>
                    </tr>
                    <tr class="gradeA">
                      <td>KHTML</td>
                      <td>Konqureror 3.3</td>
                      <td>KDE 3.3</td>
                      <td class="center hidden-phone">3.3</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>KHTML</td>
                      <td>Konqureror 3.5</td>
                      <td>KDE 3.5</td>
                      <td class="center hidden-phone">3.5</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeX">
                      <td>Tasman</td>
                      <td>Internet Explorer 4.5</td>
                      <td>Mac OS 8-9</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">X</td>
                    </tr>
                    <tr class="gradeC">
                      <td>Tasman</td>
                      <td>Internet Explorer 5.1</td>
                      <td>Mac OS 7.6-9</td>
                      <td class="center hidden-phone">1</td>
                      <td class="center hidden-phone">C</td>
                    </tr>
                    <tr class="gradeC">
                      <td>Tasman</td>
                      <td>Internet Explorer 5.2</td>
                      <td>Mac OS 8-X</td>
                      <td class="center hidden-phone">1</td>
                      <td class="center hidden-phone">C</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Misc</td>
                      <td>NetFront 3.1</td>
                      <td>Embedded devices</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">C</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Misc</td>
                      <td>NetFront 3.4</td>
                      <td>Embedded devices</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeX">
                      <td>Misc</td>
                      <td>Dillo 0.8</td>
                      <td>Embedded devices</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">X</td>
                    </tr>
                    <tr class="gradeX">
                      <td>Misc</td>
                      <td>Links</td>
                      <td>Text only</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">X</td>
                    </tr>
                    <tr class="gradeX">
                      <td>Misc</td>
                      <td>Lynx</td>
                      <td>Text only</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">X</td>
                    </tr>
                    <tr class="gradeC">
                      <td>Misc</td>
                      <td>IE Mobile</td>
                      <td>Windows Mobile 6</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">C</td>
                    </tr>
                    <tr class="gradeC">
                      <td>Misc</td>
                      <td>PSP browser</td>
                      <td>PSP</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">C</td>
                    </tr>
                    <tr class="gradeU">
                      <td>Other browsers</td>
                      <td>All others</td>
                      <td>-</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">U</td>
                    </tr>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>Rendering engine</th>
                      <th>Browser</th>
                      <th>Platform(s)</th>
                      <th class="hidden-phone">Engine version</th>
                      <th class="hidden-phone">CSS grade</th>
                    </tr>
                  </tfoot>
                </table>
              </div><!--/table-responsive-->
            </div><!--/porlets-content-->
            
            
          </div><!--/block-web--> 
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
       <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Editable Table</h3>
            </div>
         <div class="porlets-content">
          <div class="adv-table editable-table ">
                          <div class="clearfix">
                              <div class="btn-group">
                                  <button id="editable-sample_new" class="btn btn-primary">
                                      Add New <i class="fa fa-plus"></i>
                                  </button>
                              </div>
                              <div class="btn-group pull-right">
                                  <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                                  </button>
                                  <ul class="dropdown-menu pull-right">
                                      <li><a href="#">Print</a></li>
                                      <li><a href="#">Save as PDF</a></li>
                                      <li><a href="#">Export to Excel</a></li>
                                  </ul>
                              </div>
                          </div>
                          <div class="margin-top-10"></div>
                          <table class="table table-striped table-hover table-bordered" id="editable-sample">
                              <thead>
                              <tr>
                                  <th>Username</th>
                                  <th>Full Name</th>
                                  <th>Points</th>
                                  <th>Notes</th>
                                  <th>Edit</th>
                                  <th>Delete</th>
                              </tr>
                              </thead>
                              <tbody>
                              <tr class="">
                                  <td>John Doe</td>
                                  <td>Stephan Myburgh</td>
                                  <td>12345</td>
                                  <td class="center">super user</td>
                                  <td><a class="edit" href="javascript:;">Edit</a></td>
                                  <td><a class="delete" href="javascript:;">Delete</a></td>
                              </tr>
                              <tr class="">
                                  <td>Admin</td>
                                  <td>Tom Cooper</td>
                                  <td>216</td>
                                  <td class="center">new user</td>
                                  <td><a class="edit" href="javascript:;">Edit</a></td>
                                  <td><a class="delete" href="javascript:;">Delete</a></td>
                              </tr>
                              <tr class="">
                                  <td>Admin</td>
                                  <td> Shakib Al Hasan</td>
                                  <td>432</td>
                                  <td class="center">super user</td>
                                  <td><a class="edit" href="javascript:;">Edit</a></td>
                                  <td><a class="delete" href="javascript:;">Delete</a></td>
                              </tr>
                              <tr class="">
                                  <td>Admin</td>
                                  <td>WebPro</td>
                                  <td>856</td>
                                  <td class="center">elite user</td>
                                  <td><a class="edit" href="javascript:;">Edit</a></td>
                                  <td><a class="delete" href="javascript:;">Delete</a></td>
                              </tr>
                              <tr class="">
                                  <td>Admin</td>
                                  <td> WebPro</td>
                                  <td>675</td>
                                  <td class="center">new user</td>
                                  <td><a class="edit" href="javascript:;">Edit</a></td>
                                  <td><a class="delete" href="javascript:;">Delete</a></td>
                              </tr>
                              <tr class="">
                                  <td>Admin</td>
                                  <td>Alex Hales</td>
                                  <td>423</td>
                                  <td class="center">new user</td>
                                  <td><a class="edit" href="javascript:;">Edit</a></td>
                                  <td><a class="delete" href="javascript:;">Delete</a></td>
                              </tr>
                              <tr class="">
                                  <td>John Doe</td>
                                  <td>John Doe </td>
                                  <td>1234</td>
                                  <td class="center">super user</td>
                                  <td><a class="edit" href="javascript:;">Edit</a></td>
                                  <td><a class="delete" href="javascript:;">Delete</a></td>
                              </tr>
                              <tr class="">
                                  <td>Admin</td>
                                  <td>Alex Hales</td>
                                  <td>642</td>
                                  <td class="center">new user</td>
                                  <td><a class="edit" href="javascript:;">Edit</a></td>
                                  <td><a class="delete" href="javascript:;">Delete</a></td>
                              </tr>
                              <tr class="">
                                  <td>Admin</td>
                                  <td> Aaron Finch</td>
                                  <td>157</td>
                                  <td class="center">super user</td>
                                  <td><a class="edit" href="javascript:;">Edit</a></td>
                                  <td><a class="delete" href="javascript:;">Delete</a></td>
                              </tr>
                              <tr class="">
                                  <td>Admin</td>
                                  <td>Virat Kohli</td>
                                  <td>468</td>
                                  <td class="center">elite user</td>
                                  <td><a class="edit" href="javascript:;">Edit</a></td>
                                  <td><a class="delete" href="javascript:;">Delete</a></td>
                              </tr>
                              <tr class="">
                                  <td>Admin</td>
                                  <td> WebPro</td>
                                  <td>953</td>
                                  <td class="center">new user</td>
                                  <td><a class="edit" href="javascript:;">Edit</a></td>
                                  <td><a class="delete" href="javascript:;">Delete</a></td>
                              </tr>
                              <tr class="">
                                  <td>Admin</td>
                                  <td>Glenn Maxwell</td>
                                  <td>546</td>
                                  <td class="center">new user</td>
                                  <td><a class="edit" href="javascript:;">Edit</a></td>
                                  <td><a class="delete" href="javascript:;">Delete</a></td>
                              </tr>
                              </tbody>
                          </table>
                      </div>
 
            </div><!--/porlets-content-->  
          </div><!--/block-web--> 
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      
       <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Responsive Table With Expandable details</h3>
            </div>
         <div class="porlets-content">
         
         <div class="table-responsive">
                <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="hidden-table-info">
                  <thead>
                    <tr>
                      <th>Rendering engine</th>
                      <th>Browser</th>
                      <th class="hidden-phone">Platform(s)</th>
                      <th class="hidden-phone">Engine version</th>
                      <th class="hidden-phone">CSS grade</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="gradeX">
                      <td>Trident</td>
                      <td>Internet
                        Explorer 4.0</td>
                      <td class="hidden-phone">Win 95+</td>
                      <td class="center hidden-phone">4</td>
                      <td class="center hidden-phone">X</td>
                    </tr>
                    <tr class="gradeC">
                      <td>Trident</td>
                      <td>Internet
                        Explorer 5.0</td>
                      <td class="hidden-phone">Win 95+</td>
                      <td class="center hidden-phone">5</td>
                      <td class="center hidden-phone">C</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Trident</td>
                      <td>Internet
                        Explorer 5.5</td>
                      <td class="hidden-phone">Win 95+</td>
                      <td class="center hidden-phone">5.5</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Trident</td>
                      <td>Internet
                        Explorer 6</td>
                      <td class="hidden-phone">Win 98+</td>
                      <td class="center hidden-phone">6</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Trident</td>
                      <td>Internet Explorer 7</td>
                      <td class="hidden-phone">Win XP SP2+</td>
                      <td class="center hidden-phone">7</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Trident</td>
                      <td>AOL browser (AOL desktop)</td>
                      <td class="hidden-phone">Win XP</td>
                      <td class="center hidden-phone">6</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Firefox 1.0</td>
                      <td class="hidden-phone">Win 98+ / OSX.2+</td>
                      <td class="center hidden-phone">1.7</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Firefox 1.5</td>
                      <td class="hidden-phone">Win 98+ / OSX.2+</td>
                      <td class="center hidden-phone">1.8</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Firefox 2.0</td>
                      <td class="hidden-phone">Win 98+ / OSX.2+</td>
                      <td class="center hidden-phone">1.8</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Firefox 3.0</td>
                      <td class="hidden-phone">Win 2k+ / OSX.3+</td>
                      <td class="center hidden-phone">1.9</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Camino 1.0</td>
                      <td class="hidden-phone">OSX.2+</td>
                      <td class="center hidden-phone">1.8</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Camino 1.5</td>
                      <td class="hidden-phone">OSX.3+</td>
                      <td class="center hidden-phone">1.8</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Netscape 7.2</td>
                      <td class="hidden-phone">Win 95+ / Mac OS 8.6-9.2</td>
                      <td class="center hidden-phone">1.7</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Netscape Browser 8</td>
                      <td class="hidden-phone">Win 98SE+</td>
                      <td class="center hidden-phone">1.7</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Netscape Navigator 9</td>
                      <td class="hidden-phone">Win 98+ / OSX.2+</td>
                      <td class="center hidden-phone">1.8</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Mozilla 1.0</td>
                      <td class="hidden-phone">Win 95+ / OSX.1+</td>
                      <td class="center hidden-phone">1</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Mozilla 1.1</td>
                      <td class="hidden-phone">Win 95+ / OSX.1+</td>
                      <td class="center hidden-phone">1.1</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Mozilla 1.2</td>
                      <td class="hidden-phone">Win 95+ / OSX.1+</td>
                      <td class="center hidden-phone">1.2</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Mozilla 1.3</td>
                      <td class="hidden-phone">Win 95+ / OSX.1+</td>
                      <td class="center hidden-phone">1.3</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Mozilla 1.4</td>
                      <td class="hidden-phone">Win 95+ / OSX.1+</td>
                      <td class="center hidden-phone">1.4</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Mozilla 1.5</td>
                      <td class="hidden-phone">Win 95+ / OSX.1+</td>
                      <td class="center hidden-phone">1.5</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Mozilla 1.6</td>
                      <td class="hidden-phone">Win 95+ / OSX.1+</td>
                      <td class="center hidden-phone">1.6</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Mozilla 1.7</td>
                      <td class="hidden-phone">Win 98+ / OSX.1+</td>
                      <td class="center hidden-phone">1.7</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Mozilla 1.8</td>
                      <td class="hidden-phone">Win 98+ / OSX.1+</td>
                      <td class="center hidden-phone">1.8</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Seamonkey 1.1</td>
                      <td class="hidden-phone">Win 98+ / OSX.2+</td>
                      <td class="center hidden-phone">1.8</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Gecko</td>
                      <td>Epiphany 2.20</td>
                      <td class="hidden-phone">Gnome</td>
                      <td class="center hidden-phone">1.8</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Webkit</td>
                      <td>Safari 1.2</td>
                      <td class="hidden-phone">OSX.3</td>
                      <td class="center hidden-phone">125.5</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Webkit</td>
                      <td>Safari 1.3</td>
                      <td class="hidden-phone">OSX.3</td>
                      <td class="center hidden-phone">312.8</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Webkit</td>
                      <td>Safari 2.0</td>
                      <td class="hidden-phone">OSX.4+</td>
                      <td class="center hidden-phone">419.3</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Webkit</td>
                      <td>Safari 3.0</td>
                      <td class="hidden-phone">OSX.4+</td>
                      <td class="center hidden-phone">522.1</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Webkit</td>
                      <td>OmniWeb 5.5</td>
                      <td class="hidden-phone">OSX.4+</td>
                      <td class="center hidden-phone">420</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Webkit</td>
                      <td>iPod Touch / iPhone</td>
                      <td class="hidden-phone">iPod</td>
                      <td class="center hidden-phone">420.1</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Webkit</td>
                      <td>S60</td>
                      <td class="hidden-phone">S60</td>
                      <td class="center hidden-phone">413</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Presto</td>
                      <td>Opera 7.0</td>
                      <td class="hidden-phone">Win 95+ / OSX.1+</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Presto</td>
                      <td>Opera 7.5</td>
                      <td class="hidden-phone">Win 95+ / OSX.2+</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Presto</td>
                      <td>Opera 8.0</td>
                      <td class="hidden-phone">Win 95+ / OSX.2+</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Presto</td>
                      <td>Opera 8.5</td>
                      <td class="hidden-phone">Win 95+ / OSX.2+</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Presto</td>
                      <td>Opera 9.0</td>
                      <td class="hidden-phone">Win 95+ / OSX.3+</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Presto</td>
                      <td>Opera 9.2</td>
                      <td class="hidden-phone">Win 88+ / OSX.3+</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Presto</td>
                      <td>Opera 9.5</td>
                      <td class="hidden-phone">Win 88+ / OSX.3+</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Presto</td>
                      <td>Opera for Wii</td>
                      <td class="hidden-phone">Wii</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Presto</td>
                      <td>Nokia N800</td>
                      <td class="hidden-phone">N800</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Presto</td>
                      <td>Nintendo DS browser</td>
                      <td class="hidden-phone">Nintendo DS</td>
                      <td class="center hidden-phone">8.5</td>
                      <td class="center hidden-phone">C/A<sup>1</sup></td>
                    </tr>
                    <tr class="gradeC">
                      <td>KHTML</td>
                      <td>Konqureror 3.1</td>
                      <td class="hidden-phone">KDE 3.1</td>
                      <td class="center hidden-phone">3.1</td>
                      <td class="center hidden-phone">C</td>
                    </tr>
                    <tr class="gradeA">
                      <td>KHTML</td>
                      <td>Konqureror 3.3</td>
                      <td class="hidden-phone">KDE 3.3</td>
                      <td class="center hidden-phone">3.3</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeA">
                      <td>KHTML</td>
                      <td>Konqureror 3.5</td>
                      <td class="hidden-phone">KDE 3.5</td>
                      <td class="center hidden-phone">3.5</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeX">
                      <td>Tasman</td>
                      <td>Internet Explorer 4.5</td>
                      <td class="hidden-phone">Mac OS 8-9</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">X</td>
                    </tr>
                    <tr class="gradeC">
                      <td>Tasman</td>
                      <td>Internet Explorer 5.1</td>
                      <td class="hidden-phone">Mac OS 7.6-9</td>
                      <td class="center hidden-phone">1</td>
                      <td class="center hidden-phone">C</td>
                    </tr>
                    <tr class="gradeC">
                      <td>Tasman</td>
                      <td>Internet Explorer 5.2</td>
                      <td class="hidden-phone">Mac OS 8-X</td>
                      <td class="center hidden-phone">1</td>
                      <td class="center hidden-phone">C</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Misc</td>
                      <td>NetFront 3.1</td>
                      <td>Embedded devices</td>
                      <td class="center">-</td>
                      <td class="center">C</td>
                    </tr>
                    <tr class="gradeA">
                      <td>Misc</td>
                      <td>NetFront 3.4</td>
                      <td class="hidden-phone">Embedded devices</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">A</td>
                    </tr>
                    <tr class="gradeX">
                      <td>Misc</td>
                      <td>Dillo 0.8</td>
                      <td class="hidden-phone">Embedded devices</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">X</td>
                    </tr>
                    <tr class="gradeX">
                      <td>Misc</td>
                      <td>Links</td>
                      <td class="hidden-phone">Text only</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">X</td>
                    </tr>
                    <tr class="gradeX">
                      <td>Misc</td>
                      <td>Lynx</td>
                      <td class="hidden-phone">Text only</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">X</td>
                    </tr>
                    <tr class="gradeC">
                      <td>Misc</td>
                      <td>IE Mobile</td>
                      <td class="hidden-phone">Windows Mobile 6</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">C</td>
                    </tr>
                    <tr class="gradeC">
                      <td>Misc</td>
                      <td>PSP browser</td>
                      <td class="hidden-phone">PSP</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">C</td>
                    </tr>
                    <tr class="gradeU">
                      <td>Other browsers</td>
                      <td>All others</td>
                      <td class="hidden-phone">-</td>
                      <td class="center hidden-phone">-</td>
                      <td class="center hidden-phone">U</td>
                    </tr>
                  </tbody>
                </table>
              </div><!--/table-responsive-->
         
           </div><!--/porlets-content-->  
          </div><!--/block-web--> 
        </div><!--/col-md-12--> 
      </div><!--/row-->

 
    </div><!--/page-content end--> 
    @stop