@extends('master')
@section('content')
  @if(Auth::check())
    <div class="page-content">
      <h2 class="page_title"> <i class="page_icon icon-file-add"></i>Crear Página</h2>
      <p class="page_paragraph">Seleccione un template:</p>
      <div class="selection">
        <ul class="selection">
          @foreach($templates as $key => $tem)
            <li class="selection_item">
              <a href="usertemplates/creartemplate/{{$tem->id}}" class="selection_link">
                <div class="selection_overlay"><i class="icon-check selection_icon-overlay"></i></div>
                <img src="../../media/templates/{{ $tem->image }}" alt="" class="selection_img">
              </a>
            </li> 
          @endforeach
        </ul>
      </div>
    </div>
  @else
    <div class="page-content">
      <center><div class="jumbotron">
        <h1>Bienvenido a GenTemplate</h1>
        <a href="{{ route('sign_in') }}">
          <button type="button" class="btn btn-primary">Ingresar</button>
        </a>
        <a href="{{ route('sign_up') }}">
          <button type="button" class="btn btn-success">Registrar</button>
        </a>
      </div></center>
    </div>
  @endif
@stop