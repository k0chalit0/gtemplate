@extends('master')
@section('content')
	<div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2><i class="fa fa-table fa-fw "></i> Optimization Results</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Conversiones</h3>
            </div>
            <div class="porlets-content">
              <div class="table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Variaciones</th>
                      <th>Visitantes</th>
                      <th>Conversiones</th>
                      <th>Porcentaje Conversiones</th>
                      <th>Mejoras</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>

                    @foreach($tablas['tablas'] as $datos)
                      <tr>
                        <td>{{$datos['slug']}}</td>
                        <td>{{$datos['visits']}}</td>
                        <td>{{$datos['conversions']}}</td>
                        <td>{{$datos['conversions_rate'].' %'}}</td>
                        <td>-</td>
                        <td>Mejoras</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div><!--/table-responsive-->
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
    </div><!--/page-content end--> 
@stop