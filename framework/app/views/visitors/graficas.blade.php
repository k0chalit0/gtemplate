@extends('master')
@section('content')

  <div class="page-content">
    <div class="row"> 
      <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Donut Chart</h3>
            </div>
            <div class="porlets-content">
              <div id="donut-chart" class="graph"></div>
            </div>
          </div><!--/block-web--> 
        </div><!--/col-md-6--> 
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Jaguar 'E' Type vehicles in the UK</h3>
            </div>
            <div class="porlets-content">
              <div id="area-chart" class="graph"></div>
            </div>
          </div><!--/block-web--> 
        </div><!--/col-md-6-->
      </div><!--/row--> 
      
      <div class="row">
        <div class="col-md-6">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a> <a href="#" class="refresh"><i class="fa fa-repeat"></i></a> <a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Contador de visitas</h3>
            </div>
            <div class="porlets-content">
              <div id="bar-chart" class="graph"></div>
            </div>
          </div><!--/block-web--> 
        </div><!--/col-md-6--> 
      </div><!--/row-->

       <div class="row">
        <div class="col-md-12">
          <div class="block-web">
          <div id="graph"></div>
          </div><!--/block-web--> 
        </div><!--/col-md-12--> 
      </div><!--/row-->
    </div><!--/page-content end-->     
@stop