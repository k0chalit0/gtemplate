<!-- app/views/templates/create.blade.php -->

@extends('master')

@section('content')
    <div class="page-content">
        <div class="container">
            <nav class="navbar navbar-inverse">
                <ul class="nav navbar-nav">
                    <li><a href="{{ URL::to('templates') }}">Ver todos los Templates</a></li>
                    <li><a href="{{ URL::to('templates/create') }}">Crear un Template</a></li>
                </ul>
            </nav>
        	<h1>Viendo Template: "{{ $template->name }}"</h1>

            <div class="jumbotron text-center">
                <h2>{{ $template->name }}</h2>
                <p>
                    <strong>Tipo:</strong> {{ $template->type }}<br>
                    <div class="thumbnail">
        		      <img src="../../media/templates/{{ $template->image }}" alt="...">
        		    </div>
        		    <strong>Default HTML:</strong>
                    <a href="../../plantillas/generics/{{$template->defaulthtml}}" class="btn btn-info">Abrir template</a>
                </p>
            </div>
        </div>
    </div>
@endsection
