<!-- app/views/templates/create.blade.php -->

@extends('master')

@section('content')
    <div class="page-content">

	    <div class="container">

	    <nav class="navbar navbar-inverse">
	        <ul class="nav navbar-nav">
	            <li><a href="{{ URL::to('templates') }}">Ver Todos</a></li>
	            <li><a href="{{ URL::to('templates/create') }}">Crear un Template</a>
	        </ul>
	    </nav>
		<h1>Crear un Template</h1>
		<!-- if there are creation errors, they will show here -->
		{{ HTML::ul($errors->all()) }}

		{{ Form::open(array('url' => 'templates', 'enctype'=>'multipart/form-data')) }}

			{{ Field::text('name') }}
			{{ Field::text('type') }}
			<p>{{ Form::label('templates', 'Template')}}</p>
			{{ Form::file('defaulthtml') }}
			<h3>Imagen del Template</h3>
			<div id="preview" class="thumbnail">
		    <a href="#" id="file-select" class="btn btn-default">Elegir archivo</a>
		    <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNzEiIGhlaWdodD0iMTgwIj48cmVjdCB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgZmlsbD0iI2VlZSI+PC9yZWN0Pjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9Ijg1LjUiIHk9IjkwIiBzdHlsZT0iZmlsbDojYWFhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjEycHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+MTcxeDE4MDwvdGV4dD48L3N2Zz4="/>

			</div>
			{{ Form::file('image', array('id'=>'image', 'style'=>'display:none')) }}
			<p>
		    {{ Form::submit('Crear un Template', array('class' => 'btn btn-btn-success')) }}
		    </p>

		{{ Form::close() }}
	    
	    </div>
	</div>
@endsection