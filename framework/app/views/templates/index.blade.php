<!-- app/views/templates/index.blade.php -->

@extends('master')

@section('content')
    <div class="page-content">
        <div class="container">

        <nav class="navbar navbar-inverse">
            <ul class="nav navbar-nav">
                <li><a href="{{ URL::to('templates') }}">Ver todos los Templates</a></li>
                <li><a href="{{ URL::to('templates/create') }}">Crear un Template</a>
            </ul>
        </nav>

        <h1>Lista de Templates</h1>

        <!-- will be used to show any messages -->
        @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif

        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <td>ID</td>
                    <td>Nombre Template</td>
                    <td>Tipo</td>
                    <td>Default Html</td>
                    <td>Acciones</td>
                </tr>
            </thead>
            <tbody>
            @foreach($templates as $key => $value)
                <tr>
                    <td>{{ $value->id }}</td>
                    <td>{{ $value->name }}</td>
                    <td>{{ $value->type }}</td>
                    <td>{{ $value->defaulthtml }}</td>

                    <!-- we will also add show, edit, and delete buttons -->
                    <td>

                        <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                        <!-- we will add this later since its a little more complicated than the other two buttons -->

                        <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                        <a class="btn btn-small btn-success" href="{{ URL::to('templates/' . $value->id) }}">Ver</a>

                        <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                        <a class="btn btn-small btn-info" href="{{ URL::to('templates/' . $value->id . '/edit') }}">Editar</a>
                        {{ Form::open(array('method'=> 'DELETE', 'route' => array('templates.destroy', $value->id))) }}        
                        {{ Form::submit('Eliminar', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </div>
    </div>
@endsection