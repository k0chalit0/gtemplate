<!DOCTYPE html>
<html lang="en">
<head>
	@include('includes.head')
</head>
<body class="dark-theme">
@if (Auth::check())
	@include('includes.header')

	@include('includes.info-right')
	<div class="page-container">

	@include('includes.sidebar-left')
	<div id="main-content"> 
@endif


	@yield('content')
</div><!--/main-content end-->
</div><!--/page-container end-->
	@include('includes.footer')
	@yield('footer_scripts')
</body>
</html>