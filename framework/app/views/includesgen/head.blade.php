	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
	<title>SUMECLIENTES</title>

	<!-- Bootstrap -->
	<link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('bootstrap/bower_components/bootstrapcolorpicker/dist/css/bootstrap-colorpicker.min.css') }}">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://fontastic.s3.amazonaws.com/mBcpKha37mryLvEf949bD9/icons.css">
	<link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/style.css') }}" rel="stylesheet">
	<link href="{{ asset('css/style-responsive.css') }}" rel="stylesheet">
	<link href="{{ asset('plugins/kalendar/kalendar.css') }}" rel="stylesheet">
	<link href="{{ asset('css/jquerysctipttop.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('gentemplate/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('gentemplate/css/edit.css') }}" rel="stylesheet">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	      <script src="{{ asset('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') }}"></script>
	      <script src="{{ asset('https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js') }}"></script>
	    <![endif]-->