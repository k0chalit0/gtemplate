<script type="text/template" id="tmp-edit-tool"> 
  <div class="edit-tool">
    <div class="edit-tool_box <%= defaults.video? 'edit-tool_box-video' :'' %>"><% if (defaults.move) { %>
      <div class="edit-tool_item"><span title="Mover" class="edit-tool_btn btn-move handle"><i class="btn-icon icon-cursor-move-two"></i></span></div><% } %><% if (defaults.background.backgroundColor) { %>
      <div class="edit-tool_item">
        <div class="edit-tool_colorpicker edit-tool_bgcolor"><span title="Color de fondo" class="edit-tool_btn edit-tool_btn-bgc"></span>
          <input type="text" class="edit-tool_input"/>
        </div>
      </div><% } %><% if (defaults.background.backgroundImage) { %>
      <div class="edit-tool_item"><span title="Imagen de fondo" class="edit-tool_btn edit-tool_btn-bgi"><i class="btn-icon icon-image-add"></i></span>
        <input type="file" class="edit-tool_file edit-tool_file-bg hide"/>
      </div><% } %><% if (defaults.background.backgroundImage) { %>
      <div class="edit-tool_item"><span title="Imagen de fondo" class="edit-tool_btn edit-tool_btn-bgirmv"><i class="btn-icon icon-image-cancel"></i></span></div><% } %><% if (defaults.img) { %>
      <div class="edit-tool_item"><span title="Imagen" class="edit-tool_btn edit-tool_btn-img"><i class="btn-icon icon-img"></i></span>
        <input type="file" class="edit-tool_file edit-tool_file-img hide"/>
      </div><% } %><% if (defaults.fontFamily) { %>
      <div class="edit-tool_item"><span title="Fuente" class="edit-tool_btn edit-tool_btn-ff"><i class="btn-icon icon-font"></i></span>
        <div class="edit-tool_sub edit-tool_sub-ff">
          <div class="edit-tool_select">
            <select id="fontFamily" name="fontfamily"></select>
          </div>
        </div>
      </div><% } %><% if (defaults.color) { %>
      <div class="edit-tool_item">
        <div class="edit-tool_colorpicker edit-tool_color"><span title="Color" class="edit-tool_btn edit-tool_btn-color"></span>
          <input type="text" class="edit-tool_input"/>
        </div>
      </div><% } %><% if (defaults.fontSize) { %>
      <div class="edit-tool_item">
        <input type="number" min="8" max="70" placeholder="18" class="edit-tool_input edit-tool_input-fz"/>
      </div><% } %><% if (defaults.fontStyle) { %>
      <div class="edit-tool_item">
        <div title="Estilo de Texto" class="edit-tool_btn"> <i class="btn-icon icon-bold"></i></div>
        <div class="edit-tool_sub"><a href="#" title="Negrita" class="edit-tool_btn btn-bold"> <i class="btn-icon icon-bold"></i></a><a href="#" title="Cursiva" class="edit-tool_btn btn-italic"><i class="btn-icon icon-italic"></i></a><a href="#" title="Subrayado" class="edit-tool_btn btn-underline"><i class="btn-icon icon-underline"></i></a><a href="#" title="Tachado" class="edit-tool_btn btn-strikethrough"><i class="btn-icon icon-strikethrough"></i></a>
        </div>
      </div><% } %><% if (defaults.textAlign) { %>
      <div class="edit-tool_item"><span title="Alinear Texto" class="edit-tool_btn"><i class="btn-icon icon-align-left"></i></span>
        <div class="edit-tool_sub"><a href="#" title="Alinear Texto a la Izquierda" class="edit-tool_btn btn-align-left"><i class="btn-icon icon-align-left"></i></a><a href="#" title="Centrar Texto" class="edit-tool_btn btn-align-center"><i class="btn-icon icon-align-center"></i></a><a href="#" title="Alinear Texto a la Derecha" class="edit-tool_btn btn-align-right"><i class="btn-icon icon-align-right"></i></a><a href="#" title="Justificar Texto" class="edit-tool_btn btn-align-justify"><i class="btn-icon icon-align-justify"></i></a>
        </div>
      </div><% } %><% if (defaults.link) { %>
      <div class="edit-tool_item"><span title="Añadir Enlace" class="edit-tool_btn btn-link"><i class="btn-icon icon-link"></i></span>
      </div><% } %><% if (defaults.popup) { %>
      <div class="edit-tool_item"><span title="Popup" type="button" data-toggle="modal" data-target="#myModal" class="edit-tool_btn btn-popup"><i class="btn-icon icon-popup"></i></span></div><% } %><% if (defaults.countdown) { %>
      <div class="edit-tool_item"><span title="Contdown" class="edit-tool_btn btn-countdown"><i class="btn-icon icon-sort-numeric"></i></span>
        <div class="edit-tool_sub edit-tool_sub-countdown hide">
          <input type="date" class="edit-tool_input edit-tool_input"/><span class="edit-tool_btn edit-tool_btn-ok">Ok</span>
        </div>
      </div><% } %><% if (defaults.video) { %>
      <div class="edit-tool_item">
        <input type="text" placeholder="URL de video" class="edit-tool_input"/>
      </div>
      <div class="edit-tool_item">
        <label for="vautoplay" class="edit-tool_label">
          <input type="checkbox" id="vautoplay" class="edit-tool_checkbox"/>Autoplay
        </label>
      </div>
      <div class="edit-tool_item">
        <label for="vcontrols" class="edit-tool_label">
          <input type="checkbox" id="vcontrols" class="edit-tool_checkbox"/>Progress Bar
        </label>
      </div>
      <div class="edit-tool_item"><span type="button" class="edit-tool_btn btn-apply"><i class="btn-icon icon-check"></i></span></div><% } %><% if (defaults.remove && !defaults.isSection) { %>
      <div class="edit-tool_item"><span title="Eliminar" class="edit-tool_btn btn-remove"><i class="btn-icon icon-trash"></i></span></div><% } %><% if (defaults.close) { %>
      <div class="edit-tool_item"><span title="Cerrar" class="edit-tool_btn btn-close"><i class="btn-icon icon-close"></i></span></div><% } %>
    </div>
  </div><% if (defaults.isSection) { %>
  <div class="edit-tool right">
    <div class="edit-tool_box">
      <div class="edit-tool_item"><span title="Eliminar" class="edit-tool_btn btn-remove"><i class="btn-icon icon-trash"></i></span></div>
    </div>
  </div><% } %>
</script>
<script type="text/template" id="tmp-edit-move"> 
  <div class="edit-tool">
    <div class="edit-tool_box"><span title="Mover" class="edit-tool_btn btn-move handle"><i class="btn-icon icon-cursor-move-two"></i></span></div>
  </div>
</script>