<!DOCTYPE html>
<!--if lt IE 7html.no-js.lt-ie9.lt-ie8.lt-ie7
-->
<!--if IE 7html.no-js.lt-ie9.lt-ie8
-->
<!--if IE 8html.no-js.lt-ie9
-->
<!--[if gt IE 8]><!-->
<html class="no-js">
  <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta name="language" content="es">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title>GenTemplate</title>
    <link rel="stylesheet" href="https://fontastic.s3.amazonaws.com/mBcpKha37mryLvEf949bD9/icons.css">
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('bootstrap/bower_components/bootstrapcolorpicker/dist/css/bootstrap-colorpicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bootstrap/css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('bootstrap/css/edit.css') }}">
  </head>
  <body>
    <header class="header clearfix">
      <div class="box-left">
        <ul class="header_nav">
          <li>
            <div class="toggle-menu toggle-menu-left">
              <label for="check-nav"></label><a href="#" class="toggle-menu_btn"><span></span><span></span><span></span></a>
            </div>
          </li>
          <li>
            <h1 class="logo"><a href="/">GenTemplate</a></h1>
          </li>
          <li><a href="#" class="header_link"><i class="icon-arrow-double-l"></i>Ir a mi sitio web</a></li>
        </ul>
      </div>
      <div class="box-right">
        <ul class="header_nav">
          <li><a href="{{ route('logout') }}" class="header_link">
              Cerrar sesión<i class="icon-arrow-double-r"></i></a></li>
          <li>
            <div class="toggle-menu toggle-menu-left">
              <label for="check-nav-2"></label><a href="#" class="toggle-menu_btn"><span></span><span></span><span></span></a>
            </div>
          </li>
        </ul>
      </div>
    </header>
    <main class="main">
      <input type="checkbox" checked id="check-nav" class="check-nav">
      <nav id="nav-main" class="navigation">
        <ul class="menu-user">
            <li class="menu-user_item"><a class="menu-user_link"><i class="menu-user_icon icon-user"></i><span class="menu-user_text">
              {{Auth::user()->first_name." ".Auth::user()->last_name}}
            </span></a></li>
            <li class="menu-user_item"><a class="menu-user_link" href="{{ route('mistemplates') }}"><i class="menu-user_icon icon-copy"></i><span class="menu-user_text">Mis páginas</span></a>
        </ul>
        <ul class="menu">
          <li class="menu_item"><a href="/" class="menu_link"><i class="menu_icon icon-file-add"></i><span class="menu_text">Crear página</span></a></li>
          <li class="menu-user_item"><a href="{{ route('logout') }}" class="menu-user_link"><i class="menu-user_icon icon-log-out"></i><span class="menu-user_text">Salir</span></a></li>
          <li>
            {{ Form::open(['route'=> 'templatepreview', 'onsubmit' => 'return generateData();', 'method'=>'POST', 'role'=>'form']) }}
            <input type="hidden" id="id" name="id" value="{{$usertemplate->id}}">
            <input type="hidden" id="print_data" name="print_data">
            <input type="submit" class="btn btn-success" value="Vista Previa">
          </form>
            {{ Form::close() }}
          </li>
        </ul>
      </nav>
      <section id="main-content" class="l-main-content">
        <div class="main-content">
        @yield('content')
        </div>
      </section>
    </main>
<script type="text/template" id="tmp-edit-image"> 
        <div class="edit-tool">
          <div class="edit-tool_box"><span title="subir imagen" class="edit-tool_btn btn-img"><i class="btn-icon icon-img"></i></span>
            <input type="file" id="img_input" class="hide"><span title="Eliminar" class="edit-tool_btn btn-remove"><i class="btn-icon icon-trash"></i></span>
          </div>
        </div>
      </script>
      <script type="text/template" id="tmp-edit-move"> 
        <div class="edit-tool">
          <div class="edit-tool_box"><span title="Mover" class="edit-tool_btn btn-move handle"><i class="btn-icon icon-cursor-move-two"></i></span></div>
        </div>
      </script>
      <script type="text/template" id="tmp-edit-section"> 
        <div class="edit-tool">
          <div class="edit-tool_box"><span title="Mover" class="edit-tool_btn btn-move handle"><i class="btn-icon icon-cursor-move-two"></i></span><span title="Cambiar color de fondo" class="edit-tool_btn btn-bg-section"><i class="btn-icon icon-contrast"></i></span></div>
        </div>
        <div class="edit-tool right">
          <div class="edit-tool_box"><span title="Eliminar" class="edit-tool_btn btn-remove-section"><i class="btn-icon icon-trash"></i></span></div>
        </div>
      </script>
      <script type="text/template" id="tmp-edit-section-img"> 
        <div class="edit-tool">
          <div class="edit-tool_box"><span title="Mover sección" class="edit-tool_btn btn-move handle"><i class="btn-icon icon-cursor-move-two"></i></span>
            <input type="file" id="imgbg_input" class="hide"><span title="Cambiar imagen" class="edit-tool_btn btn-bg-section-img"><i class="btn-icon icon-picture"></i></span>
          </div>
        </div>
        <div class="edit-tool right">
          <div class="edit-tool_box"><span title="Eliminar" class="edit-tool_btn btn-remove-section"><i class="btn-icon icon-trash"></i></span></div>
        </div>
      </script>
      <script type="text/template" id="tmp-edit-link">
        <div class="edit-tool">
          <div class="url-box">
            <div class="input-group">
              <input type="text" placeholder="Your link" class="field form-control">
              <label for="new-tab" class="input-group-addon">
                <input type="checkbox" aria-label="" id="new-tab" class="field-check"> New Tab
              </label><span class="input-group-btn">
                <button type="button" class="btn-cancel btn btn-danger"><i class="btn-icon icon-close"></i></button>
                <button type="button" class="btn-ok btn btn-success"><i class="btn-icon icon-check"></i></button></span>
            </div>
          </div>
        </div>
      </script>
      <script type="text/template" id="tmp-edit-button"> 
        <div class="edit-tool">
          <div class="edit-tool_box">
            <button title="Color de fondo" class="edit-tool_btn btn-bg"><i class="btn-icon icon-contrast"></i></button>
            <button title="Color" class="edit-tool_btn btn-color"><i class="btn-icon icon-tint"></i></button>
            <button title="Bold" class="edit-tool_btn btn-bold"> <i class="btn-icon icon-bold"></i></button>
            <button title="Italic" class="edit-tool_btn btn-italic"><i class="btn-icon icon-italic"></i></button>
            <button title="Linea" class="edit-tool_btn btn-underline"><i class="btn-icon icon-underline"></i></button>
            <button title="Editar" class="edit-tool_btn btn-edit"><i class="btn-icon icon-edit"></i></button>
            <button title="Link" class="edit-tool_btn btn-link"><i class="btn-icon icon-link"></i></button>
            <button title="Popup" type="button" data-toggle="modal" data-target="#myModal" class="edit-tool_btn btn-popup"><i class="btn-icon icon-popup"></i></button>
            <button title="Eliminar" class="edit-tool_btn btn-remove"><i class="btn-icon icon-minus"></i></button>
            <button title="Cerrar" class="edit-tool_btn btn-close"><i class="btn-icon icon-close"></i></button>
          </div>
          <div class="url-box hide">
            <div class="input-group">
              <input type="text" placeholder="Your link" class="field form-control">
              <label for="new-tab" class="input-group-addon">
                <input type="checkbox" aria-label="" id="new-tab" class="field-check"> New Tab
              </label><span class="input-group-btn">
                <button type="button" class="btn-cancel btn btn-danger"><i class="btn-icon icon-close"></i></button>
                <button type="button" class="btn-ok btn btn-success"><i class="btn-icon icon-check"></i></button></span>
            </div>
          </div>
        </div>
      </script>
      <script type="text/template" id="tmp-edit-video"> 
        <div class="edit-tool">
          <div class="video-box">
            <div class="input-group">
              <input type="text" placeholder="Your URL video" class="field form-control"><span class="input-group-btn"><span type="button" class="edit-tool_btn btn-cancel btn btn-danger"><i class="btn-icon icon-close"></i></span><span type="button" class="edit-tool_btn btn-ok btn btn-success"><i class="btn-icon icon-check"></i></span></span>
            </div>
          </div>
        </div>
      </script>
      <script type="text/template" id="tmp-edit-font-item"> 
        <li class="menu-fonts_item"><a href="#" class="menu-fonts_link"><%=name%></a></li>
      </script>
      <script type="text/template" id="tmp-edit-text"> 
        <div class="edit-tool">
          <div class="edit-tool_box"><span type="button" class="edit-tool_btn btn-color"><i class="btn-icon icon-tint"></i></span><span type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="edit-tool_btn btn-font dropdown-toggle"> <i class="btn-icon icon-font"></i></span><span title="Bold" class="edit-tool_btn btn-bold"> <i class="btn-icon icon-bold"></i></span><span title="Italic" class="edit-tool_btn btn-italic"><i class="btn-icon icon-italic"></i></span><span title="Linea" class="edit-tool_btn btn-underline"><i class="btn-icon icon-underline"></i></span><span title="" class="edit-tool_btn btn-strikethrough"><i class="btn-icon icon-strikethrough"></i></span><span title="" class="edit-tool_btn btn-align-left"><i class="btn-icon icon-align-left"></i></span><span title="Centrar" class="edit-tool_btn btn-align-center"><i class="btn-icon icon-align-center"></i></span><span title="" class="edit-tool_btn btn-align-right"><i class="btn-icon icon-align-right"></i></span><span title="Justificar" class="edit-tool_btn btn-align-justify"><i class="btn-icon icon-align-justify"></i></span><span title="Editar" class="edit-tool_btn btn-edit"><i class="btn-icon icon-edit"></i></span><span title="Link" class="edit-tool_btn btn-link"><i class="btn-icon icon-link"></i></span><span title="Eliminar" class="edit-tool_btn btn-remove"><i class="btn-icon icon-minus"></i></span><span title="Cerrar" class="edit-tool_btn btn-close"><i class="btn-icon icon-close"></i></span>
          </div>
          <div class="url-box hide">
            <div class="input-group">
              <input type="text" placeholder="Your link" class="field form-control">
              <label for="new-tab" class="input-group-addon">
                <input type="checkbox" aria-label="" id="new-tab" class="field-check"> New Tab
              </label><span class="input-group-btn">
                <button type="button" class="btn-cancel btn btn-danger"><i class="btn-icon icon-close"></i></button>
                <button type="button" class="btn-ok btn btn-success"><i class="btn-icon icon-check"></i></button></span>
            </div>
          </div>
        </div>
      </script>
            </body>
          </html>
        </div>
      </section>
    </main>
    <!--div class="l-toolbar">
      <div class="toolbar">
        <div class="toolbar_item"><a href="#" class="toolbar_link"><i class="toolbar_icon icon-doc-landscape"></i><span class="toolbar_text"></span></a></div>
        <div class="toolbar_item"><a href="#" class="toolbar_link"><i class="toolbar_icon icon-align-font"></i><span class="toolbar_text"></span></a></div>
        <div class="toolbar_item"><a href="#" class="toolbar_link"><i class="toolbar_icon icon-align-left"></i><span class="toolbar_text"></span></a></div>
        <div class="toolbar_item"><a href="#" class="toolbar_link"><i class="toolbar_icon icon-img"></i><span class="toolbar_text"></span></a></div>
        <div class="toolbar_item"><a href="#" class="toolbar_link"><i class="toolbar_icon icon-video"></i><span class="toolbar_text"></span></a></div>
        <div class="toolbar_item"><a href="#" class="toolbar_link"><i class="toolbar_icon icon-map"></i><span class="toolbar_text"></span></a></div>
        <div class="toolbar_item"><a href="#" class="toolbar_link"><i class="toolbar_icon icon-facebook-square"></i><span class="toolbar_text"></span></a></div>
        <div class="toolbar_item"><a href="#" class="toolbar_link"><i class="toolbar_icon icon-parentheses"></i><span class="toolbar_text"></span></a></div>
      </div>
    </div-->
    <script src="{{ asset('bootstrap/js/min/plugins-min.js') }}"></script>
    <script src="{{ asset('bootstrap/js/text-editor.js') }}"></script>
    <script src="{{ asset('bootstrap/js/main.js') }}"></script>
    <script src="{{ asset('bootstrap/js/main-template.js') }}"></script>
    <script>
    function generateData(){
      $('.edit-box').remove()
      $('.edit-tool').remove()
      jQuery("#print_data").val(jQuery("#printable").html());
        return true;
    }
    </script>
  </body>
</html>