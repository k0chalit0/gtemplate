<!DOCTYPE html>
<html lang="en">
<head>
	@include('includesgen.head')
</head>
<body class="dark-theme">
@if (Auth::check())
	@include('includes.header')

	@include('includes.info-right')
@endif
<div class="page-container">
 @if (Auth::check())
	@include('includesgen.sidebar-left')
@endif

<div id="main-content"> 
	@if(Session::has('message'))
	    <div class="alert alert-success">
	        {{ Session::get('message') }}
	    </div>
	@endif
	<div class="page-content">
	@yield('content')
	</div>
</div><!--/main-content end-->
</div><!--/page-container end-->
	@include('includesgen.scripts')
	@include('includesgen.footer')
	@yield('footer_scripts')
</body>
</html>