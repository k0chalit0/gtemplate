@extends('gen')
@section('content')
	<div id="printable" class="main-sortable">
	{{ $usertemplate->contenthtml}}
	</div>
@endsection

@section('footer_scripts')
	<script language="JavaScript">
	  window.onbeforeunload = confirmExit;
	  function confirmExit()
	  {
	    return "Ha intentado salir de esta pagina. Si ha realizado algun cambio en los campos sin hacer clic en el boton Guardar, los cambios se perderan. Seguro que desea salir de esta pagina? ";
	  }
	</script>
@endsection
