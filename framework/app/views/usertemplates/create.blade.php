<!-- app/views/usertemplates/create.blade.php -->

@extends('master')

@section('content')
    <div class="page-content">

	    <div class="container">

	    <nav class="navbar navbar-inverse">
	        <ul class="nav navbar-nav">
	            <li><a href="{{ URL::to('mistemplates') }}">Ver todos</a></li>
	        </ul>
	    </nav>
		<h1>Crear Nuevo Template</h1>
		<!-- if there are creation errors, they will show here -->
		{{ HTML::ul($errors->all()) }}
		{{ Form::open(array('url' => 'usertemplates', 'enctype'=>'multipart/form-data')) }}

			{{ Field::text('name', '', array('autocomplete'=>'off', NULL )) }}
			{{-- <input type="button" id="disponibilidad" value="Comprobar disponibilidad"> --}}
			<span id="mensajeDisponible"> </span>
			{{-- Field::text('type') --}}
			<hr class="soften">
			<div class="margin-updown">
			<p>{{ Form::label('templates', 'Templates')}}</p>

			@foreach($newtemplate as $key => $tem)
            <li class="selection_item">
              
              <a href="#" class="selection_link">
                <div class="selection_overlay"><i class="icon-check selection_icon-overlay"></i></div>
                <img src="../../media/templates/{{ $tem->image }}" alt="" class="selection_img">
              </a>
            	{{ Form::radio('template',$tem->id)}}
            </li> 
            
          @endforeach
          	</div>
		    <p>{{ Form::submit('Crear', array('class' => 'btn btn-btn-success', 'disabled' => 'disabled')) }}</p>

		{{ Form::close() }}
	    
	    </div>
	</div>
@endsection

@section('footer_scripts')
<script>
	jQuery('input[name="name"]').keyup(
		function(){
			mivariable = jQuery('input[name="name"]').val();
			mivariable = mivariable.trim();
			res = mivariable.split(" ").join("-");
			res = res.toLowerCase();
			var is_exist;
			$.getJSON( "/visitors/seo", function( data ){
				is_exist = data.filter(function(item){
					return item.slug == res;
				}).length > 0;
				if(is_exist)
				{
					jQuery('#mensajeDisponible').html('No esta Disponible');
					jQuery('#mensajeDisponible').css('color','red');
					jQuery('input[type="submit"]').attr('disabled',true);
				}
				else
				{
					jQuery('#mensajeDisponible').html('Disponible');
					jQuery('#mensajeDisponible').css('color','green');
					habilitar = jQuery('input[checked="checked"]').length;
					if(habilitar == 1)
					{
						jQuery('input[type="submit"]').attr('disabled',false);
					}
					else
					{
						jQuery('input[type="submit"]').attr('disabled',true);
					}
				}


				});

		} )

	jQuery('a.selection_link').click(function() 
		{ 
			x = jQuery('#mensajeDisponible').html(); 
			if(x=="Disponible")
			{ 
				jQuery('input[type="submit"]').attr('disabled', false);
			}  
		});
	</script>
@endsection