@extends('master')

@section('content')
@if(Auth::check())
<div class="page-content">
    <div class="container">
    @if(isset($mensaje))
        <div>
        {{ $mensaje }}
        </div>
    @endif
	<h1>Mis Páginas <a class="btn btn-small btn-success" href="{{ URL::to('usertemplates/create') }}"><i class="fa fa-plus"></i>  Nuevo</a></h1>

	 <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>ID</td>
                <td>Nombre Template</td>
                <td>Estado</td>
                <td>Creacion</td>
                <td>Acciones</td>
            </tr>
        </thead>
        <tbody>
        @foreach($usertemplates as $key => $value)
            <tr>
                <td>{{ $value->id }}</td>
                <td>{{ $value->name }}</td>
                <td>{{ $value->status }}</td>
                <td>{{ $value->created_at }}</td>

                <td>
                    <a class="btn btn-small btn-warning @if($value->slug=='') disabled @endif" href="{{ URL::to('/t/' . $value->slug) }}">View Live</a>
                    <a class="btn btn-small btn-success" href="{{ URL::to('usertemplates/show/' . $value->id) }}">Ver|editar</a>
                    <a class="btn btn-small btn-primary" href="{{ URL::to('usertemplates/publicar/' . $value->id) }}" title="Editar y Asignar SEO">Publicar</a>
                    <a class="btn btn-small btn-info  @if($value->padre!=0) disabled @endif" href="{{ URL::to('usertemplates/clonar/' . $value->id) }}">clonar</a>
                    <a class="btn btn-small btn-info @if($value->slug=='') disabled @endif" href="{{ URL::to('/estadisticas/'.$value->id)}}"><i class="fa fa-bar-chart-o"></i> Estadisticas</a>
                    {{ Form::open(array('method'=> 'DELETE', 'route' => array('usertemplates.destroy', $value->id))) }}        
                    {{ Form::submit('Eliminar', array('class' => 'btn btn-danger')) }}
                    {{ Form::close() }}

                </td>
            </tr>
        @endforeach
        </tbody>
    </div>
</div>
@else
    <div class="page-content">
      <center><div class="jumbotron">
        <h1>Bienvenido a GenTemplate</h1>
        <a href="{{ route('sign_in') }}">
          <button type="button" class="btn btn-primary">Ingresar</button>
        </a>
        <a href="{{ route('sign_up') }}">
          <button type="button" class="btn btn-success">Registrar</button>
        </a>
      </div></center>
    </div>
  @endif

@endsection