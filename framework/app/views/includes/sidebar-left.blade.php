<div class="nav-collapse top-margin fixed box-shadow2 hidden-xs" id="sidebar">
    <div class="leftside-navigation">
        <div class="sidebar-section sidebar-user clearfix">
            <div class="sidebar-user-avatar"> <a href="#"> <img alt="avatar" src="/images/avatar1.jpg"> 
            </div>
            <div class="sidebar-user-name">{{Auth::user()->first_name." ".Auth::user()->last_name}}</div>
            <div class="sidebar-user-links"> <a title="" data-placement="bottom" data-toggle="" href="{{ URL::to('u/'.Auth::user()->username)}}" data-original-title="User"><i class="fa fa-user"></i></a> <a title="" data-placement="bottom" data-toggle="" href="inbox.html" data-original-title="Messages"><i class="fa fa-envelope-o"></i></a> <a title="" data-placement="bottom" data-toggle="" href="lockscreen.html" data-original-title="Logout"><i class="fa fa-sign-out"></i></a> </div>
        </div>
          <ul id="nav-accordion" class="sidebar-menu">
      <!--li>
        <h3>Settings</h3>
    </li>
    <li> <a href="#" class="active"> <i class="fa fa-dashboard"></i> <span>Dashboard</span> </a> </li>
    
    <li class="sub-menu dcjq-parent-li"> <a href="javascript:;" class="dcjq-parent"> <i class="fa fa-book"></i> <span>UI Elements</span><b class="badge bg-danger pull-right">10</b></a>
        <ul class="sub">
          <li><a href="{{ URL::to('theme/general') }}"><i class="fa fa-angle-right"></i> General</a></li>
          <li><a href="{{ URL::to('theme/buttons') }}"><i class="fa fa-angle-right"></i> Buttons</a></li>
          <li><a href="{{ URL::to('theme/slider') }}"><i class="fa fa-angle-right"></i> Slider</a></li>
          <li><a href="{{ URL::to('theme/nestable') }}"><i class="fa fa-angle-right"></i> Nestable</a></li>
          <li><a href="{{ URL::to('theme/grid') }}"><i class="fa fa-angle-right"></i> Grids</a></li>
          <li><a href="{{ URL::to('theme/icons') }}"><i class="fa fa-angle-right"></i> Icons</a></li>
          <li><a href="{{ URL::to('theme/tab-accordions') }}"><i class="fa fa-angle-right"></i> Tab & Accordions</a></li>
          <li><a href="{{ URL::to('theme/calendar') }}"><i class="fa fa-angle-right"></i> Calender</a></li>
          <li><a href="{{ URL::to('theme/porlets') }}"><i class="fa fa-angle-right"></i> Portlets</a></li>
          <li><a href="{{ URL::to('theme/invoice') }}"><i class="fa fa-angle-right"></i> Invoice</a></li>
          <li><a href="{{ URL::to('theme/treeview') }}"><i class="fa fa-angle-right"></i> Treeview</a></li>
          <li><a href="{{ URL::to('theme/address-book') }}"><i class="fa fa-angle-right"></i> Address Book</a></li>
      </ul>
  </li>
  <li class="sub-menu dcjq-parent-li"> <a href="javascript:;" class="dcjq-parent"> <i class="fa fa-th"></i> <span>Tables</span><b class="badge bg-danger pull-right">2</b></a>
    <ul class="sub">
      <li><a href="{{ URL::to('theme/basic-tables') }}"><i class="fa fa-angle-right"></i> Basic Table</a></li>
      <li><a href="{{ URL::to('theme/data-tables') }}"><i class="fa fa-angle-right"></i> Data Table</a></li>
  </ul>
</li>
<li class="sub-menu dcjq-parent-li"> <a href="javascript:;" class="dcjq-parent"> <i class="fa fa-tasks"></i> <span>Form Components</span><b class="badge bg-danger pull-right">7</b></a>
    <ul class="sub">
      <li><a href="{{ URL::to('theme/form-elements') }}"><i class="fa fa-angle-right"></i>Form Elements</a></li>
      <li><a href="{{ URL::to('theme/form-validation') }}"><i class="fa fa-angle-right"></i>Form Validation</a></li>
      <li><a href="{{ URL::to('theme/file-upload') }}"><i class="fa fa-angle-right"></i>Multiple File Upload</a></li>
      <li><a href="{{ URL::to('theme/x-editable') }}"><i class="fa fa-angle-right"></i>X-Editable</a></li>
  </ul>
</li>
<li>
    <h3>Management</h3>
</li>
<li class="sub-menu dcjq-parent-li"> <a href="javascript:;" class="dcjq-parent"> <i class="fa fa-envelope"></i> <span>Mail </span><b class="badge bg-danger pull-right">3</b></a>
    <ul class="sub">
      <li><a href="{{ URL::to('theme/inbox') }}"><i class="fa fa-angle-right"></i> Inbox</a></li>
      <li><a href="{{ URL::to('theme/compose-mail') }}"><i class="fa fa-angle-right"></i> Compose Mail</a></li>
      <li><a href="{{ URL::to('theme/read') }}"><i class="fa fa-angle-right"></i> View Mail</a></li>
  </ul>
</li>
<li class="sub-menu dcjq-parent-li"> <a href="javascript:;" class="dcjq-parent"> <i class=" fa fa-bar-chart-o"></i> <span>Charts</span></span></a>
    <ul class="sub">
      <li><a href="{{ URL::to('theme/morris-charts') }}"><i class="fa fa-angle-right"></i> Morris</a></li>
      <li><a href="{{ URL::to('theme/graph') }}"><i class="fa fa-angle-right"></i> Graph</a></li>
      <li><a href="{{ URL::to('theme/float-chart') }}"><i class="fa fa-angle-right"></i> Float Charts</a></li>
  </ul>
</li>

<li> <a href="{{ URL::to('theme/gallery') }}"> <i class="fa fa-film"></i> <span>Gallery</span> </a> </li>
<li class="sub-menu dcjq-parent-li"> <a href="javascript:;" class="dcjq-parent"> <i class="fa fa-glass"></i> <span>Extra</span><b class="badge bg-danger pull-right">5</b></a>
    <ul class="sub">
      <li><a href="{{ URL::to('theme/profile') }}"><i class="fa fa-angle-right"></i> Profile</a></li>
      <li><a href="{{ URL::to('theme/timeline') }}"><i class="fa fa-angle-right"></i> Timeline</a></li>
      <li><a href="{{ URL::to('theme/blank-page') }}"><i class="fa fa-angle-right"></i> Blank Page</a></li>
      <li><a href="{{ URL::to('theme/lockscreen') }}"><i class="fa fa-angle-right"></i>Lock Screen</a></li>
      <li><a href="error-404.html"><i class="fa fa-angle-right"></i> Error-404</a></li>
      <li><a href="{{ URL::to('theme/login') }}"><i class="fa fa-angle-right"></i> Login</a></li>
  </ul>
</li-->
<li>
    <h3>GenTemplate</h3>
</li>
<li> <a href="{{ route('variaciones') }}"> <i class="fa fa-cogs"></i> <span>Mis variaciones</span> </a> </li>
<li> <a href="{{ route('mistemplates') }}"> <i class="fa fa-list-alt"></i> <span>Mis páginas</span> </a> </li>
@include('includes.menuadmin')
</ul><!--/nav-accordion sidebar-menu--> 
    </div><!--/leftside-navigation--> 
</div><!--/sidebar--> 
