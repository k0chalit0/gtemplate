<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="{{ asset('js/jquery-2.0.2.min.js') }}"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script> 
<script src="{{ asset('js/accordion.js') }}"></script> 
<script src="{{ asset('js/common-script.js') }}"></script> 
<script src="{{ asset('js/jquery.nicescroll.js') }}"></script> 
<!--script src="{{ asset('plugins/demo-slider/demo-slider.js') }}"></script-->
<script src="{{ asset('js/app.js') }}"></script> 
@if(isset($page))
	@if($page == 'calendario')
		<script src="{{ asset('plugins/fullcalendar/jquery-ui.custom.min.js')}}"></script> 
		<script src="{{ asset('plugins/fullcalendar/fullcalendar.min.js')}}"></script> 
		<script src="{{ asset('plugins/fullcalendar/external-draging-calendar.js')}}"></script>
	@elseif($page == 'data-tables')
		<script src="{{ asset('plugins/data-tables/jquery.dataTables.js') }}"></script> 
		<script src="{{ asset('plugins/data-tables/DT_bootstrap.js') }}"></script> 
		<script src="{{ asset('plugins/data-tables/dynamic_table_init.js') }}"></script>
		<script src="{{ asset('plugins/edit-table/edit-table.js') }}"></script>
		<script>
		          jQuery(document).ready(function() {
		              EditableTable.init();
		          });
		 </script>
	@elseif($page=='file-upload')
		<script src="{{ asset('plugins/file-uploader/js/vendor/jquery.ui.widget.js') }}"></script> 
		<script src="{{ asset('plugins/file-uploader/js/tmpl.min.js') }}"></script> 
		<script src="{{ asset('plugins/file-uploader/js/load-image.min.js') }}"></script> 
		<script src="{{ asset('plugins/file-uploader/js/canvas-to-blob.min.js') }}"></script> 
		<script src="{{ asset('plugins/file-uploader/js/jquery.blueimp-gallery.min.js') }}"></script> 
		<script src="{{ asset('plugins/file-uploader/js/jquery.iframe-transport.js') }}"></script> 
		<script src="{{ asset('plugins/file-uploader/js/jquery.fileupload.js') }}"></script> 
		<script src="{{ asset('plugins/file-uploader/js/jquery.fileupload-process.js') }}"></script> 
		<script src="{{ asset('plugins/file-uploader/js/jquery.fileupload-image.js') }}"></script> 
		<script src="{{ asset('plugins/file-uploader/js/jquery.fileupload-audio.js') }}"></script> 
		<script src="{{ asset('plugins/file-uploader/js/jquery.fileupload-video.js') }}"></script> 
		<script src="{{ asset('plugins/file-uploader/js/jquery.fileupload-validate.js') }}"></script> 
		<script src="{{ asset('plugins/file-uploader/js/jquery.fileupload-ui.js') }}"></script> 
		<script src="{{ asset('plugins/file-uploader/js/main.js') }}"></script>
		<!-- The template to display files available for upload --> 
		<script id="template-upload" type="text/x-tmpl">
		      {% for (var i=0, file; file=o.files[i]; i++) { %}
		      <tr class="template-upload fade">
		          <td>
		              <span class="preview"></span>
		          </td>
		          <td>
		              <p class="name">{%=file.name%}</p>
		              <strong class="error text-danger"></strong>
		          </td>
		          <td>
		              <p class="size">Processing...</p>
		              <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
		          </td>
		          <td>
		              {% if (!i && !o.options.autoUpload) { %}
		              <button class="btn btn-primary start" disabled>
		                  <i class="glyphicon glyphicon-upload"></i>
		                  <span>Start</span>
		              </button>
		              {% } %}
		              {% if (!i) { %}
		              <button class="btn btn-warning cancel">
		                  <i class="glyphicon glyphicon-ban-circle"></i>
		                  <span>Cancel</span>
		              </button>
		              {% } %}
		          </td>
		      </tr>
		      {% } %}
		  </script> 
		<!-- The template to display files available for download --> 
		<script id="template-download" type="text/x-tmpl">
		      {% for (var i=0, file; file=o.files[i]; i++) { %}
		      <tr class="template-download fade">
		          <td>
		            <span class="preview">
		                {% if (file.thumbnailUrl) { %}
		                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
		                {% } %}
		            </span>
		          </td>
		          <td>
		              <p class="name">
		                  {% if (file.url) { %}
		                  <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
		                  {% } else { %}
		                  <span>{%=file.name%}</span>
		                  {% } %}
		              </p>
		              {% if (file.error) { %}
		              <div><span class="label label-danger">Error</span> {%=file.error%}</div>
		              {% } %}
		          </td>
		          <td>
		              <span class="size">{%=o.formatFileSize(file.size)%}</span>
		          </td>
		          <td>
		              {% if (file.deleteUrl) { %}
		              <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
		              <i class="glyphicon glyphicon-trash"></i>
		              <span>Delete</span>
		              </button>
		              <input type="checkbox" name="delete" value="1" class="toggle">
		              {% } else { %}
		              <button class="btn btn-warning cancel">
		                  <i class="glyphicon glyphicon-ban-circle"></i>
		                  <span>Cancel</span>
		              </button>
		              {% } %}
		          </td>
		      </tr>
		      {% } %}
		  </script>
	@elseif($page=='float-chart')
		<!--jQuery Flot Chart-->
		<script src="{{ asset('plugins/flot-chart/jquery.flot.js') }}"></script>
		<script src="{{ asset('plugins/flot-chart/jquery.flot.tooltip.min.js') }}"></script>
		<script src="{{ asset('plugins/flot-chart/jquery.flot.resize.js') }}"></script>
		<script src="{{ asset('plugins/flot-chart/jquery.flot.pie.resize.js') }}"></script>
		<script src="{{ asset('plugins/flot-chart/jquery.flot.selection.js') }}"></script>
		<script src="{{ asset('plugins/flot-chart/jquery.flot.stack.js') }}"></script>
		<script src="{{ asset('plugins/flot-chart/jquery.flot.time.js') }}"></script>
		<script src="{{ asset('js/flot.chart.init.js') }}"></script>

	@elseif($page=='form-elements')
		<script type="text/javascript" src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script> 
		<script type="text/javascript" src="{{ asset('plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script> 
		<script type="text/javascript" src="{{ asset('plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script> 
		<script type="text/javascript" src="{{ asset('plugins/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script> 
		<script type="text/javascript" src="{{ asset('js/form-components.js') }}"></script> 
		<script type="text/javascript"  src="{{ asset('plugins/input-mask/jquery.inputmask.min.js') }}"></script> 
		<script type="text/javascript"  src="{{ asset('plugins/input-mask/demo-mask.js') }}"></script> 
		<script type="text/javascript"  src="{{ asset('plugins/toggle-switch/toggles.min.js') }}"></script> 
		<script type="text/javascript"  src="{{ asset('plugins/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script> 
		<script type="text/javascript"  src="{{ asset('plugins/dropzone/dropzone.min.js') }}"></script> 
		<script type="text/javascript"  src="{{ asset('plugins/bootstrap-wizard/bootstrap-wizard.min.js') }}"></script> 
		<script type="text/javascript" src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script> 
		<script type="text/javascript"  src="{{ asset('js/form-wizard.js') }}"></script> 
	@elseif($page=='form-validation')
		<script src="{{ asset('plugins/validation/parsley.min.js') }}"></script>
	@elseif($page=='gallery')
		<script src="{{ asset('plugins/gallery/jquery.magnific-popup.min.js') }}"></script> 
		<script>
		 $(document).ready(function(){
		  if ($.fn.magnificPopup) {
					$('.image-zoom').magnificPopup({
						type: 'image',
						closeOnContentClick: false,
						closeBtnInside: true,
						fixedContentPos: true,
						mainClass: 'mfp-no-margins mfp-with-zoom', 
						image: {
							verticalFit: true,
							tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
						}
					});

				}

		    });
		</script>
	@elseif($page=='morris-charts')
		<script src="{{ asset('plugins/morris/morris.min.js') }}"></script> 
		<script src="{{ asset('plugins/morris/raphael-min.js') }}"></script> 
		<script src="{{ asset('plugins/morris/morris-script.js') }}"></script>
	@elseif($page=='nestable')
		<script src="{{ asset('plugins/nestable/jquery.nestable.js') }}"></script> 
		<script src="{{ asset('plugins/nestable/nestable-edit.js') }}"></script>
	@elseif($page=='slider')
		<script src="{{ asset('plugins/bootstrap-slider/bootstrap-slider.js') }}"></script> 
		<script>
			$(document).ready(function() {		
				$('.slider-element').slider();
			});
		</script>
	@elseif($page=='treeview')
		<script src="{{ asset('plugins/tree-views/jquery.cookie.js') }}" type="text/javascript"></script>
		<script src="{{ asset('plugins/tree-views/jquery.treeview.js') }}" type="text/javascript"></script>
		<script type="text/javascript" src="{{ asset('plugins/tree-views/edit-treeview.js') }}"></script>
	@elseif($page=='graph')
		<script src="{{ asset('js/graph.js') }}" type="text/javascript"></script>
		<script src="{{ asset('js/edit-graph.js') }}" type="text/javascript"></script>
	@elseif($page=='x-editable')
		<script src="{{ asset('plugins/bootstrap-editable/bootstrap-editable.min.js') }}"></script>
		<script src="{{ asset('plugins/x-editable/form-x-editable.js') }}"></script> 
		<script src="{{ asset('plugins/x-editable/form-x-editable-demo.js') }}"></script>
		<script src="{{ asset('plugins/mockjax/jquery.mockjax.min.js') }}"></script> 
		<script src="{{ asset('js/moment.min.js') }}"></script>
		<script src="{{ asset('js/select2.min.js') }}"></script> 
		<script src="{{ asset('js/address.min.js') }}"></script> 
		<script src="{{ asset('plugins/typeahead/typeahead.min.js') }}"></script> 
		<script src="{{ asset('plugins/typeahead/typeaheadjs.min.js') }}"></script> 
	@endif
@endif

@if(isset($usertemplate))
		<script src="{{ asset('js/graph.js') }}"></script>
		<script src="{{ asset('plugins/morris/morris.min.js') }}"></script>
		<script src="{{ asset('plugins/morris/raphael-min.js') }}"></script>
		
	    <script>
	    $(function () {
		    $.get( "/visitors/mchart/{{$usertemplate->id}}", function( xdata ){
		        Morris.Donut({
		          element: 'donut-chart',
		          data: xdata,  
		          colors: ['#e54c47', '#36d0ba', '#428bca', '#5bc0de', '#bfd738'],
		          formatter: function (y) { return y }
		        });
		      });

		    $.get( "/visitors/mchartarea/{{$usertemplate->id}}", function( xdata1 ){
		          Morris.Area(
		          {    
		            element: 'area-chart',
		            data: xdata1,  

		          xkey: 'period',
		          ykeys: ['original', 'clon'],
		          labels: ['Original', 'Clon'],
		          hideHover: 'auto',
		          lineWidth: 2,
		          pointSize: 10,
		          lineColors: ['#4a8bc2', '#ff6c60'],
		          fillOpacity: 0.5,
		          smooth: true,
		        });
		      });
		    $.get( "/visitors/mchartbar/{{$usertemplate->id}}", function( xdata2 ){
			    Morris.Bar({
			        element: 'bar-chart',
			        data: xdata2
			          
			          // {device: 'original', geekbench: 10},
			          // {device: 'clon', geekbench: 5}
			        ,
			        xkey: 'slug',
			        ykeys: ['visits'],
			        labels: ['Visitas'],
			        barRatio: 0.4,
			        xLabelAngle: 35,
			        hideHover: 'auto',
			        barColors: ['#6883a3']
			      });
			});

			$.getJSON( "/visitors/g/{{$usertemplate->id}}", function( data ){
				$('#graph').graphify({
					//options: true,
					start: 'combo',
					obj: data,	
				});
			});
		});
	    </script>
	    @endif

	    @if(isset($grafs))
	    	<script src="{{ asset('js/graph.js') }}"></script>
			<script>
				var data;
				$(function () {
					$.getJSON( "/visitors/g/{{$grafs->id}}", function( data ){
						$('#graph').graphify({
							//options: true,
							start: 'combo',
							obj: data,	
						});
					});
				});
			</script>
		@endif
		
		@if(isset($estadisticas))
	    	<script src="{{ asset('js/graph.js') }}"></script>
			<script>
				var data;
				$(function () {
					$.getJSON( "/visitors/g/{{$estadisticas['id']}}", function( data ){
						$('#graph').graphify({
							//options: true,
							start: 'combo',
							obj: data,	
						});
					});
				});
			</script>
		@endif

		@if(isset($newtemplate))
			<script>
				jQuery(".selection_link").click(function(e)
				{
					e.preventDefault();
					jQuery('.selection_link .selection_overlay').removeClass('elegido');
					jQuery(this).find('.selection_overlay').addClass('elegido');
					jQuery(':radio').attr('checked',false); 
					jQuery(this).siblings(':radio').attr('checked',true);
				});
			</script>
		@endif





