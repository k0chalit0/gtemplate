	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
	<title>SUMECLIENTES</title>

	<!-- Bootstrap -->
	<link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://fontastic.s3.amazonaws.com/mBcpKha37mryLvEf949bD9/icons.css">
	<link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/style.css') }}" rel="stylesheet">
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('css/style-responsive.css') }}" rel="stylesheet">
	<link href="{{ asset('css/jquerysctipttop.css') }}" rel="stylesheet" type="text/css">
	<!--link href="{{ asset('gentemplate/css/app.css') }}" rel="stylesheet"-->
	@if(isset($page))
		@if($page == 'calendario'))
			<link href="{{ asset('plugins/fullcalendar/fullcalendar.css')}}" rel="stylesheet">
			<link href="{{ asset('plugins/fullcalendar/fullcalendar.print.css')}}" rel="stylesheet">
		@elseif($page == 'data-tables')
			<link href="{{ asset('plugins/advanced-datatable/css/demo_table.css') }}" rel="stylesheet">
			<link href="{{ asset('plugins/advanced-datatable/css/demo_page.css') }}" rel="stylesheet">
		@elseif($page == 'file-upload')	
			<link rel="stylesheet" href="{{ asset('plugins/file-uploader/css/blueimp-gallery.min.css') }}">
			<link rel="stylesheet" href="{{ asset('plugins/file-uploader/css/jquery.fileupload.css') }}">
			<link rel="stylesheet" href="{{ asset('plugins/file-uploader/css/jquery.fileupload-ui.css') }}">
		@elseif($page == 'form-elements')	
			<link href="{{ asset('plugins/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" rel="stylesheet">
			<link href="{{ asset('plugins/dropzone/dropzone.css') }}" rel="stylesheet">
			<link rel="stylesheet" type="text/css" href="{{ asset('plugins/bootstrap-datepicker/css/datepicker.css') }}" />
			<link rel="stylesheet" type="text/css" href="{{ asset('plugins/bootstrap-timepicker/compiled/timepicker.css') }}" />
			<link rel="stylesheet" type="text/css" href="{{ asset('plugins/bootstrap-colorpicker/css/colorpicker.css') }}" />
		@elseif($page == 'gallery')	
			<link href="{{ asset('plugins/gallery/magnific-popup.css') }}" rel="stylesheet">
		@elseif($page == 'graph')	
			<link href="{{ asset('css/jquerysctipttop.css') }}" rel="stylesheet" type="text/css">
		@elseif($page == 'morris-charts')	
			<link href="{{ asset('plugins/morris/morris-0.4.3.min.css') }}" rel="stylesheet">
		@elseif($page == 'nestable')
			<link href="{{ asset('plugins/nestable/nestable.css') }}" rel="stylesheet">
		@elseif($page == 'slider')
			<link href="{{ asset('plugins/bootstrap-slider/slider.css') }}" rel="stylesheet">
		@elseif($page == 'x-editable')
			<link href="{{ asset('css/select2.css') }}" rel="stylesheet">
			<link href="{{ asset('plugins/bootstrap-editable/bootstrap-editable.css') }}" rel="stylesheet">
		@endif
	@endif
	@if(isset($usertemplate))
		<link href="{{ asset('plugins/morris/morris-0.4.3.min.css') }}" rel="stylesheet">
	@endif
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	      <script src="{{ asset('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') }}"></script>
	      <script src="{{ asset('https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js') }}"></script>
	    <![endif]-->