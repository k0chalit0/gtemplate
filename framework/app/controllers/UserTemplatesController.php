<?php

use GenTemplate\Entities\UserTemplate;
use GenTemplate\Entities\Template;
use GenTemplate\Managers\RegisterManager;
//use GenTemplate\Entities\UserTemplate;
use GenTemplate\Components\FieldBuilder;
use GenTemplate\Entities\User;
use GenTemplate\Entities\SeoTemplate;
use GenTemplate\Entities\Visitor;


class UserTemplatesController extends BaseController
{

	private $cabeceras = '<html class="no-js">
  <head>
    <meta charset="utf-8">
    <meta name="language" content="es">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title>GenTemplate</title>
    <link rel="stylesheet" href="https://fontastic.s3.amazonaws.com/mBcpKha37mryLvEf949bD9/icons.css">
    <link rel="stylesheet" href="http://sumeclientes.net/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://sumeclientes.net/gentemplate/css/app.css">
  	</head>
  	<body>
	';

  private $footers = '
    <script src="http://sumeclientes.net/bootstrap/js/min/plugins-min.js"></script>
    <script src="http://sumeclientes.net/bootstrap/js/main.js"></script>
  </body>
</html>';

	public function create()
    {
    	if(!Auth::check())
        {
            return Redirect::route('home');  
        }
    	$templates = Template::all();
        return View::make('usertemplates.create')->with('newtemplate',$templates);
    }

    public function store(){
    	$usertemplate = new UserTemplate;
    	$usertemplate->name = trim(Input::get('name'));
    	// $usertemplate->type = Input::get('type');
    	$usertemplate->status = "enable";

    	$template = Template::find(Input::get('template'));

    	$foldergeneric = 'plantillas/generics';
		$folderusere= 'plantillas/users/'.Auth::user()->id.'/edit';
		$folderuserp= 'plantillas/users/'.Auth::user()->id.'/previews';
		$folderuserd= 'plantillas/users/'.Auth::user()->id.'/downloads';
		
		if (!file_exists($folderusere)) {
    		mkdir($folderusere, 0777, true);
		}
		if (!file_exists($folderuserp)) {
    		mkdir($folderuserp, 0777, true);
		}
		if (!file_exists($folderuserd)) {
    		mkdir($folderuserd, 0777, true);
		}
		$name = "template_".Auth::user()->id."_".strtolower(str_replace(' ', '-', $usertemplate->name)).".html";
		copy($foldergeneric.'/'.$template->defaulthtml, $folderuserp.'/'.$name);
		copy($foldergeneric.'/'.$template->defaulthtml, $folderusere.'/'.$name);
		copy($foldergeneric.'/'.$template->defaulthtml, $folderuserp.'/'.$name);
		$usertemplate->type="original";
		$usertemplate->editablehtml = $name;
		$usertemplate->finalhtml = $name;
		$usertemplate->user_id = Auth::user()->id;
		$usertemplate->save();
		$last = UserTemplate::all()->last()->id;

		Session::flash('message', '<i class="fa fa-thumbs-o-up"></i>&nbsp;&nbsp;&nbsp; El template ha sido creado exitosamente');
		return Redirect::to('usertemplates/show/'.$last);

    	//var_dump(Input::all());
    }

	function creartemplate($idtemplate)
	{
		if(!Auth::check())
        {
            return Redirect::route('home');  
        }
		$usertemplate = new UserTemplate;
		$usertemplate->name = "Template".rand(1,1000);
		$usertemplate->type = "test";
		$usertemplate->status = "enable";

		$template = Template::find($idtemplate);

		$foldergeneric = 'plantillas/generics';
		$folderusere= 'plantillas/users/'.Auth::user()->id.'/edit';
		$folderuserp= 'plantillas/users/'.Auth::user()->id.'/previews';
		$folderuserd= 'plantillas/users/'.Auth::user()->id.'/downloads';
		
		if (!file_exists($folderusere)) {
    		mkdir($folderusere, 0777, true);
		}
		if (!file_exists($folderuserp)) {
    		mkdir($folderuserp, 0777, true);
		}
		if (!file_exists($folderuserd)) {
    		mkdir($folderuserd, 0777, true);
		}
		$name = "template_".Auth::user()->id."_".date('YmdHis').".html";
		copy($foldergeneric.'/'.$template->defaulthtml, $folderusere.'/'.$name);
		copy($foldergeneric.'/'.$template->defaulthtml, $folderuserp.'/'.$name);

		$usertemplate->editablehtml = $name;
		$usertemplate->finalhtml = $name;
		$usertemplate->user_id = Auth::user()->id;
		$usertemplate->save();
		$last = UserTemplate::all()->last()->id;
		
		return Redirect::to('usertemplates/show/'.$last);
	}

	function show($id)
	{
		$usertemplate = UserTemplate::find($id);
		if(Auth::user()->id != $usertemplate->user_id)
		{
			return Redirect::route('mistemplates');
		}
		$filegenerate = '';
		$folderusere= 'plantillas/users/'.Auth::user()->id.'/edit';
		ob_start();
		require_once ($folderusere.'/'.$usertemplate->editablehtml);
		$filegenerate = ob_get_clean();
		$usertemplate->contenthtml = $filegenerate;

		// $this->generarpreview($id, $filegenerate);
        
        return View::make('usertemplates.editable')
            ->with('usertemplate', $usertemplate);
	}

	function clonar($id)
	{
		$utemplateoriginal = UserTemplate::find($id);
		if($utemplateoriginal->padre != 0)
		{
			$mensaje = "Este template ya es un clon";
			return Redirect::to('mistemplates')->with('mensaje',$mensaje);
		}
		$count = UserTemplate::where('padre', $id)->count();
		if($count>0)
		{
			$mensaje = "El template ya ha sido clonado";
			return Redirect::to('mistemplates')->with('mensaje',$mensaje);
		}

		$utemplateclon = new UserTemplate;	
		$utemplateclon->type= "clon";
		$utemplateclon->name .= $utemplateoriginal->name."-clon";
		$utemplateclon->status = "enable";
		$utemplateclon->padre = $id;

		$folderusere= 'plantillas/users/'.Auth::user()->id.'/edit';
		$folderuserp= 'plantillas/users/'.Auth::user()->id.'/previews';
		
		$name = "template_".Auth::user()->id."_".date('YmdHis').".html";
		copy($folderusere.'/'.$utemplateoriginal->editablehtml, $folderusere.'/'.$name);
		copy($folderuserp.'/'.$utemplateoriginal->finalhtml, $folderuserp.'/'.$name);
		unset($utemplateoriginal);
		$utemplateclon->user_id = Auth::user()->id;
		$utemplateclon->editablehtml = $name;
		$utemplateclon->finalhtml = $name;
		$utemplateclon->save();

        return Redirect::to('mistemplates');
	}

	function generarpreview($id,$content){
		$usertemplate = UserTemplate::find($id);
		$folderuserp= 'plantillas/users/'.Auth::user()->id;
		ob_start();
		$content;
		file_put_contents($folderuserp.'/previews/'.$usertemplate->finalhtml, ob_get_contents());
		//$files = glob($folderuserp.'/*');
		//Zipper::make($folderuserp.'/test.zip')->add($files);
	}

	function limpiar($template){
		$limpiando= array('contenteditable="true"', 
			'editable',
			'el-movable',
			'class="dnd"', 
			'dnd', 
			'select-element', 
			'block-movable' , 
			'select-element-video', 
			'<span class="handle ui-sortable-handle"><i class="icon-align-justify"></i></span>',
			'<script type="text/template" id="tmp-handle"><span class="handle"><i class="icon-move"></i></span></script>',
			'<span class="handle"><i class="icon-align-justify"></i></span>',
			'<div class="edit-box">'
		);
		$template = str_replace($limpiando,'', $template);
		return $template;
	}

	function savetemplate($elemento, $folder){
		ob_start();
		echo $elemento;
		file_put_contents($folder, ob_get_contents());
		ob_end_clean();
		return true;
	}

	function preview(){
		$var = Input::get('print_data');
		$var = str_replace(array('<span class="handle ui-sortable-handle"><i class="icon-align-justify"></i></span>','<span class="handle"><i class="icon-align-justify"></i></span>', 
			'<div class="edit-box">
        <div class="edit-buttons">
          <div role="group" class="btn-group">
            <button type="button" class="btn-bg btn btn-default colorpicker-element"><i class="btn-icon icon-contrast"></i></button>
          </div>
        </div>
      </div>'), '', $var);
		$preview =  $this->limpiar($var);
		$preview = str_replace('"','\'', $preview);
		$id= Input::get('id');
		$folderuserp= 'plantillas/users/'.Auth::user()->id;
		$usertemplate =  UserTemplate::find($id);
		//Actualizando Edit version
		$this->savetemplate($var, $folderuserp.'/edit/'.$usertemplate->editablehtml);
		//Actualizando Preview version
		$this->savetemplate($preview, $folderuserp.'/previews/'.$usertemplate->editablehtml);
		//$this->savetemplate($this->cabeceras.$preview.$this->footers, $folderuserp.'/downloads/'.$usertemplate->editablehtml);
		return View::make('usertemplates.preview')
            ->with('preview', $preview);
	}

	function destroy($id)
	{
		$usertemplate = UserTemplate::find($id);
		$folderusere= 'plantillas/users/'.Auth::user()->id.'/edit';
		$folderuserp= 'plantillas/users/'.Auth::user()->id.'/previews';
		$folderuserd= 'plantillas/users/'.Auth::user()->id.'/downloads';
		if (file_exists($folderusere."/".$usertemplate->editablehtml))
			unlink($folderusere."/".$usertemplate->editablehtml);
		if (file_exists($folderuserp."/".$usertemplate->finalhtml))
			unlink($folderuserp."/".$usertemplate->finalhtml);
		if (file_exists($folderuserd."/".$usertemplate->editablehtml))
			unlink($folderuserd."/".$usertemplate->editablehtml);
        $usertemplate->delete();
        return Redirect::route('mistemplates');
	}

	function viewlive($slug){
		$usertemplate = SeoTemplate::where('slug',$slug)->first()->usertemplate;
		if($usertemplate!=NULL)
		{
			$request = Request::instance();
			$request->setTrustedProxies(array('127.0.0.1')); // only trust proxy headers coming from the IP addresses on the array (change this to suit your needs)
			$ip = $request->getClientIp();

			$visit = new Visitor;
			$visit->ip = $ip;
			$visit->usertemplate_id = $usertemplate->id;
			$visit->dateview = date('Y-m-d');
			$visit->save();
		 	$folderusere= 'plantillas/users/'.$usertemplate->user_id.'/downloads';
		  	require_once ($folderusere.'/'.$usertemplate->editablehtml);
		}
		else
		{
		 	echo "La pagina que busca no ha sido encontrada";
		}
		
	}

}

