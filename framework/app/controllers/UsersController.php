<?php
use GenTemplate\Entities\User;
use GenTemplate\Managers\RegisterManager;
//use GenTemplate\Repositories\UserRepo;
use GenTemplate\Components\FieldBuilder;


class UsersController extends BaseController
{
	public function signUp()
	{	
		if(Auth::check())
		{
			return Redirect::route('home');
		}
		else
		{	
			$fieldBuilder = new FieldBuilder();
			return View::make('users/sign-up', compact('fieldBuilder'));
		}
		
	}


	public function logIn()
	{
		if(Auth::check())
		{
			return Redirect::route('home');
		}
		else
		{	
			$fieldBuilder = new FieldBuilder();
			return View::make('users/log-in', compact('fieldBuilder'));
		}
	}

	public function register(){
	
		$user = new User;	
		$manager = new RegisterManager($user, Input::all());
		
		if($manager->save())
		{
			return Redirect::route('home');
		}
		
		return Redirect::back()->withInput()->withErrors($manager->getErrors());
	}

	public function statusLogin(){
		if(!Auth::check())
		{
			return "noLogin";
		}
		elseif( (Auth::user()->rol!='admin') && (Auth::user()->rol!='superadmin') )
		{
			return 'noAdmin';
		}
	}

	public function lista()
	{	if(!Auth::check())
		{
			return View::make('home');
		}
		$usertemplates = User::find(Auth::user()->id)->userTemplates;
		return View::make('usertemplates.mistemplates')
            ->with('usertemplates', $usertemplates);
	}

	public function index(){
		if ($this->statusLogin()=='noAdmin' OR $this->statusLogin()== 'noLogin')
			return Redirect::route('home');
		$users= User::all();
		
        return View::make('users.index')
            ->with('users', $users);	
	}

	public function show($id){
		$user= User::find($id);

        return View::make('users.show')
            ->with('user', $user);	
	}

	public function asignAdmin($id)
	{
		$user = User::find($id);
		$user->rol='admin';
		$user->save();
		return Redirect::route('users.index');
	}

	public function desasignAdmin($id)
	{
		if(Auth::user()->rol!='superadmin')
		{
			return Redirect::route('users.index');	
		}
		$user = User::find($id);
		$user->rol='user';
		$user->save();
		return Redirect::route('users.index');
	}

	public function profile($username){
		$user = User::where('username',$username)->first();
		return View::make('users.profile')->with('userprofile', $user);
	}

}