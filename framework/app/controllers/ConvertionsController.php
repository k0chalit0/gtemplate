<?php
use GenTemplate\Entities\Convertion;
use GenTemplate\Entities\UserTemplate;

class ConvertionsController extends BaseController
{

	public function conversion(){
		$slug = Input::get('path');
		$slug = str_replace('/t/','', $slug);
		//$slug = "myfirstclon";
		$request = Request::instance();
		$request->setTrustedProxies(array('127.0.0.1')); // only trust proxy headers coming from the IP addresses on the array (change this to suit your needs)
		$ip = $request->getClientIp();
		$usertemplate = UserTemplate::where('slug',$slug)->first();
		$convertion = new Convertion;
		$convertion->usertemplate_id = $usertemplate->id;
		$convertion->ip = $ip;
		file_put_contents("./logs/archivo.log", $convertion, FILE_APPEND);
		$convertion->save();
	}

}	

