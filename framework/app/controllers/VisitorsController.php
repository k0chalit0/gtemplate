<?php
use GenTemplate\Entities\Visitor;
use GenTemplate\Entities\UserTemplate;
use GenTemplate\Entities\Convertion;

class VisitorsController extends BaseController
{
	public function getIndex(){
		return View::make('visitors.index');
	}

	public function distinctIp($usertemplate)
	{
		$ips = Visitor::select(DB::raw('ip, count(DISTINCT ip) as views'))
		  ->where('usertemplate_id', $usertemplate)
		  ->groupby('ip')
		  ->get();
		return $ips;
	}

	public function getData(){

		return Response::json(
			array(
					'id'=> 'ggg',
					'width'=> '100%',
					'height'=> 375,
					'xGrid'=> false,
					'legend'=> true,
					'title'=> 'Original vs Clon',
					'points'=> [
						[7, 26, 33, 74, 12, 49, 33, 33, 74, 12, 49, 33],
						[32, 46, 75, 38, 62, 20, 52, 75, 38, 62, 20, 52],
					],
					'pointRadius'=> 3,
					'colors'=> ['#1caf9a', 'black'],
					'xDist'=> 100,
					'dataNames'=> ['Original', 'Clon', 'Linux'],
					'xName'=> 'Day',
					'tooltipWidth'=> 15,
					'animations'=> true,
					'pointAnimation'=> true,
					'averagePointRadius'=> 5,
					'design'=> array(
						'tooltipColor'=> '#fff',
						'gridColor'=> '#f3f1f1',
						'tooltipBoxColor'=> '#d9534f',
						'averageLineColor'=> '#d9534f',
						'pointColor'=> '#d9534f',
						'lineStrokeColor'=> 'grey')
				));
	}

	public function getG($id){
		$usertemplate1 = UserTemplate::find($id);
		//$usertemplate2 = UserTemplate::where('padre',$id)->first();
		// $x = $usertemplate1->visitors()->select(DB::raw('count(distinct ip) as views'))->groupby('dateview')->get();
		// $y = $usertemplate2->visitors()->select(DB::raw('count(distinct ip) as views'))->groupby('dateview')->get();
		//echo $visits = $this->distinctIp($id1);
		$count1 = Visitor::where('usertemplate_id', $usertemplate1->id)->count();
		$count2 = Convertion::where('usertemplate_id', $usertemplate1->id)->count();

		$fecha = strtotime('-13 day' , strtotime(date('Y-m-d')));
		$contador=0;
		while(date('Y-m-d',$fecha) <= date('Y-m-d'))
		{
			$dato1 = $usertemplate1->visitors()->select(DB::raw('count(distinct ip) as views'))->where('dateview', date('Y-m-d',$fecha).' 00:00:00')->groupby('dateview')->get()->first()['views'];
			$x[] = ($dato1!=NULL)? intval($dato1) : 0;
			$dato2 = $usertemplate1->convertions()->select(DB::raw('count(distinct ip) as views'))->where('created_at', '>=' ,date('Y-m-d',$fecha).' 00:00:00')->where('created_at', '<=' ,date('Y-m-d',$fecha).' 23:59:59')->get()->first()['views'];
			$y[] = ($dato2!=NULL)? intval($dato2) : 0;
			$fechas[]= date('M, d',$fecha);
			$fecha = strtotime('+1 day' , strtotime(date('Y-m-d',$fecha)));
		}

		return Response::json(
			array(
					'id'=> 'ggg',
					'width'=> '100%',
					'height'=> 375,
					'xGrid'=> false,
					'legend'=> true,
					'title'=> 'Visitas vs Conversiones',
					'x' => $fechas,
					'points'=> [
						//[7,1,1,1,1,1,3,],
						//[1,0,0,0,0,0,3,],
						$x,
						$y,
					],
					'pointRadius'=> 3,
					'colors'=> ['#428bca', '#1caf9a'],
					'xDist'=> 100,
					'scale' => 1,
					'dataNames'=> [ 'Visitas', 'Conversiones' ],
					'xName'=> 'Day',
					'tooltipWidth'=> 15,
					'animations'=> true,
					'pointAnimation'=> true,
					'averagePointRadius'=> 5,
					'design'=> array(
						'tooltipColor'=> '#fff',
						'gridColor'=> '#f3f1f1',
						'tooltipBoxColor'=> '#d9534f',
						'averageLineColor'=> '#283593',
						'pointColor'=> '#283593',
						'lineStrokeColor'=> 'grey')
				));
	  }

	public function getMchart($id){
		$usertemplate = UserTemplate::find($id);
		$x = $usertemplate->visitors()->select(DB::raw('ip as label, count(id) as value'))->groupby('ip')->get();
		return $x;
	}

	public function getMchartarea($id){
		$usertemplate = UserTemplate::find($id);
		$usertemplateclon = UserTemplate::where('padre',$id)->first();
		$x = $usertemplate->visitors()->select(DB::raw('dateview as period, count(DISTINCT(ip)) as original'))->groupby('dateview')->groupby('dateview')->get();
		$y = $usertemplateclon->visitors()->select(DB::raw('dateview as period, count(DISTINCT(ip)) as clon'))->groupby('dateview')->groupby('dateview')->get();

		//echo $x;
		$x = $x->toArray();
		$y = $y->toArray();
		$arraymerge = array_merge($x, $y);

		foreach ($x as $k => $value) {
			foreach ($y as $k => $data) {
				if($value['period']==$data['period'])
				{
					$array[]= array_merge($data,$value);
				}
			}
		}

		foreach($array as $key=>$data){
			$array[$key]['period'] = str_replace(' 00:00:00', '', $data['period']);
		}

		return Response::json($array);
	}

	public function getMchartbar($id){
		$usertemplate = UserTemplate::find($id);
		$usertemplateclon = UserTemplate::where('padre',$id)->first();
		$x = $usertemplate->visitors()->select(DB::raw('count(DISTINCT(ip)) as visits '))->get()->first();
		$y = $usertemplateclon->visitors()->select(DB::raw('count(DISTINCT(ip)) as visits '))->get()->first();
		
		$x = $x->toArray();
		$x['slug'] = $usertemplate->slug;
		$y = $y->toArray();
		$y['slug']= $usertemplateclon->slug;
		$array = array_merge([$x], [$y]);

		return Response::json($array);
	}

	public function tablas($id){
		$usertemplate = UserTemplate::find($id);
		$usertemplateclon = UserTemplate::where('padre',$id)->first();
		$x = $usertemplate->visitors()->select(DB::raw('count(DISTINCT(ip)) as visits '))->get()->first();
		$y = $usertemplateclon->visitors()->select(DB::raw('count(DISTINCT(ip)) as visits '))->get()->first();
		$convertionsx = $usertemplate->convertions()->select(DB::raw('count(DISTINCT(ip)) as convertions '))->get()->first()->convertions;
		$convertionsy = $usertemplateclon->convertions()->select(DB::raw('count(DISTINCT(ip)) as convertions '))->get()->first()->convertions;
		
		$x = $x->toArray();
		$x['slug'] = $usertemplate->slug;
		$x['conversions'] = $convertionsx;
		$x['conversions_rate'] = round(($convertionsx/$x['visits'])*100,2);
		$y = $y->toArray();
		$y['slug']= $usertemplateclon->slug;
		$y['conversions'] = $convertionsy;
		$y['conversions_rate'] = round(($convertionsy/$y['visits'])*100,2);
		$t = array_merge([$x], [$y]);
		$tablas['tablas'] = $t;
		$tablas['user'] = $id;

		return View::make('visitors/tables')->with('tablas',$tablas);
	}

	public function estadisticas($id){
		$usertemplate = UserTemplate::find($id);
		$x = $usertemplate->visitors()->select(DB::raw('count(DISTINCT(ip)) as visits '))->get()->first();
		$convertionsx = $usertemplate->convertions()->select(DB::raw('count(DISTINCT(ip)) as convertions '))->get()->first()->convertions;
		
		$x = $x->toArray();
		$x['slug'] = $usertemplate->slug;
		$x['conversions'] = $convertionsx;
		$x['conversions_rate'] = round(($convertionsx/$x['visits'])*100,2);
		$x['id'] = $id;

		return View::make('visitors/estadisticas')->with('estadisticas',$x);
	}

	function graficas($id){
		$usertemplate = UserTemplate::find($id);
	  	return View::make('visitors/graficas')->with('usertemplate', $usertemplate);
	  }

	  function graficos($id){
		$usertemplate = UserTemplate::find($id);
	  	return View::make('visitors/graficos')->with('grafs', $usertemplate);
	  }

	function getSeo(){
		//$slugs = DB::table('seo_templates')->select('slug')->get();
		$slugs2 = DB::table('user_templates')->select('name as slug')->get();

		foreach ($slugs2 as $key => $value) {
			$slugs2[$key]->slug = str_replace(' ','-',trim($value->slug));
		}
		return Response::json($slugs2);
	}
}