<?php
use GenTemplate\Entities\SeoTemplate;
use GenTemplate\Components\FieldBuilder;
use GenTemplate\Entities\UserTemplate;
use GenTemplate\Managers\SaveSeoTemplateManager;

class SeoTemplatesController extends BaseController
{
	private $cabeceras = '<html class="no-js">
	  <head>
	    <meta charset="utf-8">
	    <meta name="language" content="es">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">';

	private $styles = '<link rel="stylesheet" href="https://fontastic.s3.amazonaws.com/mBcpKha37mryLvEf949bD9/icons.css">
	    <link rel="stylesheet" href="http://sumeclientes.net/bootstrap/css/bootstrap.min.css">
	    <link rel="stylesheet" href="http://sumeclientes.net/gentemplate/css/app.css">
		<link href="http://fonts.googleapis.com/css?family=Adamina" rel="stylesheet" type="text/css">
		<link href="http://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
		<link href="http://fonts.googleapis.com/css?family=Oswald" rel="stylesheet" type="text/css">
		<link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
		<link href="http://fonts.googleapis.com/css?family=Alice" rel="stylesheet" type="text/css">
		<link href="http://fonts.googleapis.com/css?family=OpenSans"rel="stylesheet" type="text/css">
		<link href="http://fonts.googleapis.com/css?family=Calligraffitti" rel="stylesheet" type="text/css">
	  	</head>
	  	<body>
		';

	private $footers = '
	    
	    <script src="http://sumeclientes.net/gentemplate/js/jquery.min.js"></script>
	    <script src="http://sumeclientes.net/gentemplate/js/jquery.countdown.min.js"></script>
	    <script src="http://sumeclientes.net/gentemplate/js/underscore-min.js"></script>
	    <script src="http://sumeclientes.net/gentemplate/js/main.js"></script>
	  </body>
	</html>';
	function publicar($id){
		$seotemplate = SeoTemplate::where('usertemplate_id',$id)->first();
		if($seotemplate!=NULL)
		{
			return View::make('seo.edit')
            ->with('seotemplate', $seotemplate);
		}
		else
		{
			$usertemplate= UserTemplate::find($id);
			return View::make('seo.create')->with('usertemplate',$usertemplate);
		}
	}

	public function store()
    {
        //
        $seotemplate = new SeoTemplate;
		$manager = new SaveSeoTemplateManager($seotemplate, Input::all());
		$usertemplate = UserTemplate::find(Input::get('usertemplate_id'));
		$folderuserp= 'plantillas/users/'.Auth::user()->id;
		//var_dump(Input::all());
		if($manager->save())
		{
			$title = '<title>'.Input::get('title').'</title>';
			$meta_description = '<meta name="description" content="'.Input::get('description').'">';
			$meta_keywords = '<meta name="keywords" content="'.Input::get('keywords').'">';
			ob_start(); # apertura de bufer
			include( $folderuserp.'/previews/'.$usertemplate->editablehtml);
			$preview = ob_get_contents();
			ob_end_clean(); # cierre de bufer
			
			$archivo = $this->cabeceras.$title.$meta_description.$meta_keywords.$this->styles.$preview.$this->footers;
			$this->savedownload($archivo, $folderuserp.'/downloads/'.$usertemplate->editablehtml);
			$usertemplate->slug =  Input::get('slug');
			$usertemplate->save();
			//echo "Lo logre aqui tambien";
			//return Redirect::route('seotemplates.publicar');
			return Redirect::route('mistemplates');
		}
		else
		{
			return Redirect::back()->withInput()->withErrors($manager->getErrors());
		}
		
    }

    public function index(){ }
    public function show(){ }

    public function update($id)
    {
        //
        $seotemplate = SeoTemplate::find($id);
		$manager = new SaveSeoTemplateManager($seotemplate, Input::all());
		$usertemplate = UserTemplate::find($seotemplate->usertemplate->id);
		$folderuserp= 'plantillas/users/'.Auth::user()->id;
		
		if($manager->save())
		{
			$title = '<title>'.Input::get('title').'</title>';
			$meta_description = '<meta name="description" content="'.Input::get('description').'">';
			$meta_keywords = '<meta name="keywords" content="'.Input::get('keywords').'">';
			ob_start(); # apertura de bufer
			include( $folderuserp.'/previews/'.$usertemplate->editablehtml );
			$preview = ob_get_contents();
			ob_end_clean(); # cierre de bufer
			$archivo = $this->cabeceras.$title.$meta_description.$meta_keywords.$this->styles.$preview.$this->footers;
			$this->savedownload($archivo, $folderuserp.'/downloads/'.$usertemplate->editablehtml);
			//echo "Lo logre";
			//return Redirect::route('seotemplates.publicar');
		}
		return Redirect::back()->withInput()->withErrors($manager->getErrors());
    }

    function savedownload($elemento, $folder){
		ob_start();
		echo $elemento;
		file_put_contents($folder, ob_get_contents());
		ob_end_clean();
		return true;
	}
}

