<?php
use GenTemplate\Entities\Variation;
use GenTemplate\Entities\UserTemplate;
use GenTemplate\Entities\User;

class VariationsController extends BaseController
{
	function variaciones(){
		
		$usertemplates = User::find(Auth::user()->id)->userTemplates;
		$templates = [];
		$variaciones = [];
		$vars = [];
		$count = 0;
		foreach($usertemplates as $ut)
		{
			$var = Variation::where('template1',$ut['id'])->orWhere('template2', $ut['id'])->get()->first();

			if(($var == NULL)||((isset($var)) && ($var->status=='disable')) ) {
				if($ut['slug']!='')
				{$templates[$count]['id'] = $ut['id'];
				$templates[$count]['name'] = $ut['name'];}
				$count++;
			}
			elseif(($var!=NULL)&&(!in_array($var['id'],$variaciones,true))) {
				$variaciones[] = $var['id'];
				$vars[] = $var;
			}
		}
		$result['templates'] = $templates;
		$result['vars'] = $vars;
		return View::make('variations/index')->with('result',$result);
	}

	public function guardar(){

		$variacion = new Variation;
		$variacion->template1 = Input::get('template')[0];
		$variacion->template2 = Input::get('template')[1];
		$variacion->counter = 0;
		$variacion->status = 'enable';
		$variacion->url = 'variacion_'.rand(1,10000);

		$variacion->save();

		return Redirect::to('/variaciones'); 
	}

	public function vervar($url){

		$variacion = Variation::where('url',$url)->first();
		$variacion->counter++;
		$variacion->save();
		if($variacion!=NULL)
		{
			$usertemplate1 = UserTemplate::find($variacion->template1);
			$usertemplate2 = UserTemplate::find($variacion->template2);
			if($variacion->counter%2==0)	
			return Redirect::to('/t/'.$usertemplate1->slug);
			else
			return Redirect::to('/t/'.$usertemplate2->slug);

		}
		else
		{
		 	echo "La pagina que busca no ha sido encontrada";
		}
	}

}	