<?php 
Use GenTemplate\Entities\Template;
use GenTemplate\Managers\SaveTemplateManager;

class TemplatesController extends BaseController{

	public function index()
    {
        if((Auth::user()->rol!='superadmin')&&(Auth::user()->rol!='admin'))
        {
            return Redirect::route('home');  
        }
        $templates = Template::all();

        return View::make('templates.index')
            ->with('templates', $templates);
    }

    public function create()
    {
        // load the create form (app/views/templates/create.blade.php)
        return View::make('templates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    private $folder = 'media/templates';
    private $foldergeneric = 'plantillas/generics';
    public function store()
    {
        //
        $template = new Template;
        if(Input::file('defaulthtml'))
		{
			$filet = Input::file('defaulthtml');
			$extensiont=$filet->getClientOriginalExtension();
			$namet = 'template_'.rand(1,1000000);
			$uploadt = $filet->move($this->foldergeneric.'/', $namet.'.'.$extensiont);
		}
        if(Input::file('image'))
		{
			$file = Input::file('image');
			$extension=$file->getClientOriginalExtension();
			$name = 'img_template_'.rand(1,1000000);
			$upload = $file->move($this->folder.'/', $name.'.'.$extension);
		}
		$setNameImage = array('image'=>$name.".".$extension ,'defaulthtml'=>$namet.".".$extensiont );
		$manager = new SaveTemplateManager($template, array_merge(Input::all(),$setNameImage));
		
		if($manager->save())
		{
			return Redirect::route('templates.index');
		}
		
		return Redirect::back()->withInput()->withErrors($manager->getErrors());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        // get the template
        if((Auth::user()->rol!='superadmin')&&(Auth::user()->rol!='admin'))
        {
            return Redirect::route('home');  
        }
        $template = Template::find($id);

        return View::make('templates.show')
            ->with('template', $template);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        // get the nerd
        $template = Template::find($id);

        // show the edit form and pass the nerd
        return View::make('templates.edit')
            ->with('template', $template);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if((Auth::user()->rol!='superadmin')&&(Auth::user()->rol!='admin'))
        {
            return Redirect::route('home');  
        }
        $template = Template::find($id);
        $folder = 'media/templates';
        $foldergeneric = 'plantillas/generics';
        if (file_exists($foldergeneric."/".$template->defaulthtml))
        unlink($foldergeneric."/".$template->defaulthtml);
        if (file_exists($folder."/".$template->image))
        unlink($folder."/".$template->image);
        $template->delete();
        return Redirect::route('templates.index');
    }
}
