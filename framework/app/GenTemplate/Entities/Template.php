<?php

namespace GenTemplate\Entities;

use Illuminate\Database\Eloquent\Model;

class Template extends \Eloquent 
{

    protected $table = 'templates';
 
	protected $fillable = ['name', 'type', 'defaulthtml', 'image'];
 
	protected $guarded = ['id'];
}