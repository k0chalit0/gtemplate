<?php

namespace GenTemplate\Entities;

use Illuminate\Database\Eloquent\Model;

class Convertion extends \Eloquent
{
    //
	protected $table = 'convertions';
	protected $guarded = ['id'];
	protected $contentedit;

	public function usertemplate(){
		return $this->hasMany('GenTemplate\Entities\UserTemplate','usertemplate_id');
	}
}
