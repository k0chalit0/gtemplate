<?php

namespace GenTemplate\Entities;

use Illuminate\Database\Eloquent\Model;

class Variation extends \Eloquent
{
    //
	protected $table = 'variations';
	protected $guarded = ['id'];
	protected $contentedit;
}