<?php

namespace GenTemplate\Entities;

use Illuminate\Database\Eloquent\Model;

class SeoTemplate extends \Eloquent
{
    //
	protected $table = 'seo_templates';
	protected $fillable = ['title', 'description', 'keywords','usertemplate_id','slug'];
 
	protected $guarded = ['id','slug'];
	protected $contentedit;

    public function usertemplate(){
    	return $this->belongsTo('GenTemplate\Entities\UserTemplate');
    }
}
