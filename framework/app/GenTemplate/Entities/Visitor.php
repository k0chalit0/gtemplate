<?php

namespace GenTemplate\Entities;

use Illuminate\Database\Eloquent\Model;

class Visitor extends \Eloquent
{
    //
	protected $table = 'visitors';
	protected $guarded = ['id'];
	protected $contentedit;

	public function usertemplate(){
		return $this->hasMany('GenTemplate\Entities\UserTemplate','usertemplate_id');
	}
}
