<?php
namespace GenTemplate\Entities;

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Database\Eloquent\Model;

class User extends \Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password','remember_token');

	protected $fillable = array('first_name', 'last_name','username', 'email', 'password');

	public function getAuthIdentifier(){
		return $this->getKey();
	}

	public function setPasswordAttribute($value){
		$this->attributes['password'] = \Hash::make($value);
	}

	public function userTemplates(){
		return $this->hasMany('GenTemplate\Entities\UserTemplate', 'user_id', 'id')->orderBy('name')->orderBy('id');
	}
}
