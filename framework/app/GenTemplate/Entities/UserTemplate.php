<?php

namespace GenTemplate\Entities;

use Illuminate\Database\Eloquent\Model;

class UserTemplate extends \Eloquent
{
    //
	protected $table = 'user_templates';
	protected $contentedit;

    public function user(){
    	return $this->belongsTo('GenTemplate\Entities\User','local_key');
    }

    public function usertemplate(){
    	return $this->hasOne('GenTemplate\Entities\SeoTemplate');
    }

    public function visitors(){
		return $this->hasMany('GenTemplate\Entities\Visitor','usertemplate_id');
	}

    public function convertions(){
        return $this->hasMany('GenTemplate\Entities\Convertion','usertemplate_id');
    }
}
