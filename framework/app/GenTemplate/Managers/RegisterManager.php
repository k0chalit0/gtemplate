<?php 

namespace GenTemplate\Managers;

class RegisterManager extends BaseManager
{
	public function getRules()
	{
		$rules = [
			'first_name' => 'required',
			'last_name'=> 'required',
			'username' => 'required|alpha_num|unique:users,username|not_in:templates,usertemplates,themes,preview,sign-up,log-in,log-out',
			'email' => 'required|email|unique:users,email',
			'password' => 'required|confirmed',
			'password_confirmation' => 'required'
		];
		return $rules;
	}

}