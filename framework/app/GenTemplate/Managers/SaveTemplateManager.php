<?php 

namespace GenTemplate\Managers;

class SaveTemplateManager extends BaseManager
{
	public function getRules()
	{
		$rules = [
			'name' => 'required',
			'type'=> 'required',
			'defaulthtml'=>'',
			'image' => 'required'
		];

		return $rules;
	}

}