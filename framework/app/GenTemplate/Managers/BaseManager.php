<?php

namespace GenTemplate\Managers;

abstract class BaseManager {

	protected $entity;
	protected $data;
	protected $errors;

	public function __construct($entity, $data)
	{
		$this->entity = $entity;
		$this->data = array_only($data, array_keys($this->getRules()));
	}

	abstract public function getRules();

	public function isValid()
	{	
		$rules = $this->getRules();
		$messages = array(
			'email.unique' => 'Este email ya ha sido registrado<a href="/log-in">&nbsp;Ingrese</a>',
			'username.unique' => 'Este username ya ha sido registrado',
			);
		$validation = \Validator::make($this->data, $rules, $messages);

		$isValid = $validation->passes();

		$this->errors = $validation->messages();

		return $isValid;
	}

	public function save()
	{
		if(! $this->isValid())
		{
			return false;
		}

		$this->entity->fill($this->data);
		$this->entity->save();

		return true;
	}

	public function getErrors()
	{
		return $this->errors;
	}
}