<?php 

namespace GenTemplate\Managers;

class SaveSeoTemplateManager extends BaseManager
{
	public function getRules()
	{
		$rules = [
			'title' => 'required|min:5|max:70',
			'description'=> 'required|min:10|max:160',
			'keywords' => '',
			'slug' => 'min:3|max:55|unique:seo_templates',
			'usertemplate_id' => 'required'
		];

		return $rules;
	}

}