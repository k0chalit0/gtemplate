<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/olakase', function()
{
	return View::make('tests.content');
});


Route::get('/', ['as' => 'home', 'uses' => 'UsersController@lista']);

Route::get('sign-up', ['as' => 'sign_up', 'uses'=> 'UsersController@signUp']);
Route::post('sign-up', ['as' => 'register', 'uses'=> 'UsersController@register']);

Route::get('log-in', ['as' => 'sign_in', 'uses'=> 'UsersController@logIn']);
Route::post('log-in', ['as' => 'login', 'uses'=> 'AuthController@login']);

Route::get('logout', ['as' => 'logout', 'uses'=> 'AuthController@logout']);
Route::resource('users', 'UsersController');
Route::get('users/asignAdmin/{id}', 'UsersController@asignAdmin');
Route::get('users/desasignAdmin/{id}', 'UsersController@desasignAdmin');

Route::resource('usertemplates', 'UserTemplatesController');
Route::resource('templates', 'TemplatesController');
Route::resource('seotemplates', 'SeoTemplatesController');
Route::get('usertemplates/show/{id}', 'UserTemplatesController@show');
Route::get('usertemplates/publicar/{id}', 'SeoTemplatesController@publicar');
//Vista
//Route::get('/t/{username}/{template}', 'UserTemplatesController@viewlive');
Route::get('/t/{slug}', 'UserTemplatesController@viewlive');

Route::get('usertemplates/creartemplate/{idtemplate}', 'UserTemplatesController@creartemplate');
Route::get('usertemplates/clonar/{id}', 'UserTemplatesController@clonar');
Route::post('usertemplates/preview', ['as' => 'templatepreview', 'uses'=> 'UserTemplatesController@preview']);

Route::get('mistemplates', ['as' => 'mistemplates', 'uses'=> 'UsersController@lista']);
Route::get('/tablas/{id}', 'VisitorsController@tablas');
Route::get('/estadisticas/{id}', 'VisitorsController@estadisticas');

//Visitors
Route::controller('/visitors', 'VisitorsController');

Route::get('graficas/{id}','VisitorsController@graficas');
Route::get('graficos/{id}','VisitorsController@graficos');

Route::get('/u/{username}', 'UsersController@profile');

//Convertions
//Route::controller('/convertions', 'ConvertionsController');
Route::post('/conversiones', 'ConvertionsController@conversion');
Route::get('variaciones', ['as' => 'variaciones', 'uses' => 'VariationsController@variaciones']);
Route::post('/variations/guardar', 'VariationsController@guardar');

Route::get('/v/{url}', 'VariationsController@vervar');


//Paginas del theme

Route::get('/theme/basic-tables', function()
{
	return View::make('theme.basic-tables');
});

Route::get('/theme/address-book', function()
{
	return View::make('theme.address-book');
});

Route::get('/theme/blank-page', function()
{
	return View::make('theme.blank-page');
});

Route::get('/theme/buttons', function()
{
	return View::make('theme.buttons');
});

Route::get('/theme/calendar', function()
{
	return View::make('theme.calendar')->with('page','calendario');
});

Route::get('/theme/compose-mail', function()
{
	return View::make('theme.compose-mail');
});

Route::get('/theme/data-tables', function()
{
	return View::make('theme.data-tables')->with('page','data-tables');
});

Route::get('/theme/file-upload', function()
{
	return View::make('theme.file-upload')->with('page','file-upload');
});

Route::get('/theme/float-chart', function()
{
	return View::make('theme.float-chart')->with('page','float-chart');
});

Route::get('/theme/form-elements', function()
{
	return View::make('theme.form-elements')->with('page','form-elements');
});

Route::get('/theme/form-validation', function()
{
	return View::make('theme.form-validation')->with('page','form-validation');
});

Route::get('/theme/gallery', function()
{
	return View::make('theme.gallery')->with('page','gallery');
});

Route::get('/theme/general', function()
{
	return View::make('theme.general')->with('page','general');
});

Route::get('/theme/graph', function()
{
	return View::make('theme.graph')->with('page','graph');
});

Route::get('/theme/grid', function()
{
	return View::make('theme.grid');
});

Route::get('/theme/icons', function()
{
	return View::make('theme.icons');
});

Route::get('/theme/inbox', function()
{
	return View::make('theme.inbox');
});

Route::get('/theme/index', function()
{
	return View::make('theme.index');
});

Route::get('/theme/invoice', function()
{
	return View::make('theme.invoice');
});

Route::get('/theme/lockscreen', function()
{
	return View::make('theme.lockscreen');
});

Route::get('/theme/login', function()
{
	return View::make('theme.login');
});

Route::get('/theme/morris-charts', function()
{
	return View::make('theme.morris-charts')->with('page','morris-charts');
});

Route::get('/theme/nestable', function()
{
	return View::make('theme.nestable')->with('page','nestable');
});

Route::get('/theme/portlets', function()
{
	return View::make('theme.portlets');
});

Route::get('/theme/profile', function()
{
	return View::make('theme.profile');
});

Route::get('/theme/read', function()
{
	return View::make('theme.read');
});

Route::get('/theme/register', function()
{
	return View::make('theme.register');
});

Route::get('/theme/slider', function()
{
	return View::make('theme.slider')->with('page','slider');
});

Route::get('/theme/tab-accordions', function()
{
	return View::make('theme.tab-accordions');
});

Route::get('/theme/timeline', function()
{
	return View::make('theme.timeline');
});

Route::get('/theme/treeview', function()
{
	return View::make('theme.treeview')->with('page','treeview');
});

Route::get('/theme/x-editable', function()
{
	return View::make('theme.x-editable')->with('page','x-editable');
});

