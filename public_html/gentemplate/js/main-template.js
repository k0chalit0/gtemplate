/**
 * Global variables
 *
 * @author Samuel Ed
 */
var App = {};
$(function() {
	App = {
		elemsEdit : '.edit-element',
		templateEditBox : null,
		
		init : function(){
			this.setEdit();
			this.setElMovable();			
		},
		setEdit : function(){
			var This = this;
			$('.editable').each(function() {
				if( $(this).hasClass('section') ){
					$(this).editTool("section");
				}
				if( $(this).hasClass('bgpage') ){
					$(this).editTool("bgpage");
				}
				if( $(this).hasClass('countdown') ){
					$(this).editTool('initCountdown');
					$(this).on('click',function(e){
						$(this).editTool('countdown');
					});
				}
				if( $(this).hasClass('img') ){
					$(this).on('click',function(e){
						e.preventDefault();
						$(this).editTool("image");
					});
				}
				if( $(this).hasClass('link') ){
					$(this).on('click',function(e){
						e.preventDefault();
						$(this).editTool("link");
					});
				}
				if( $(this).hasClass('btn') ){
					$(this).on('click',function(e){
						e.preventDefault();
						$(this).editTool("button");
					});
				}
				if( $(this).hasClass('bg') ){
					$(this).on('click',function(e){
						e.preventDefault();
						$(this).editTool("background");
					});
				}
				if( $(this).hasClass('txt') ){
					$(this).on('click',function(e){
						e.preventDefault();
						$(this).editTool("text");
					});
				}
				if( $(this).hasClass('video') ){
					$(this).on('click',function(e){
						e.preventDefault();
						$(this).editTool("video");
					});
				}
				/*if( $(this).hasClass('countdown') ){
					$(this).on('click',function(e){
						e.preventDefault();
						$(this).editTool("countdown");
					});
				}*/
			});
		},
		setElMovable : function (){
			$('#main').sortable({
				draggable: ".block-movable",
				handle: ".handle"
			});
			$('.el-movable').each(function() {
				var tmpl = _.template($('#tmp-edit-move').html());
				$(this)
					.css('position','relative')
					.prepend(tmpl);
				// This.setBackground();
			});
			$('.el-sortable').sortable({
				draggable: ".el-movable",
				handle: ".handle"
			});
		},
	};

	App.init();
});
