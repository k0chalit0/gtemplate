var App = {};
$(function() {
	var varCountdown = {
    labels   : ['days', 'hours', 'minutes', 'seconds'],
		nextYear : (new Date().getFullYear() + 1) + '-01-01',
		currDate : '00:00:00:00',
		nextDate : '00:00:00:00',
		example  : $('#gtime')
	};
  App = {
		init : function(){
			var date = $("#gtime").data('countdown');
			console.log(date);
			this.countView();
			this.initCountdown(date);
			
		},
		countView : function(){
			var This = this;
			$('.count-view').on('click', function(event) {
				event.preventDefault();
				This.postJSON(window.location.pathname);
			});
		},
		postJSON : function(path){
			$.ajax
	    ({
        type: "POST",
        //the url where you want to sent the userName and password to
        url: '/conversiones',
        dataType: 'json',
        
        //json object to sent to the authentication url
        data: {"path": path},
        success: function (data) {
        	console.log(data);
        	//alert("Thanks!"); 
        },
        error : function(xhr, status) {
		        //alert('Disculpe, existiÃ³ un problema');
		    },
		    // cÃ³digo a ejecutar sin importar si la peticiÃ³n fallÃ³ o no
		    complete : function(xhr, status) {
		        //alert('PeticiÃ³n realizada');
		    }
	    });
		},
		initCountdown : function(date){
			var This = this;
			console.log(date);
			date = (typeof date === 'undefined' || date === '') ? varCountdown.nextYear : date;
			
			$('#gtime').html("");
			this.drawCountdown();
			$('#gtime').countdown(date, function(event) {

				var newDate = event.strftime('%D:%H:%M:%S'),
					  data;
				if (newDate !== varCountdown.nextDate) {
					varCountdown.currDate = varCountdown.nextDate;
					varCountdown.nextDate = newDate;
					// Setup the data
					data = {
						'curr': This.strfobj(varCountdown.currDate),
						'next': This.strfobj(varCountdown.nextDate)
					};
					// Apply the new values to each node that changed
					This.diff(data.curr, data.next).forEach(function(label) {
						var selector = '.%s'.replace(/%s/, label),
								$node = varCountdown.example.find(selector);
						// Update the node
						$node.removeClass('flip');
						$node.find('.curr').text(data.curr[label]);
						$node.find('.next').text(data.next[label]);
						// Wait for a repaint to then flip
						_.delay(function($node) {
							$node.addClass('flip');
						}, 50, $node);
					});
				}
			});
		},
		drawCountdown : function(){
			var template = _.template($('#tmp-countdown').html()),
				  initData = this.strfobj(varCountdown.currDate);
			varCountdown.labels.forEach(function(label, i) {
				varCountdown.example.append(template({
					curr: initData[label],
					next: initData[label],
					label: label
				}));
			});
		},
		// Parse countdown string to an object
		strfobj : function(str) {
			var parser   = /([0-9]{2})/gi;
			var parsed = str.match(parser),
				obj = {};
			varCountdown.labels.forEach(function(label, i) {
				obj[label] = parsed[i]
			});
			return obj;
		},
		// Return the time components that diffs
		diff : function(obj1, obj2) {
			var diff = [];
			varCountdown.labels.forEach(function(key) {
				if (obj1[key] !== obj2[key]) {
					diff.push(key);
				}
			});
			return diff;
		},
	};
	App.init();
});

































