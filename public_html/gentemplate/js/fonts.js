var fontsGoogle = {
	Adamia : {
		tag  			 : "<link id='Adamia' href='http://fonts.googleapis.com/css?family=Adamina' rel='stylesheet' type='text/css'>",
		fontFamily : "'Adamina', serif"
	},
	/*Alice : {
		tag 			 : "<link id='Alice' href='http://fonts.googleapis.com/css?family=Alice' rel='stylesheet' type='text/css'>",
		fontFamily : "'Alice', serif"
	},
	Arvo : {
		tag  			 : "<link id='Arvo' href='http://fonts.googleapis.com/css?family=Arvo' rel='stylesheet' type='text/css'>",
		fontFamily : "'Arvo', serif"
	},
	Asap : {
		tag  			 : "<link id='Asap' href='http://fonts.googleapis.com/css?family=Asap' rel='stylesheet' type='text/css'>",
		fontFamily : "'Asap', serif"
	},
	AmaticSC : {
		tag  			 : "<link id='AmaticSC' href='http://fonts.googleapis.com/css?family=Amatic+SC' rel='stylesheet' type='text/css'>",
		fontFamily : "'Amatic SC', cursive"
	},
	Bitter : {
		tag  			 : "<link id='Bitter' href='http://fonts.googleapis.com/css?family=Bitter' rel='stylesheet' type='text/css'>",
		fontFamily : "'Cabin', sans-serif"
	},
	Cabin : {
		tag  			 : "<link id='Cabin' href='http://fonts.googleapis.com/css?family=Cabin' rel='stylesheet' type='text/css'>",
		fontFamily : "'Adamina', serif"
	},
	Calligraffitti : {
		tag  			 : "<link id='Calligraffitti' href='http://fonts.googleapis.com/css?family=Calligraffitti' rel='stylesheet' type='text/css'>",
		fontFamily : "'Calligraffitti', cursive"
	},*/
	DancingScript : {
		tag  			 : "<link id='DancingScript' href='http://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet' type='text/css'>",
		fontFamily : "'Dancing Script', cursive"
	},
	DroidSans : {
		tag  			 : "<link id='DroidSans' href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'>",
		fontFamily : "'Droid Sans', sans-serif"
	},
	/*DroidSerif : {
		tag  			 : "<link id='DroidSerif' href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>",
		fontFamily : "'Droid Serif', serif"
	},
	JosefinSlab : {
		tag  			 : "<link id='JosefinSlab' href='http://fonts.googleapis.com/css?family=Josefin+Slab' rel='stylesheet' type='text/css'>",
		fontFamily : "'Josefin Slab', serif"
	},*/
	JustAnotherHand : {
		tag  			 : "<link id='JustAnotherHand' href='http://fonts.googleapis.com/css?family=Just+Another+Hand' rel='stylesheet' type='text/css'>",
		fontFamily : "'Just Another Hand', cursive"
	},
	/*Lato : {
		tag  			 : "<link id='Lato' href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>",
		fontFamily : "'Lato', sans-serif"
	},
	Lobster : {
		tag  			 : "<link id='Lobster' href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>",
		fontFamily : "'Lobster', cursive"
	},*/
	LobsterTwo : {
		tag  			 : "<link id='LobsterTwo' href='http://fonts.googleapis.com/css?family=Lobster+Two' rel='stylesheet' type='text/css'>",
		fontFamily : "'Lobster Two', cursive"
	},
	Montserrat : {
		tag  			 : "<link id='Montserrat' href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>",
		fontFamily : "'Montserrat', sans-serif"
	},
	NixieOne : {
		tag  			 : "<link id='NixieOne' href='http://fonts.googleapis.com/css?family=Nixie+One' rel='stylesheet' type='text/css'>",
		fontFamily : "'NixieOne', cursive"
	},
	OpenSans : {
		tag  			 : "<link id='OpenSans' href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>",
		fontFamily : "'Open Sans', sans-serif"
	},
	Oswlad : {
		tag 			 : "<link id='Oswlad' href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>",
		fontFamily : "font-family: 'Oswald', sans-serif"
	},
	/*PTSans : {
		tag  			 : "<link id='PTSans' href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>",
		fontFamily : "'PT Sans', serif"
	},*/
	Pacifico : {
		tag  			 : "<link id='Pacifico' href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>",
		fontFamily : "'Pacifico', cursive"
	},
	/*Raleway : {
		tag  			 : "<link id='Raleway' href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>",
		fontFamily : "'Raleway', sans-serif"
	},
	Roboto : {
		tag  			 : "<link id='Roboto' href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>",
		fontFamily : "'Roboto', sans-serif"
	},*/
	ShadowsIntoLightTwo : {
		tag  			 : "<link id='ShadowsIntoLightTwo' href='http://fonts.googleapis.com/css?family=Shadows+Into+Light+Two' rel='stylesheet' type='text/css'>",
		fontFamily : "'Shadows Into Light Two', cursive"
	},
	/*SourceSansPro : {
		tag  			 : "<link id='SourceSansPro' href='http://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>",
		fontFamily : "'Source Sans Pro', sans-serif"
	},
	SixCaps : {
		tag  			 : "<link id='SixCaps' href='http://fonts.googleapis.com/css?family=Six+Caps' rel='stylesheet' type='text/css'>",
		fontFamily : "'Six Caps', sans-serif"
	},
	Syncopate : {
		tag  			 : "<link id='Syncopate' href='http://fonts.googleapis.com/css?family=Syncopate' rel='stylesheet' type='text/css'>",
		fontFamily : "'Syncopate', sans-serif"
	},*/
	TheGirlNextDoor : {
		tag  			 : "<link id='TheGirlNextDoor' href='http://fonts.googleapis.com/css?family=The+Girl+Next+Door' rel='stylesheet' type='text/css'>",
		fontFamily : "'The Girl Next Door', cursive"
	},
	/*TitilliumWeb : {
		tag  			 : "<link id='TitilliumWeb' href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>",
		fontFamily : "'Titillium Web', sans-serif"
	},
	Ubuntu : {
		tag  			 : "<link id='Ubuntu' href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>",
		fontFamily : "'Ubuntu', sans-serif"
	},
	Vollkorn : {
		tag  			 : "<link id='Vollkorn' href='http://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet' type='text/css'>",
		fontFamily : "'Vollkorn', serif"
	},
	YanoneKaffeesatz : {
		tag  			 : "<link id='Yanone Kaffeesatz' href='http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz' rel='stylesheet' type='text/css'>",
		fontFamily : "'Yanone Kaffeesatz', sans-serif"
	},
	GreatVibes : {
		tag  			 : "<link id='GreatVibes' href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>",
		fontFamily : "'Great Vibes', cursive"
	},*/
	BubblegumSans : {
		tag  			 : "<link id='BubblegumSans' href='http://fonts.googleapis.com/css?family=Bubblegum+Sans' rel='stylesheet' type='text/css'>",
		fontFamily : "'Bubblegum Sans', cursive"
	},
	LondrinaSolid : {
		tag  			 : "<link id='LondrinaSolid' href='http://fonts.googleapis.com/css?family=Londrina+Solid' rel='stylesheet' type='text/css'>",
		fontFamily : "'Londrina Solid', cursive"
	},
	/*Nunito : {
		tag  			 : "<link id='Nunito' href='http://fonts.googleapis.com/css?family=Nunito' rel='stylesheet' type='text/css'>",
		fontFamily : "'Nunito', sans-serif"
	},*/
/*}

var fonts ={*/
	Impact : {
		tag  			 : "",
		fontFamily : "Impact, Charcoal, sans-serif"
	},
	PalatinoLinotype : {
		tag  			 : "",
		fontFamily : "'Palatino Linotype', 'Book Antiqua', Palatino, serif"
	},
	Tahoma : {
		tag  			 : "",
		fontFamily : "Tahoma, Geneva, sans-serif"
	},
	CenturyGothic : {
		tag  			 : "",
		fontFamily : "Century Gothic, sans-serif"
	},
	LucidaSansUnicode : {
		tag  			 : "",
		fontFamily : "'Lucida Sans Unicode', 'Lucida Grande', sans-serif"
	},
	ArialBlack : {
		tag  			 : "",
		fontFamily : "'Arial Black', Gadget, sans-serif"
	},
	TimesNewRoman : {
		tag  			 : "",
		fontFamily : "'Times New Roman', Times, serif"
	},
	ArialNarrow : {
		tag  			 : "",
		fontFamily : "'Arial Narrow', sans-serif"
	},
	Verdana : {
		tag  			 : "",
		fontFamily : "Verdana, Geneva, sans-serif"
	},
	Copperplate : {
		tag  			 : "",
		fontFamily : "Copperplate / Copperplate Gothic Light, sans-serif"
	},
	LucidaConsole : {
		tag  			 : "",
		fontFamily : "'Lucida Console', Monaco, monospace"
	},
	GillSans : {
		tag  			 : "",
		fontFamily : "Gill Sans / Gill Sans MT, sans-serif"
	},
	TrebuchetMS : {
		tag  			 : "",
		fontFamily : "'Trebuchet MS', Helvetica, sans-serif"
	},
	CourierNew : {
		tag  			 : "",
		fontFamily : "'Courier New', Courier, monospace"
	},
	Arial : {
		tag  			 : "",
		fontFamily : "Arial, Helvetica, sans-serif"
	},
	Georgia : {
		tag  			 : "",
		fontFamily : "Georgia, Serif"
	},
	
};