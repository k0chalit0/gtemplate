/*global _:true*/
/*global console*/
/*global confirm*/
/*global fontsGoogle*/
/*global prompt*/
var defaults = {
	isPage : false,
	isSection : false,
	close : true,
	move  : false,
	remove : true,
	background : false,
	img : false,
	color : true,
	fontStyle : true,
	textAlign : true,
	fontSize  : false,
	fontFamily : false,
	edit 			: true,
	link 			: false,
	popup 		: false,
	countdown : false,

};
(function($) {
	var varCountdown = {
    labels   : ['days', 'hours', 'minutes', 'seconds'],
		nextYear : (new Date().getFullYear() + 1) + '-01-01',
		currDate : '00:00:00:00',
		nextDate : '00:00:00:00',
		example  : $('#gtime')
	};
	var methods = {
		init : function() {
		},
		setEditBox : function(){
			var tmpl = _.template($('#tmp-edit-tool').html());
			if(defaults.isSection || defaults.isPage){
				this.css('position','relative').prepend(tmpl);
				this.find('.edit-tool').addClass('edit-tool_section');
			}
			else{
				if (!$(this).parent('#element-selected').length) {
					methods.destroyBox.apply(this,[]);
					this
						.wrap('<div id="element-selected" class="element-selected"></div>')
						.after(tmpl);
				}
			}
			methods.setEventClose.apply(this,[]);
			methods.setColor.apply(this,[]);
			methods.setBold.apply(this,[]);
			methods.setItalic.apply(this,[]);
			methods.setUnderline.apply(this,[]);
			methods.setStrikethrough.apply(this,[]);
			methods.setFontFamily.apply(this,[]);
			methods.setFontSize.apply(this,[]);
			methods.setAlignLeft.apply(this,[]);
			methods.setAlignRight.apply(this,[]);
			methods.setAlignCenter.apply(this,[]);
			methods.setAlignJustify.apply(this,[]);
			methods.setLink.apply(this,[]);
			methods.setEditText.apply(this,[]);
			methods.setImage.apply(this,[]);
			methods.setBackgroundColor.apply(this,[]);
			methods.setBackgroundImg.apply(this,[]);
			methods.setCountdown.apply(this,[]);
			methods.remove.apply(this,[]);
		},
		//Methods Publics
		bgpage : function(){
			defaults = {
				isPage : true,
				isSection  : false,
				close 		 : false,
				move  		 : false,
				remove 		 : false,
				background : {
					backgroundColor : true,
					backgroundImage : true
				},
				img 			 : false,
				color 		 : false,
				fontStyle  : false,
				textAlign  : false,
				edit 			 : false,
				link 			 : false,
				popup 		 : false,
				countdown  : false,
			};
			methods.setEditBox.apply(this,[]);
		},
		section : function(){
			defaults = {
				isPage : false,
				isSection  : true,
				close 		 : false,
				move  		 : true,
				remove 		 : true,
				background : {
					backgroundColor : true,
					backgroundImage : true
				},
				img 			 : false,
				color 		 : false,
				fontStyle  : false,
				textAlign  : false,
				edit 			 : false,
				link 			 : false,
				popup 		 : false,
				countdown  : false,
			};
			methods.setEditBox.apply(this,[]);
		},
		countdown : function(){
			if( $(this).closest('#element-selected').length === 0 ){
				$('.section').find('.edit-tool_section').removeClass('hide');
				$(this).closest('.section').find('.edit-tool_section').addClass('hide');
				defaults = {
					isPage : false,
					isSection  : false,
					close 		 : true,
					move  		 : false,
					remove 		 : true,
					background : {
						backgroundColor : false,
						backgroundImage : false
					},
					img 			 : false,
					color 		 : false,
					fontStyle  : false,
					textAlign  : false,
					edit 			 : false,
					link 			 : false,
					popup 		 : false,
					countdown  : true
				};
				methods.setEditBox.apply(this,[]);
			}
		},
		text : function(){
			if( $(this).closest('#element-selected').length === 0 ){
				$('.section').find('.edit-tool_section').removeClass('hide');
				$(this).attr('contenteditable', 'true');
				$(this).closest('.section').find('.edit-tool_section').addClass('hide');
				$(this).closest('.el-movable').find('.edit-tool:first').addClass('hide');
				defaults = {
					isPage : false,
					isSection  : false,
					close 		 : true,
					move  		 : false,
					remove 		 : true,
					background : false,
					img 			 : false,
					color 		 : true,
					fontStyle  : true,
					fontSize   : true,
					fontFamily : true,
					textAlign  : true,
					edit 			 : true,
					link 			 : true,
					popup 		 : false,
					countdown  : false,
				};
				methods.setEditBox.apply(this,[]);
			}
		},
		image : function(){
			$('.section').find('.edit-tool_section').removeClass('hide');
			$(this).closest('.section').find('.edit-tool_section').addClass('hide');
			defaults = {
				isPage : false,
				isSection  : false,
				close 		 : true,
				move  		 : false,
				remove 		 : true,
				background : false,
				img 			 : true,
				color 		 : false,
				fontStyle  : false,
				textAlign  : false,
				edit 			 : false,
				link 			 : false,
				popup 		 : false,
				countdown  : false,
			};
			methods.setEditBox.apply(this,[]);
		},
		video : function(){
			var tmpl = _.template($('#tmp-edit-video').html());
			methods.destroyBox.apply(this,[]);
			this
				.wrap('<div id="element-selected" class="element-selected"></div>')
				.after(tmpl);
				console.log("video");
			methods.changeVideo.apply(this,[]);
		},
		link : function(){
			var tmpl = _.template($('#tmp-edit-link').html());
			methods.destroyBox.apply(this,[]);
			this
				.wrap('<div id="element-selected" class="element-selected"></div>')
				.after(tmpl);
			methods.setLink.apply(this,[]);
		},
		button : function(){
			console.log($(this).parent('#element-selected'));
			// if (!$(this).parent('#element-selected')) {
				$('.section').find('.edit-tool_section').removeClass('hide');
				// $(this).attr('id','elemSelected');
				$(this).attr('contenteditable', 'true');
				$(this).closest('.section').find('.edit-tool_section').addClass('hide');
				defaults = {
					isPage : false,
					isSection  : false,
					close 		 : true,
					move  		 : false,
					remove 		 : true,
					background : {
						backgroundColor : true,
						backgroundImage : false
					},
					img 			 : false,
					color 		 : true,
					fontStyle  : true,
					textAlign  : false,
					edit 			 : true,
					link 			 : true,
					popup 		 : true,
					countdown : false,
				};
				methods.setEditBox.apply(this,[]);
			// }
		},
		background : function(){
			$('.section').find('.edit-tool_section').removeClass('hide');
			$(this).closest('.section').find('.edit-tool_section').addClass('hide');
			defaults = {
				isPage : false,
				isSection  : false,
				close 		 : true,
				move  		 : false,
				remove 		 : true,
				background : {
					backgroundColor : true,
					backgroundImage : true
				},
				img 			 : false,
				color 		 : false,
				fontStyle  : false,
				textAlign  : false,
				edit 			 : false,
				link 			 : false,
				popup 		 : false,
				countdown : false,
			};
			methods.setEditBox.apply(this,[]);
		},
		backgroundColor : function(){
			defaults = {
				isPage : false,
				isSection  : false,
				close 		 : true,
				move  		 : false,
				remove 		 : true,
				background : {
					backgroundColor : true,
					backgroundImage : false
				},
				img 			 : true,
				color 		 : false,
				fontStyle  : false,
				textAlign  : false,
				edit 			 : false,
				link 			 : false,
				popup 		 : false,
				countdown  : false,
			};
			methods.setEditBox.apply(this,[]);
		},
		backgroundImage : function(){
			defaults = {
				isPage : false,
				isSection  : false,
				close 		 : true,
				move  		 : false,
				remove 		 : true,
				background : {
					backgroundColor : false,
					backgroundImage : true
				},
				img 			 : true,
				color 		 : false,
				fontStyle  : false,
				textAlign  : false,
				edit 			 : false,
				link 			 : false,
				popup 		 : false,
				countdown  : false,
			};
			methods.setEditBox.apply(this,[]);
		},
		destroyBg : function(){
			$('#element-selected').find('.edit-tool').remove();
			$('#element-selected')
				.css('position','')
				.attr('id', '');
		},
		destroyBox : function(){
			console.log('destroybox');
			console.log($('#element-selected'));
			$('#element-selected').find('.edit-tool').remove();
			$('#element-selected').find('.editable').unwrap();
		},
		// Methods Privates
		remove : function(){
			var This = this,
					parent = (defaults.isSection) ? this : '#element-selected';

			$(parent).find('.btn-remove').on('click', function(e) {
				e.preventDefault();
				console.log(This);
				var r = confirm("¿Esta seguro que desea eliminar este elemento?");
				var sectionParent = $(This).closest('.section');
				
				if (r === true) {
				  methods.destroyBox.apply(this,[]);
					This.remove();
					if(sectionParent.find('.editable').size() === 0){
						sectionParent.remove();
					}
				}
			});
		},
		// Countdown
		initCountdown : function(date){
			console.log(date);
			date = (typeof date === 'undefined' || date === '') ? varCountdown.nextYear : date;
			console.log(date);
			$('#gtime').html("");
			methods.drawCountdown();
			$('#gtime').countdown(date, function(event) {

				var newDate = event.strftime('%D:%H:%M:%S'),
					  data;
				if (newDate !== varCountdown.nextDate) {
					varCountdown.currDate = varCountdown.nextDate;
					varCountdown.nextDate = newDate;
					// Setup the data
					data = {
						'curr': methods.strfobj(varCountdown.currDate),
						'next': methods.strfobj(varCountdown.nextDate)
					};
					// Apply the new values to each node that changed
					methods.diff(data.curr, data.next).forEach(function(label) {
						var selector = '.%s'.replace(/%s/, label),
								$node = varCountdown.example.find(selector);
						// Update the node
						$node.removeClass('flip');
						$node.find('.curr').text(data.curr[label]);
						$node.find('.next').text(data.next[label]);
						// Wait for a repaint to then flip
						_.delay(function($node) {
							$node.addClass('flip');
						}, 50, $node);
					});
				}
			});
		},
		drawCountdown : function(){
			var template = _.template($('#tmp-countdown').html()),
				  initData = methods.strfobj(varCountdown.currDate);
			varCountdown.labels.forEach(function(label) {
				varCountdown.example.append(template({
					curr: initData[label],
					next: initData[label],
					label: label
				}));
			});
		},
		setCountdown : function(){
			$('#element-selected').find('.btn-countdown').on('click', function(event) {
				event.preventDefault();
				console.log("lcik");
				$('.edit-tool_sub-countdown').toggleClass('hide');
			});
			$('.edit-tool_sub-countdown').find('.edit-tool_btn-ok').on('click', function(event) {
				event.preventDefault();
				var newDate = $('.edit-tool_sub-countdown').find('.edit-tool_input').val();
				console.log(newDate);
				if (newDate === '') {
					alert("Ingrese una fecha!");
				}
				$('#gtime').attr('data-countdown',newDate);
				methods.initCountdown(newDate);
			});
		},
		// Parse countdown string to an object
		strfobj : function(str) {
			var parser   = /([0-9]{2})/gi;
			var parsed = str.match(parser),
				  obj    = {};
			varCountdown.labels.forEach(function(label, i) {
				obj[label] = parsed[i];
			});
			return obj;
		},
		// Return the time components that diffs
		diff : function(obj1, obj2) {
			var diff = [];
			varCountdown.labels.forEach(function(key) {
				if (obj1[key] !== obj2[key]) {
					diff.push(key);
				}
			});
			return diff;
		},
		setFontFamily : function(){
			var This = this;
			// var subFf = $('#element-selected').find('.edit-tool_sub-ff');
			if (defaults.fontFamily) {
				$.each(fontsGoogle, function(index){
					$('#fontFamily').append('<option value="'+index+'">'+index+'</option>');
				});
				$('#fontFamily').change(function() {
					if ( $('#'+$(this).val()).length <= 0 ) {
						$('head').append(fontsGoogle[$(this).val().toString()]['tag']);
					}
					$(This).css('font-family', fontsGoogle[$(this).val().toString()]['fontFamily']);
				});
			}
		},
		setFontSize : function(){
			var This = this;
			$('.edit-tool').find('.edit-tool_input-fz').on('input', function(event) {
				event.preventDefault();
				console.log(This);
				This.css('font-size', $(this).val()+'px');
				//.css('z-index','900');
			});
		},
		setColor : function(){
			$('#element-selected').find('.edit-tool_color').colorpicker({
				align : 'left',
				component : '.edit-tool_btn-color',
			}).on('changeColor', function(ev) {
				document.execCommand('foreColor',false, ev.color.toHex());
			});
		},
		setBackgroundColor : function(){
			var This = this,
					parent = (defaults.isSection || defaults.isPage) ? this : '#element-selected';
			$(parent).find('.edit-tool_bgcolor').colorpicker({
				color : $(This).css("background-color"),
				align : 'left',
				component : '.edit-tool_btn-bgc',
			}).on('changeColor', function(ev) {
				This.css('background-color', ev.color.toHex());
			});
		},
		setBackgroundImg : function(){
			var This = this,
					parent = (defaults.isSection || defaults.isPage) ? this : '#element-selected';
			$(parent).find('.edit-tool_btn-bgi:first').on('click', function(e) {
				e.preventDefault();
				console.log("bgi");
				$(parent).find('.edit-tool_file-bg:first').click().change(function(){
					console.log("click change bg");
					if (this.files && this.files[0]) {
						var reader = new FileReader();
						reader.onload = function (e) {
							$(This).css('background-image', 'url('+e.target.result +')');	
						};
						reader.readAsDataURL(this.files[0]);
					}
				});
			});
			$(parent).find('.edit-tool_btn-bgirmv').on('click', function(e) {
				e.preventDefault();
				console.log($(This).css('background-image'));
				if ($(This).css('background-image') !== 'none') {
					console.log("tiene");
					$(This).css('background-image','');
				}
			});

		},
		setImage : function(){
			$('#element-selected').find('.edit-tool_btn-img').on('click', function(e) {
				e.preventDefault();
				$('#element-selected').find('.edit-tool_file-img').click().change(function(){
						if (this.files && this.files[0]) {
							var reader = new FileReader();
							reader.onload = function (e) {
								$('#element-selected').find('img').attr('src', e.target.result);
							};
							reader.readAsDataURL(this.files[0]);
						}
				});
			});
		},
		setBackgroundButton : function(){
			var This = this;
			$('.btn-bg').colorpicker({
				align : 'left'
			}).on('changeColor', function(ev) {
				This.css({
					'background-color' 	: ev.color.toHex(),
					'border-color' 			: ev.color.toHex()
				});
				if(This.has('a').length > 0){
					This.find('a').css('background-color', ev.color.toHex());
				}
			});
		},
		setEditText : function(){
			var This = this;
			$('.edit-tool').find('.btn-edit').on('click', function(event) {
				event.preventDefault();
				This.attr('contenteditable', 'true').focus();
				//.css('z-index','900');
			});
		},
		destroyEditText : function(){
			this.attr('contenteditable', 'false').focus().attr('style','');
		},
		setLink : function(){
			// falta validar
			$('.btn-link').on('click', function(e) {
				e.preventDefault();
				var valueURL = prompt("Ingrese una URL", "");
				if (valueURL !== null) {
					document.execCommand('CreateLink',false,valueURL);
				}
				methods.destroyBox.apply(this,[]);
			});
		},
		setBold : function(){
			$('.edit-tool').find('.btn-bold').on('click', function(e) {
				e.preventDefault();
				document.execCommand('bold',false,true);
			});
		},
		
		setItalic : function(){
			$('.edit-tool').find('.btn-italic').on('click', function(e) {
				e.preventDefault();
				document.execCommand('italic',false,true);
			});
		},
		setUnderline : function(){
			$('.edit-tool').find('.btn-underline').on('click', function(e) {
				e.preventDefault();
				document.execCommand('underline',false,true);
			});
		},
		setStrikethrough : function(){
			$('.edit-tool').find('.btn-strikethrough').on('click', function(e) {
				e.preventDefault();
				document.execCommand('strikethrough',false,true);
			});
		},
		setAlignLeft : function(){
			$('.edit-tool').find('.btn-align-left').on('click', function(e) {
				e.preventDefault();
				document.execCommand('justifyLeft',false,true);
			});
		},
		setAlignRight : function(){
			$('.edit-tool').find('.btn-align-right').on('click', function(e) {
				e.preventDefault();
				console.log("right");
				document.execCommand('justifyRight',false,true);
			});
		},
		setAlignCenter : function(){
			$('.edit-tool').find('.btn-align-center').on('click', function(e) {
				e.preventDefault();
				document.execCommand('JustifyCenter',false,true);
			});
		},
		setAlignJustify : function(){
			$('.edit-tool').find('.btn-align-justify').on('click', function(e) {
				e.preventDefault();
				document.execCommand('justifyFull',false,true);
			});
		},
		setEventClose : function(){
			$('.edit-tool').find('.btn-close').on('click', function(e) {
				e.preventDefault();
				$('.section').find('.edit-tool_section').removeClass('hide');
				$('.el-movable').find('.edit-tool:first').removeClass('hide');
				methods.destroyBox.apply(this,[]);
			});
		},
		changeVideo : function() {
			var This = this;
			$('.video-box').find('.btn-ok').on('click', function(e) {
				e.preventDefault();
				var value = $('.video-box').find('.field').val(),
						autoplay = ($('#vautoplay').prop("checked")) ? 1 : 0,
						controls = ($('#vcontrols').prop("checked")) ? 1 : 0;

				This.html("").append(methods.getContentVideo(value,autoplay,controls));

				methods.destroyBox.apply(this,[]);
			});

			$('.url-box').find('.btn-cancel').on('click', function(e) {
				e.preventDefault();
				console.log("cancel");
				$('.edit-buttons, .url-box').toggleClass('hide');
			});
		},

		checkUrl : function(test_url) {
			var testLoc = document.createElement('a');
					testLoc.href = test_url.toLowerCase();
			var url = testLoc.hostname;
			var what;
			if (url.indexOf('youtube.com') !== -1) {
					what='youtube';
			}else if (url.indexOf('vimeo.com') !== -1) {
					what='vimeo';
			}else if (url.indexOf('amazonaws.com') !== -1) {
					what='amazon';
			}else{
					what='None';
			}
			return what;
		},
		getContentVideo : function(url, autoplay, controls){
			var contentVideo, idVideo, newURL;

			switch (methods.checkUrl(url)) {
				case "vimeo":
					var regExp = /^.*(vimeo\.com\/)((channels\/[A-z]+\/)|(groups\/[A-z]+\/videos\/))?([0-9]+)/;
					var parseUrl = regExp.exec(url);
					idVideo = parseUrl[5];
					newURL = 'http://player.vimeo.com/video/'+ idVideo +'?autoplay='+ autoplay;
					contentVideo = '<iframe src="'+ newURL +'" width="500" height="281" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
					break;
				case "youtube":
					idVideo = url.split('v=')[1];
					var ampersandPosition = idVideo.indexOf('&');
					if(ampersandPosition !== -1) {
						idVideo = idVideo.substring(0, ampersandPosition);
					}
					newURL = 'http://www.youtube.com/embed/'+ idVideo +'?autoplay='+ autoplay +'&controls='+ controls +'&showinfo=0'+'&rel=0';
					contentVideo = '<iframe src="'+ newURL +'" frameborder="0" allowfullscreen width="560" height="349"></iframe>';
					break;
				case "amazon":
					autoplay = (autoplay === 1) ? 'autoplay' : '';
					controls = (controls === 1) ? 'controls' : '';
					contentVideo = '<video '+ autoplay +' '+ controls +' src="'+ url +'">Tu navegador no soporta HTML5 </video>';
					break;
				default:
					contentVideo = '<p><font color="#FFFFFF">Ingrese un video de YouTube, Video o Amazon Web Sevices</font></p>';
			}

			return contentVideo;
		}

		/*rgbtohex : function(colorval) {
			var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
			delete(parts[0]);
			for (var i = 1; i <= 3; ++i) {
					parts[i] = parseInt(parts[i]).toString(16);
					if (parts[i].length == 1) parts[i] = '0' + parts[i];
			}
			color = '#' + parts.join('');
			return color;
		}*/
	};

	$.fn.editTool = function( method, options ){
		defaults = $.extend(defaults, options);
		if ( methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			//Si no se pasa ningún parámetro o el parámetro es 
			//un objeto de configuración llamamos al inicializador	
			return methods.init.apply( this, arguments );
		} else {
			//En el resto de los casos mostramos un error
			$.error( 'La función ' +  method + ' no existe en jQuery.editTool' );
		}	
	};
})(jQuery);