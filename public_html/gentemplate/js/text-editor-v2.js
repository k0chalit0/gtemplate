/*global _:true*/
var defaults = {
		close : true,
		move  : false,
		delete : true,
		background : false;
		img : false,
		color : true,

	};
(function($) {
	/*var defaults = {
		fonts : null,
		img   : true
	};*/

	var methods = {
		init : function() {
			methods.text.apply(this,[]);
		},
		text : function(){
			var tmpl = _.template($('#tmp-edit-text').html());

			methods.destroyBox.apply(this,[]);
			this
				.wrap('<div id="element-selected" class="element-selected"></div>')
				.after(tmpl);
			methods.setEventClose.apply(this,[]);
			methods.setBold.apply(this,[]);
			methods.setItalic.apply(this,[]);
			methods.setUnderline.apply(this,[]);
			methods.setStrikethrough.apply(this,[]);
			methods.setAlignLeft.apply(this,[]);
			methods.setAlignRight.apply(this,[]);
			methods.setAlignCenter.apply(this,[]);
			methods.setAlignJustify.apply(this,[]);
			methods.setColor.apply(this,[]);
			methods.setLink.apply(this,[]);
			methods.setEditText.apply(this,[]);
			methods.setFont.apply(this,[]);
			methods.remove.apply(this,[]);
		},
		//Methods Publics
		image : function(){
			var tmpl = _.template($('#tmp-edit-image').html());
			methods.destroyBox.apply(this,[]);
			this
				.wrap('<div id="element-selected" class="element-selected"></div>')
				.after(tmpl);
			methods.setEventImage.apply(this,[]);
			methods.remove.apply(this,[]);
		},
		video : function(){
			var tmpl = _.template($('#tmp-edit-video').html());
			methods.destroyBox.apply(this,[]);
			this
				.wrap('<div id="element-selected" class="element-selected"></div>')
				.after(tmpl);
			methods.changeVideo.apply(this,[]);
		},
		link : function(){
			var tmpl = _.template($('#tmp-edit-link').html());
			methods.destroyBox.apply(this,[]);
			this
				.wrap('<div id="element-selected" class="element-selected"></div>')
				.after(tmpl);
			methods.setLink.apply(this,[]);
		},
		button : function(){
			var tmpl = _.template($('#tmp-edit-button').html());
			methods.destroyBox.apply(this,[]);
			this
				.wrap('<div id="element-selected" class="element-selected"></div>')
				.after(tmpl);
			methods.setEventClose.apply(this,[]);				
			methods.setLink.apply(this,[]);
			methods.setBackgroundButton.apply(this,[]);
			methods.setColor.apply(this,[]);
			methods.setBold.apply(this,[]);
			methods.setItalic.apply(this,[]);
			methods.setUnderline.apply(this,[]);
			methods.setEditText.apply(this,[]);
			methods.remove.apply(this,[]);
		},
		background : function(){
			// defaults.img = false;
			var tmpl = _.template($('#tmp-edit-bgc').html());
			console.log(this);
			methods.destroyBg.apply(this,[]);
			this
				.attr('id', 'element-selected')
				.css('position','relative')
				.prepend(tmpl);
			methods.setBackground.apply(this,[]);
		},
		destroyBg : function(){
			$('#element-selected').find('.edit-tool').remove();
			$('#element-selected')
				.css('position','')
				.attr('id', '');
		},
		destroyBox : function(){
			$('#element-selected').find('.edit-tool').remove();
			$('#element-selected').find('.editable').unwrap();
		},
		// Methods Privates
		remove : function(){
			var This = this;
			$('.edit-tool').find('.btn-remove').on('click', function(e) {
				e.preventDefault();
				console.log("remove");
				methods.destroyBox.apply(this,[]);
				This.remove();
			});
		},
		setFont : function(){
			var tmpl = _.template($('#tmp-edit-font-item').html());
			tmpl({name:'ola k ase'});
			// console.log(tmpl);
			$('.menu-fonts').prepend(tmpl);

			/*$.each(fontsGoogle, function(index, value){
				tmpl({nameFont:index});
				$('.menu-fonts').prepend(tmpl.html());
			});*/

			/*var This = this;
			$('.menu-fonts').find('.menu-fonts_link').on('click', function(e) {
				e.preventDefault();
				
			});*/
		},
		setColor : function(){
			var This = this;
			$('.btn-color').colorpicker({
				align : 'left'
			}).on('changeColor', function(ev) {
				This.css('color', ev.color.toHex());
				if(This.has('a').length > 0){
					This.find('a').css('color', ev.color.toHex());
				}
			});
		},
		setBackground : function(){
			var This = this;
			$('.btn-bg').colorpicker({
				align : 'left'
			}).on('changeColor', function(ev) {
				This.css('background-color', ev.color.toHex());
				if(This.has('a').length > 0){
					This.find('a').css('background-color', ev.color.toHex());
				}
			});
		},
		setBackgroundButton : function(){
			var This = this;
			$('.btn-bg').colorpicker({
				align : 'left'
			}).on('changeColor', function(ev) {
				This.css({
					'background-color' 	: ev.color.toHex(),
					'border-color' 			: ev.color.toHex()
				});
				if(This.has('a').length > 0){
					This.find('a').css('background-color', ev.color.toHex());
				}
			});
		},
		setEditText : function(){
			var This = this;
			$('.edit-tool').find('.btn-edit').on('click', function(event) {
				event.preventDefault();
				This.attr('contenteditable', 'true').focus();
				//.css('z-index','900');
			});
		},
		destroyEditText : function(){
			this.attr('contenteditable', 'false').focus().attr('style','');
		},
		setLink : function(){
			var This = this;
			$('.edit-tool').find('.btn-link').on('click', function(e) {
				e.preventDefault();
				$('.edit-buttons, .url-box').toggleClass('hide');
			});
			$('.url-box').find('.btn-ok').on('click', function(e) {
				e.preventDefault();
				var value = $('.url-box').find('.field').val(),
						tagA  = "<a href='"+value+"'></a>";
				if($('.url-box').find('.field-check').prop("checked") === true){
					tagA = "<a href='"+value+"' target='_blank'></a>";
				}
				if(This.has('a').length > 0){
					This.find('a').contents().unwrap();
				}
				if(This.is('a')){
					This.attr('href',value);
				}else{
					This.wrapInner(tagA);
					$('.edit-buttons, .url-box').toggleClass('hide');
				}
				methods.destroyBox.apply(this,[]);
			});
			$('.url-box').find('.btn-cancel').on('click', function(e) {
				e.preventDefault();
				$('.edit-buttons, .url-box').toggleClass('hide');
			});
		},
		setBold : function(){
			var This = this;
			$('.edit-tool').find('.btn-bold').on('click', function(e) {
				e.preventDefault();
				if(This.has('b').length > 0){
					This.find('b').contents().unwrap();
				}else{
					This.wrapInner("<b></b>");	
				}
			});
		},
		setItalic : function(){
			var This = this;
			$('.edit-tool').find('.btn-italic').on('click', function(e) {
				e.preventDefault();
				if(This.has('i').length > 0){
					This.find('i').contents().unwrap();
				}else{
					This.wrapInner("<i></i>");	
				}
			});
		},
		setUnderline : function(){
			var This = this;
			$('.edit-tool').find('.btn-underline').on('click', function(e) {
				e.preventDefault();
				if(This.has('u').length > 0){
					This.find('u').contents().unwrap();
				}else{
					This.wrapInner("<u></u>");	
				}
			});
		},
		setStrikethrough : function(){
			var This = this;
			$('.edit-tool').find('.btn-strikethrough').on('click', function(e) {
				e.preventDefault();
				if(This.has('s').length > 0){
					This.find('s').contents().unwrap();
				}else{
					This.wrapInner("<s></s>");	
				}
			});
		},
		setAlignLeft : function(){
			var This = this;
			$('.edit-tool').find('.btn-align-left').on('click', function(e) {
				e.preventDefault();
				if(This.css('text-align') !== "left"){
					This.css('text-align','left');
				}else{
					This.css('text-align','');	
				}
			});
		},
		setAlignRight : function(){
			var This = this;
			$('.edit-tool').find('.btn-align-right').on('click', function(e) {
				e.preventDefault();
				if(This.css('text-align') !== "right"){
					This.css('text-align','right');
				}else{
					This.css('text-align','');	
				}
			});
		},
		setAlignCenter : function(){
			var This = this;
			$('.edit-tool').find('.btn-align-center').on('click', function(e) {
				e.preventDefault();
				if(This.css('text-align') !== "center"){
					This.css('text-align','center');
				}else{
					This.css('text-align','');	
				}
			});
		},
		setAlignJustify : function(){
			var This = this;
			$('.edit-tool').find('.btn-align-justify').on('click', function(e) {
				e.preventDefault();
				if(This.css('text-align') !== "justify"){
					This.css('text-align','justify');
				}else{
					This.css('text-align','');	
				}
			});
		},
		setEventClose : function(){
			$('.edit-tool').find('.btn-close').on('click', function(e) {
				e.preventDefault();
				methods.destroyBox.apply(this,[]);
			});
		},
		setEventImage : function(){
			$('.edit-tool').find('.btn-img').on('click', function(event) {
				event.preventDefault();
				$('#img_input').click().change(function(){
						if (this.files && this.files[0]) {
							var reader = new FileReader();
							reader.onload = function (e) {
								$('#element-selected').find('img').attr('src', e.target.result);
							};
							reader.readAsDataURL(this.files[0]);
						}
				});
			});
		},
		changeVideo : function() {
			var This = this;
			$('.video-box').find('.btn-ok').on('click', function(e) {
				e.preventDefault();
				var value = $('.video-box').find('.field').val();
				var video_id = value.split('v=')[1];
				var ampersandPosition = video_id.indexOf('&');
				if(ampersandPosition !== -1) {
				  video_id = video_id.substring(0, ampersandPosition);
				}
				var url = 'http://www.youtube.com/embed/'+ video_id +'?rel=0&amp;hd=1';
				This.find('iframe').remove();
				This.append('<iframe src="'+url+'" frameborder="0" allowfullscreen width="560" height="349"></iframe>');
				methods.destroyBox.apply(this,[]);
			});
			$('.url-box').find('.btn-cancel').on('click', function(e) {
				e.preventDefault();
				$('.edit-buttons, .url-box').toggleClass('hide');
			});
		},
	};

	$.fn.editTool = function( method, options ){
		defaults = $.extend(defaults, options);
		if ( methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			//Si no se pasa ningún parámetro o el parámetro es 
			//un objeto de configuración llamamos al inicializador	
			return methods.init.apply( this, arguments );
		} else {
			//En el resto de los casos mostramos un error
			$.error( 'La función ' +  method + ' no existe en jQuery.editTool' );
		}	
	};
})(jQuery);